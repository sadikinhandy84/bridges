<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_anggota');
		$this->load->model('m_kategori');
		$this->load->model('m_galeri');
		$this->load->model('m_album');
	}

	function index(){
        $jum=$this->m_anggota->get_all_anggota();
        $page=$this->uri->segment(3);
        if(!$page):
            $offset = 0;
        else:
            $offset = $page;
        endif;
        $limit=6;
        $config['base_url'] = base_url() . 'anggota/index/';
        $config['total_rows'] = $jum->num_rows();
        $config['per_page'] = $limit; //show record per halaman
        $config['uri_segment'] =3; // uri parameter
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = floor($choice);
		$this->pagination->initialize($config);
		$x['newskat']=$this->m_kategori->get_all_kategori();
		$x['dataweb'] = $this->db->get('tbl_web')->row_array();
        $x['pagination'] =$this->pagination->create_links();
		$x['data']=$this->m_anggota->get_all_anggota_perpage($offset,$limit);
		$this->load->view('v_anggota',$x);
	}

	function page(){
		$limit=6;
		$jum=$this->m_anggota->get_all_anggota();
		$jum = ($jum->num_rows());

        $page=$this->uri->segment(4);
        if(!$page):
            $offset = 0;
        else:
            $offset = $page;
        endif;
        $limit=1;
        $config['base_url'] = base_url() . 'anggota/page/';
        $config['total_rows'] = $jum;
        $config['per_page'] = $limit; //show record per halaman
        $config['uri_segment'] =3; // uri parameter
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = floor($choice);
		$this->pagination->initialize($config);
		$x['newskat']=$this->m_kategori->get_all_kategori();
		$x['dataweb'] = $this->db->get('tbl_web')->row_array();
        $x['pagination'] =$this->pagination->create_links();
		$x['data']=$this->m_anggota->get_all_anggota_perpage($offset,$limit);
		$this->load->view('v_anggota',$x);
	}

    function detail_kartu()
	{
		$kode = $this->input->post('kode');
		$data = $this->m_pengguna->get_pengguna_login($kode);

		$this->load->view('v_anggota_detail',$data);
	}
	
}