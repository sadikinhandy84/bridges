<?php 
class About extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_galeri');
		$this->load->model('m_album');
		$this->load->model('m_tulisan');
		$this->load->model('m_pengunjung');
        $this->m_pengunjung->count_visitor();
	}

	function index(){
		$x['newskat']=$this->m_tulisan->get_kategori_for_blog();
		$x['dataweb'] = $this->db->get('tbl_web')->row_array();
		$x['alb']=$this->m_album->get_all_album();
		$this->load->view('v_sejarah',$x);
	}
}