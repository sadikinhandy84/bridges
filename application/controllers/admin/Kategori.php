<?php
class Kategori extends CI_Controller{
	function __construct(){
		parent::__construct();
		if(!isset($_SESSION['logged_in'])){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_kategori');
		$this->load->library('upload');
	}


	function index(){
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['data']=$this->m_kategori->get_all_kategori();
		$data['namamenu'] 		= "KATEGORI";
		$data['header'] 		=  'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_kategori';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';	
		$this->load->view('admin/layout/main',$data);
	}	
	

	function simpan_kategori(){
		$kategori= strip_tags($this->input->post('xkategori'));
		$status=strip_tags($this->input->post('optstatus'));

		$data = array(
				'kategori_nama' => $kategori,
				'slug' => url_title($kategori,'-',true),
				'status'=> $status
		);

		$this->m_kategori->simpan_kategori($data);
		echo $this->session->set_flashdata('msg','success');
		redirect('admin/kategori');
	}

	function update_kategori(){
		$kode=strip_tags($this->input->post('kode'));
		$kategori= $this->input->post('xkategori');
		$status=$this->input->post('optstatus');


		$data = array(
				'kategori_nama' => $kategori,
				'slug' => url_title($kategori,'-',true),
				'status'=> $status
		);

		$this->m_kategori->update_kategori($kode,$data);
		echo $this->session->set_flashdata('msg','info');
		redirect('admin/kategori');
	}
	function hapus_kategori(){
		$kode=strip_tags($this->input->post('kode'));
		$this->m_kategori->hapus_kategori($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/kategori');
	}
	

}