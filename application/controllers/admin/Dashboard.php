<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller{
	function __construct(){
		parent::__construct();
		if(!isset($_SESSION['logged_in'])){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_pengunjung');
	}
	function index(){
			$data['dataweb'] = $this->db->get('tbl_web')->row_array();
			$data['visitor'] = $this->m_pengunjung->statistik_pengujung();
			$data['header'] 		=  'admin/layout/v_header';
			$data['sidebar']		= 'admin/layout/v_sidebar';
			$data['content']		= 'admin/layout/v_dashboard';
			$data['footer']			= 'admin/layout/v_footer';
			$data['js']			= 'admin/layout/v_js';
		
			$this->load->view('admin/layout/main',$data);
	
	}
	
}