<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pengaturan extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['logged_in'])){
			$url=base_url('administrator');
			redirect('url');
		}
		
		$this->load->model('M_pengaturan');
		$this->load->library('upload'); //load library upload
	}
	
	function index(){
		//ambil data website
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['data']=$this->M_pengaturan ->tampil_data_web();
		//load template halaman 
		$data['namamenu'] = "PENGATURAN";
		$data['header'] 		=  'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_pengaturan';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']			= 'admin/layout/v_js';	
		$this->load->view('admin/layout/main',$data);
	
	}

	
	
	function  ubah_website()
	{
		
		$namaweb			= str_replace("'","`",$this->input->post('namaweb'));
		$idweb				= $this->input->post('idweb');
		$linkweb			= $this->input->post('linkweb');
		$keteranganweb		= $this->input->post('keteranganweb');
		$favicon2			= $this->input->post('favicon2');
		$logo2				= $this->input->post('logo2');
		$alamat				= str_replace("'","`",$this->input->post('alamatweb'));
		$linkweb			= $this->input->post('linkweb');
		$emailweb			= $this->input->post('emailweb');
		$tlpweb1			= $this->input->post('tlpweb1');
		$tlpweb2			= $this->input->post('tlpweb2');
		$tlpweb3			= $this->input->post('tlpweb3');
		$faxweb1			= $this->input->post('faxweb1');
		$faxweb2			= $this->input->post('faxweb2');
		$facebooklink	 	= $this->input->post('facebooklink');	
		$instagramlink		= $this->input->post('instagramlink');
		$youtubelink		= $this->input->post('youtubelink');
		$twitterlink		= $this->input->post('twitterlink');
		$longitude			= $this->input->post('longitude');
		$latitude			= $this->input->post('latitude');
		$kelurahanweb		= $this->input->post('kelurahanweb');
		$kecamatanweb		= $this->input->post('kecamatanweb');
		$kotaweb			= $this->input->post('kotaweb');
		$provinsiweb		= $this->input->post('provinsiweb');
		$kodeposweb		= $this->input->post('kodeposweb');
		$mapslink				= $this->input->post('maps');
		$linkvideo				= $this->input->post('linkvideo');

		
		///upload gambar
		 
		$config['upload_path'] 	= './storage/images/website/'; // path folder
		$config['allowed_types'] = 'gif|jpg|jpeg|bmp|png'; // type gambar yangbisa di akses
		$config['file_name']		= 'favicon'; //nama file yang terupload
		$config['overwrite']		= TRUE;
		//$config['overwrite']		= TRUE; //nama file yang sama akan direplace
		//$config['encrypt_name']	=  TRUE; //nama file yang terupload (kode nya otomatis)/ 
		$this->load->library('upload', $config);
		 
		 $this->upload->initialize($config);
		 if(!empty($_FILES['filefavicon']['name']))
		 {
			
			if ($this->upload->do_upload('filefavicon'))
			{
				
				$favicon=$this->upload->data();
				$fav = $favicon['file_name'];
				//jika ingin dikompress
				$config['image_library']='gd2';
				$config['source_image']='./storage/images/website/'.$fav['file_name'];
				$config['create_thumb']= FALSE;
				$config['maintain_ratio']= FALSE;
				$config['quality']= '50%';
				$config['width']= 300;
				$config['height']= 300;
				$config['new_image']= './storage/images/website/'.$fav['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
			}
			else
			{
				echo $this->session->set_flashdata('msg','error');
				redirect('admin/pengaturan');
			}
		 }
		 else
		 {
			$fav = $favicon2;
		 }

		 $config2['upload_path'] 	= './storage/images/website/'; // path folder
		 $config2['allowed_types']	= 'gif|jpg|jpeg|bmp|png'; // type gambar yangbisa di akses
		 $config2['file_name']		= 'logo'; //nama file yang terupload
		 $config2['overwrite']		= TRUE; //nama file yang sama akan direplace
		 //$config['encrypt_name']	=  TRUE; //nama file yang terupload (kode nya otomatis)/ 
		 $this->load->library('upload', $config2);

		 $this->upload->initialize($config2);

		 if(!empty($_FILES['filelogo']['name']))
		 {
			 if($this->upload->do_upload('filelogo'))
			 {
				$log	= $this->upload->data();
				$logo 	= $log['file_name'];
				//jika ingin dikompress
				$config['image_library']='gd2';
				$config['source_image']='./storage/images/website/'.$logo['file_name'];
				$config['create_thumb']= FALSE;
				$config['maintain_ratio']= FALSE;
				$config['quality']= '50%';
				$config['width']= 300;
				$config['height']= 300;
				$config['new_image']= './storage/images/website/'.$logo['file_name'];
				$this->load->library('image_lib', $config2);
				$this->image_lib->resize();
			 }
			 else
			 {
				echo $this->session->set_flashdata('msg','error');
				redirect('admin/pengaturan');
			 }
		 }
		 else
		 {
			$logo = $logo2;
		 }
			
		
		 
		$data = array(
			'favicon_pic'   =>$fav,
			'logo_pic'		=>$logo,			
		    'nama_web'		=>$namaweb,	
			'link_web'		=>$linkweb,		
			'alamat_web'	=>$alamat,
			'link_web' 		=>$linkweb,			
			'email_web'		=>$emailweb,		
			'tlp_web1'		=>$tlpweb1,		
			'tlp_web2' 		=>$tlpweb2,		
			'tlp_web3'		=>$tlpweb3,		
			'fax_web1'		=>$faxweb1,		
			'fax_web2'		=>$faxweb2,		
			'facebook_link'	=>$facebooklink,	
			'instagram_link'=>$instagramlink,	
			'youtube_link'	=>$youtubelink,
			'twitter_link'	=>$twitterlink,
			'longitude'		=>$longitude,
			'latitude'		=>$latitude,
			'kelurahan_web'	=>$kelurahanweb,
			'kecamatan_web'=>$kecamatanweb,
			'kota_web'		=>$kotaweb,
			'provinsi_web'	=>$provinsiweb,
			'kodepos_web'	=>$kodeposweb,
			'slogan_web' => $keteranganweb,
			'mapslink'		=>$mapslink,
			'link_video'	=>$linkvideo);
		
			
		if ($this->M_pengaturan->ubah_data_web($idweb,$data))
		{
			echo $this->session->set_flashdata('msg','success');
			redirect('admin/pengaturan');
		}
		else
		{
			echo $this->session->set_flashdata('msg','error');
			redirect('admin/pengaturan');
		}

	}	


    function upload_icon(){
		$config['upload_path'] 	= './storage/images/website/'; // path folder
		$config['allow_types']		= 'gif|jpg|jpeg|bmp|png'; // type gambar yangbisa di akses
		$config['file_name']		= 'favicon'; //nama file yang terupload
		$config['overwrite']		= TRUE; //nama file yang sama akan direplace
		//$config['encrypt_name']	=  TRUE; //nama file yang terupload (kode nya otomatis)/ 
		
		$this->upload->initialize($config);
		if(!empty($_FILES['filefavicon']['name']))
		{
			$fav = $this->upload->data();
			$fav = $favicon['file_name'];
			$data = array(
				'favicon_pic'  =>$fav
				);
			$hasil = $this->M_pengaturan->ubah_web($idweb,$data);
			echo json_encode($hasil);
		}
	}
	
}

