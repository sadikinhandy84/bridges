<?php
class Inbox extends CI_Controller{
	function __construct(){
		parent::__construct();
		if(!isset($_SESSION['logged_in'])){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_kontak');
	}

	function index(){
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$this->m_kontak->update_status_kontak();
		$data['data']=$this->m_kontak->get_all_inbox();
		$data['namamenu'] 		= " INBOX";
		$data['header'] 		=  'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_inbox';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';	
		$this->load->view('admin/layout/main',$data);
	}

	function hapus_inbox(){
		$kode=$this->input->post('kode');
		$this->m_kontak->hapus_kontak($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/inbox');
	}
}