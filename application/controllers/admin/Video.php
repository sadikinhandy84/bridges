<?php
class Video extends CI_Controller{
	function __construct(){
		parent::__construct();
		if(!isset($_SESSION['logged_in'])){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_video');
		$this->load->model('M_pengguna');
	
	}


	function index(){
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['data']=$this->M_video->get_all_video();
		$data['alb']=$this->M_video->get_all_videokat();
		$data['namamenu'] 		= 'Video';
		$data['header'] 		= 'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_video';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';	
		$this->load->view('admin/layout/main',$data);

	}
	
	function simpan_video(){

		$judul=strip_tags($this->input->post('xjudul'));
		$link=strip_tags($this->input->post('xvideo'));
		$kat=strip_tags($this->input->post('xalbum'));

		$kode=$this->session->userdata('idadmin');
		$user=$this->M_pengguna->get_pengguna_login($kode);
		$p=$user->row_array();
		$user_id=$p['pengguna_id'];
		$user_nama=$p['pengguna_nama'];

		if ($this->M_video->simpan_video($judul,$kat,$user_id,$user_nama,$link))
		{
			echo $this->session->set_flashdata('msg','success');
			redirect('admin/video');
		}else{
			echo $this->session->set_flashdata('msg','warning');
			redirect('admin/video');
		}
	                 
	}
	
	function update_video(){

		$video_id=$this->input->post('kode');
		$judul=strip_tags($this->input->post('xjudul'));
		$link=strip_tags($this->input->post('xvideo'));
		$kat=strip_tags($this->input->post('xalbum'));
		$kode=$this->session->userdata('idadmin');
		$user=$this->M_pengguna->get_pengguna_login($kode);
		$p=$user->row_array();
		$user_id=$p['pengguna_id'];
		$user_nama=$p['pengguna_nama'];
		$this->M_video->update_video($video_id,$judul,$kat,$user_id,$user_nama,$link);
		echo $this->session->set_flashdata('msg','info');
		redirect('admin/video');

	}

	function hapus_galeri(){
		$kode=$this->input->post('kode');
		$kat=$this->input->post('kat');
		$this->M_video->hapus_video($kode,$kat);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/video');
	}

}