<?php
class Partner extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (!isset($_SESSION['logged_in'])) {
			$url = base_url('administrator');
			redirect($url);
		};
		$this->load->model('m_partner');
		$this->load->model('m_pengguna');
		$this->load->library('upload');
	}


	function index()
	{
		$kode = $this->session->userdata('idadmin');
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['data'] = $this->m_partner->get_all_partner();
		$data['namamenu'] 		= " Partner";
		$data['header'] 		=  'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_partner';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';
		$this->load->view('admin/layout/main', $data);
	}

	function simpan_partner()
	{

		$config['upload_path'] = './assets/images/'; //path folder
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

		$judul = $this->input->post('judul');
		$link = $this->input->post('link');
	
		$this->upload->initialize($config);
		if (!empty($_FILES['filefoto']['name'])) {
			if ($this->upload->do_upload('filefoto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['quality'] = '60%';
				$config['width'] = 300;
				$config['height'] = 300;
				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$gambar = $gbr['file_name'];


				$data = array(
					'judul' => $judul,
					'link' => $link,
					'photo' => $gambar,
				);

				$this->m_partner->simpan_partner($data);
				echo $this->session->set_flashdata('msg', 'success');
				redirect('admin/partner');
			} else {
				echo $this->session->set_flashdata('msg', 'warning');
				redirect('admin/partner');
			}
		} else {
			$judul = $this->input->post('judul');
			$link = $this->input->post('link');
			$data = array(
				'judul' => $judul,
				'link' => $nik,
			);
			$this->m_partner->simpan_partner($data);
			echo $this->session->set_flashdata('msg', 'success');
			redirect('admin/partner');
		}
	}

	function update_partner()
	{

		$id = $this->input->post('id');
		$judul = $this->input->post('judul');
		$link = $this->input->post('link');
		
		$config['upload_path'] = './assets/images/'; //path folder
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

		$this->upload->initialize($config);
		if (!empty($_FILES['filefoto']['name'])) {
			if ($this->upload->do_upload('filefoto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['quality'] = '60%';
				$config['width'] = 300;
				$config['height'] = 300;
				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$gambar = $gbr['file_name'];


				$data = array(
					'judul' => $judul,
					'link' => $link,
					'photo' => $gambar,
				);

				$this->m_partner->update_partner($id,$data);
				echo $this->session->set_flashdata('msg', 'success');
				redirect('admin/partner');
			} else {
				echo $this->session->set_flashdata('msg', 'warning');
				redirect('admin/partner');
			}
		} else {
			$data = array(
				'judul' => $judul,
				'link' => $link,
				
			);

			$this->m_partner->update_partner($id,$data);
			echo $this->session->set_flashdata('msg', 'success');
			redirect('admin/partner');
		}
	}

	function hapus_partner()
	{
		$kode = $this->input->post('kode');
		$data = $this->m_pengguna->get_pengguna_login($kode);
		$q = $data->row_array();
		$p = $q['photo'];
		$path = base_url() . 'assets/images/' . $p;
		delete_files($path);
		$this->m_partner->hapus_partner($kode);
		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/partner');
	}

	
}
