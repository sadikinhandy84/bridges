<?php
class Slider extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['logged_in'])) {
            $url = base_url('administrator');
            redirect($url);
        };

        $this->load->model('M_slider');
        $this->load->model('M_pengguna');
        $this->load->library('upload');
    }

    public function index()
    {

        $data['dataweb'] = $this->db->get('tbl_web')->row_array();
        $data['data'] = $this->M_slider->get_all_slider();
        $data['namamenu'] = 'Slider';
        $data['header'] = 'admin/layout/v_header';
        $data['sidebar'] = 'admin/layout/v_sidebar';
        $data['content'] = 'admin/v_slider';
        $data['footer'] = 'admin/layout/v_footer';
        $data['js'] = 'admin/layout/v_js';
        $this->load->view('admin/layout/main', $data);

    }

    public function simpan_slider()
    {
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = true; //nama yang terupload nantinya

        $this->upload->initialize($config);
       
        if (!empty($_FILES['filefoto']['name'])) {
            if ($this->upload->do_upload('filefoto')) {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/images/' . $gbr['file_name'];
                $config['create_thumb'] = false;
                $config['maintain_ratio'] = false;
                // $config['width'] = 4040;
                // $config['height'] = 1296;
                $config['new_image'] = './assets/images/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar = $gbr['file_name'];
                $judul = $this->input->post('judul');
                $keterangan = $this->input->post('keterangan');
                $linkmenu = $this->input->post('linkmenu');

                $data = array(
                     'title' 	=> $judul,
                     'text'		=> $keterangan,
                     'img'		=> $gambar,
                     'linkmenu'	=> $linkmenu
                );


                $this->M_slider->simpan_slider($data);
                echo $this->session->set_flashdata('msg', 'success');
                redirect('admin/slider');
            } else {
                echo $this->session->set_flashdata('msg', 'warning');
                redirect('admin/slider');
            }

        } else {
            redirect('admin/slider');
        }

    }

    public function update_slider()
    {

        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = true; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if (!empty($_FILES['filefoto']['name'])) {
            if ($this->upload->do_upload('filefoto')) {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/images/' . $gbr['file_name'];
                $config['create_thumb'] = false;
                $config['maintain_ratio'] = false;
 
                // $config['width'] = 4040;
                // $config['height'] = 1296;
                $config['new_image'] = $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                
                $id = $this->input->post('kode');
                $images = $this->input->post('gambar');
                $path = './assets/images/' . $images;
                @unlink($path);
               
                $gambar = $gbr['file_name'];
                $judul = $this->input->post('judul');
                $keterangan = $this->input->post('keterangan');
                $linkmenu = $this->input->post('linkmenu');

                $data = array(
                    'title' => $judul,
                    'text' => $keterangan,
                    'img' => $gambar,
                    'linkmenu' => $linkmenu,
                );


                $this->M_slider->ubah_slider($id,$data);
                echo $this->session->set_flashdata('msg', 'info');
                redirect('admin/slider');

            } else {
                echo $this->session->set_flashdata('msg', 'warning');
                redirect('admin/slider');
            }

        } else {
            $galeri_id = $this->input->post('kode');
            $judul = $this->input->post('judul');
            $keterangan = $this->input->post('keterangan');
            $linkmenu = $this->input->post('linkmenu');

            $data = array(
                'title' => $judul,
                'text' => $keterangan,
                'linkmenu' => $linkmenu,
            );

           
           
            $this->M_slider->update_slider_tanpa_img($galeri_id, $data);
            echo $this->session->set_flashdata('msg', 'info');
            redirect('admin/slider');
        }

    }

    public function hapus_slider()
    {
        $kode = $this->input->post('kode');
        $gambar = $this->input->post('gambar');
        $path = './assets/images/' . $gambar;
        @unlink($path);
        $this->M_slider->hapus_slider($kode);
        echo $this->session->set_flashdata('msg', 'success-hapus');
        redirect('admin/slider');
    }

}
