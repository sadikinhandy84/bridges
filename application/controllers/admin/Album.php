<?php
class Album extends CI_Controller{
	function __construct(){
		parent::__construct();
		if(!isset($_SESSION['logged_in'])){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_album');
		$this->load->model('M_pengguna');
		$this->load->library('upload');
	}


	function index(){
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['data']=$this->M_album->get_all_album();
		$data['namamenu'] 		= 'Album';
		$data['header'] 		= 'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_album';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';	
		$this->load->view('admin/layout/main',$data);
	}
	
	function simpan_album(){
				$config['upload_path'] = './storage/images/galery/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./storage/images/galery/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 500;
	                        $config['height']= 400;
	                        $config['new_image']= './storage/images/galery/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $gambar=$gbr['file_name'];
							$album=strip_tags($this->input->post('xnama_album'));
							$kode=$this->session->userdata('idadmin');
							$user=$this->M_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];
							$this->M_album->simpan_album($album,$user_id,$user_nama,$gambar);
							echo $this->session->set_flashdata('msg','success');
							redirect('admin/album');
					}else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/album');
	                }
	                 
	            }else{
					redirect('admin/album');
				}
				
	}
	
	function update_album(){
				
	            $config['upload_path'] = './storage/images/galery/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./storage/images/galery/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 500;
	                        $config['height']= 400;
	                        $config['new_image']= './storage/images/galery/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $gambar=$gbr['file_name'];
	                        $album_id=$this->input->post('kode');
	                        $album_nama=strip_tags($this->input->post('xnama_album'));
							$images=$this->input->post('gambar');
							$path='./storage/images/galery/'.$images;
							@unlink($path);
							$kode=$this->session->userdata('idadmin');
							$user=$this->M_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];
							if ($this->M_album->update_album($album_id,$album_nama,$user_id,$user_nama,$gambar)){
								echo $this->session->set_flashdata('msg','success');
								redirect('admin/album');
							}
							else
							{
								echo $this->session->set_flashdata('msg','info');
								redirect('admin/album');
							}
							
	                    
	                }else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/album');
	                }
	                
	            }else{
							$album_id=$this->input->post('kode');
	                        $album_nama=strip_tags($this->input->post('xnama_album'));
							$kode=$this->session->userdata('idadmin');
							$user=$this->M_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];
							$this->M_album->update_album_tanpa_img($album_id,$album_nama,$user_id,$user_nama);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/album');
	            } 

	}

	function hapus_album(){
		$kode=$this->input->post('kode');
		$gambar=$this->input->post('gambar');
		$path='./storage/images/galery/'.$gambar;
		@unlink($path);
		$this->M_album->hapus_album($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/album');
	}

}