<?php
class Videokat extends CI_Controller{
	function __construct(){
		parent::__construct();
		if(!isset($_SESSION['logged_in'])){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_video');
		$this->load->model('M_pengguna');
		$this->load->library('upload');
	}


	function index(){
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['data']=$this->M_video->get_all_videokat();
		$data['namamenu'] 		= 'Kategori Video';
		$data['header'] 		= 'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_videokat';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';	
		$this->load->view('admin/layout/main',$data);
	}
	
	function simpan_videokat(){
				$config['upload_path'] = './storage/images/galery/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./storage/images/galery/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 500;
	                        $config['height']= 400;
	                        $config['new_image']= './storage/images/galery/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $gambar=$gbr['file_name'];
							$album=strip_tags($this->input->post('xnama_album'));
							$kode=$this->session->userdata('idadmin');
							$user=$this->M_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];
							$this->M_video->simpan_videokat($album,$user_id,$user_nama,$gambar);
							echo $this->session->set_flashdata('msg','success');
							redirect('admin/Videokat');
					}else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/Videokat');
	                }
	                 
	            }else{
					redirect('admin/Videokat');
				}
				
	}
	
	function update_videokat(){
				
	            $config['upload_path'] = './storage/images/galery/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./storage/images/galery/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 500;
	                        $config['height']= 400;
	                        $config['new_image']= './storage/images/galery/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $gambar=$gbr['file_name'];
	                        $album_id=$this->input->post('kode');
	                        $album_nama=strip_tags($this->input->post('xnama_album'));
							$images=$this->input->post('gambar');
							$path='./storage/images/galery/'.$images;
							@unlink($path);
							$kode=$this->session->userdata('idadmin');
							$user=$this->M_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];

							$data = array(
								'videokat_nama' => $album_nama,
								'videokat_pengguna_id '  => $user_id,
								'videokat_author'  => $user_nama,
								'videokat_cover'  => $gambar
							);
					   

							if ($this->M_video->update_videokat($album_id,$data)){
								echo $this->session->set_flashdata('msg','error');
								redirect('admin/Videokat');
							}
							else
							{
								echo $this->session->set_flashdata('msg','info');
								redirect('admin/Videokat');
							}
							
	                    
	                }else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/Videokat');
	                }
	                
	            }else{
							$album_id=$this->input->post('kode');
	                        $album_nama=strip_tags($this->input->post('xnama_album'));
							$kode=$this->session->userdata('idadmin');
							$user=$this->M_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];

							$data = array(
								'videokat_nama' => $album_nama,
								'videokat_pengguna_id '  => $user_id,
								'videokat_author'  => $user_nama,
							);


							$this->M_video->update_videokat($album_id,$data);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/Videokat');
	            } 

	}

	function hapus_videokat(){
		$kode=$this->input->post('kode');
		$gambar=$this->input->post('gambar');
		$path='./storage/images/galery/'.$gambar;
		@unlink($path);
		$this->M_video->hapus_videokat($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/Videokat');
	}

}