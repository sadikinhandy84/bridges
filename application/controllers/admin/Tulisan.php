<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tulisan extends CI_Controller{
	function __construct(){
		parent::__construct();
		if(!isset($_SESSION['logged_in'])){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_kategori');
		$this->load->model('m_tulisan');
		$this->load->model('m_pengguna');
		$this->load->library('upload');
	}



	function index(){
		$data['data']=$this->m_tulisan->get_all_tulisan();
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['namamenu'] 		= "LIST BERITA";
		$data['header'] 		=  'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_tulisan';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';	
		$this->load->view('admin/layout/main',$data);
	
	}
	function add_tulisan(){
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['kat']=$this->m_kategori->get_all_kategori();
		$data['namamenu'] 		= "TAMBAH BERITA";
		$data['header'] 		=  'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_add_tulisan';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';	
		$this->load->view('admin/layout/main',$data);
	}
	function get_edit($id){
		$kode=$id;
		$data['data']=$this->m_tulisan->get_tulisan_by_kode($kode);
		$data['kat']=$this->m_kategori->get_all_kategori();
		$current_category = $this->db->select('category_id')->where(array('post_id' => $kode))->get('tbl_posts_categories')->result_array();

		$category_ids = array();
        if(!empty($current_category)){
            foreach($current_category as $current){
                $category_ids[] = $current['category_id'];
            }
		}
		
		$data['listkat']= $category_ids;
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['namamenu'] 		= "EDIT BERITA";
		$data['header'] 		=  'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_edit_tulisan';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';	
		$this->load->view('admin/layout/main',$data);

		



	}
	function simpan_tulisan(){
				$config['upload_path'] = './storage/images/berita/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

				$this->upload->initialize($config);
				
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./storage/images/berita/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 840;
	                        $config['height']= 450;
	                        $config['new_image']= './storage/images/berita/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
							$this->image_lib->resize();
							
							

	                        $gambar=$gbr['file_name'];
							$judul=strip_tags($this->input->post('xjudul'));
							$filter=str_replace("?", "", $judul);
							$filter2=str_replace("$", "", $filter);
							$pra_slug=$filter2.'.html';
							$slug=str_replace(",","",strtolower(str_replace(" ", "-", $pra_slug)));
							$isi=$this->input->post('xisi');
							$kode=$this->session->userdata('idadmin');
							$user=$this->m_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];
							$kategorilist= $this->input->post('xkategori[]');
							$this->m_tulisan->simpan_tulisan($judul,$isi,$user_id,$user_nama,$gambar,$slug,$kategorilist);
							
							
							echo $this->session->set_flashdata('msg','success');
							redirect('admin/tulisan');
					}else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/tulisans');
	                }
	                 
	            }else{
					redirect('admin/tulisan');
				}
				
	}
	
	function update_tulisan(){
				
	            $config['upload_path'] = './storage/images/berita/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

				$data = $_POST;
				
				unset($data['category']);
				unset($data['tag']);

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {

							$judul=strip_tags($this->input->post('xjudul'));
							$filter=str_replace("?", "", $judul);
							$filter2=str_replace("$", "", $filter);
							$pra_slug=$filter2.'.html';
							$slug=strtolower(str_replace(" ", "-", $pra_slug));
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./storage/images/berita/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 840;
	                        $config['height']= 450;
	                        $config['new_image']= './storage/images/berita/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $gambar=$gbr['file_name'];
	                        $tulisan_id=$this->input->post('kode');
	                        $slug=str_replace(",","",strtolower(str_replace(" ", "-", $pra_slug)));
	                        $filter=str_replace("?", "", $judul);
							$filter2=str_replace("$", "", $filter);
							$pra_slug=$filter2.'.html';
							$slug=strtolower(str_replace(" ", "-", $pra_slug));
							$isi=$this->input->post('xisi');
						
							$kode=$this->session->userdata('idadmin');
							$user=$this->m_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];
							$kategorilist= $this->input->post('xkategori[]');
							$this->m_tulisan->update_tulisan($tulisan_id,$judul,$isi,$user_id,$user_nama,$gambar,$slug,$kategorilist);
							
							
					
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/tulisan');
	                    
	                }else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/tulisan');
	                }
	                
	            }else{
							$tulisan_id=$this->input->post('kode');
							$judul=strip_tags($this->input->post('xjudul'));
							$filter=str_replace("?", "", $judul);
							$filter2=str_replace("$", "", $filter);
							$pra_slug=$filter2.'.html';
							$slug=strtolower(str_replace(" ", "-", $pra_slug));
							$isi=$this->input->post('xisi');
							$kode=$this->session->userdata('idadmin');
							$user=$this->m_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];
							$kategorilist= $this->input->post('xkategori[]');
							$this->m_tulisan->update_tulisan_tanpa_img($tulisan_id,$judul,$isi,$user_id,$user_nama,$slug,$kategorilist);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/tulisan');
	            } 

	}

	function hapus_tulisan(){
		$kode=$this->input->post('kode');
		$gambar=$this->input->post('gambar');
		$path='./storage/images/berita/'.$gambar;
		unlink($path);
		$this->m_tulisan->hapus_tulisan($kode);

		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/tulisan');
	}

	
	
	public function upload_editor_image()
    {
        $image_server_url =  "/storage/images/berita/";
        $funcNum = $_GET['CKEditorFuncNum'] ; 
 
/*-------This is important to mention to identify the different dialog box in editor. If not mention it will throw error when any other dialog plugin (like youtube etc)  is used along with it----*/
 
        try {
            $image = $this->general_model->upload_image('upload', "storage/images/berita/" );
            echo "<script 			type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '" . 	$image_server_url . $image . "', '');</script>";
        }
        catch (Exception $e) {
            echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '', '" . $e->getMessage() . "');</script>";
        }
	}

}