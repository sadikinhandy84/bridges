<?php
class Anggota extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (!isset($_SESSION['logged_in'])) {
			$url = base_url('administrator');
			redirect($url);
		};
		$this->load->model('m_anggota');
		$this->load->model('m_pengguna');
		$this->load->library('upload');
	}


	function index()
	{
		$kode = $this->session->userdata('idadmin');
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['data'] = $this->m_anggota->get_all_anggota();
		$data['namamenu'] 		= " Anggota";
		$data['header'] 		=  'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_anggota';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';
		$get_prov = $this->db->select('*')->from('wilayah_provinsi')->get();
		$data['provinsi'] = $get_prov->result();
		$this->load->view('admin/layout/main', $data);
	}

	function add_anggota(){
		$kode = $this->session->userdata('idadmin');
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$get_prov = $this->db->select('*')->from('wilayah_provinsi')->get();
		$data['provinsi'] = $get_prov->result();
		$data['namamenu'] 		= "ADD ANGGOTA";
		$data['header'] 		=  'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_add_anggota';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';	
		$this->load->view('admin/layout/main',$data);
	}

	function simpan_anggota()
	{

		$config['upload_path'] = './assets/images/'; //path folder
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
		$this->upload->initialize($config);

		$nokta = $this->input->post('nokta');
		$nik = $this->input->post('nik');
		$nama = $this->input->post('nama');
		$jenkel = $this->input->post('jenkel');
		$alamat = $this->input->post('alamat');
		$tempat = $this->input->post('tempat');
		$tgl = $this->input->post('tgl');
		$kerja = $this->input->post('kerja');
		$nohp = $this->input->post('nohp');
		$ukuran = $this->input->post('ukuran');
		$tglkartu = $this->input->post('tglkartu');
		$prov = $this->input->post('prov');
		$kab = $this->input->post('kab');
		$kec = $this->input->post('kec');
		$des = $this->input->post('des');
		$stsmember = $this->input->post('member');

		if (!empty($_FILES['filefoto']['name'])) {
			if ($this->upload->do_upload('filefoto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$gambar = $gbr['file_name'];
				
			
				$gambarpic = array('photo' => $gambar);
			} else {
				echo $this->session->set_flashdata('msg', 'error');
				redirect('admin/anggota');
			}
		}

	
		if (!empty($_FILES['filekartu']['name'])) {
			if ($this->upload->do_upload('filekartu')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$kartu = $gbr['file_name'];

				$kartupic = array('kartu' => $kartu);
			
				
			} else {
				echo $this->session->set_flashdata('msg', 'warning');
				redirect('admin/anggota');
			}
		}

		if (!empty($_FILES['filektp']['name'])) {
			if ($this->upload->do_upload('filektp')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$ktp = $gbr['file_name'];

				$ktp = array('ktp' => $ktp);
			
				
			} else {
				echo $this->session->set_flashdata('msg', 'warning');
				redirect('admin/anggota');
			}
		}

				$data = array(
					'no_kta' => $nokta,
					'nik' => $nik,
					'nama_lengkap' => $nama,
					'jenis_kelamin' => $jenkel,
					'alamat' => $alamat,
					'tempat_lahir' => $tempat,
					'tgl_lahir' => $tgl,
					'pekerjaan' => $kerja,
					'no_hp' => $nohp,
					'ukuran_baju' => $ukuran,
					'tgl_kartu' => $tglkartu,
					'provinsi' => $prov,
					'kota' => $kab,
					'kecamatan' => $kec,
					'desa' => $des,
					'status_member' => $stsmember
				);
				$data = $data + $gambarpic  + $kartupic + $ktp;

				$this->m_anggota->simpan_anggota($data);
				echo $this->session->set_flashdata('msg', 'success');
				redirect('admin/anggota');
		
	}
	

	function edit_anggota($id){
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['data']=$this->m_anggota->get_anggota_byid($id);
		$get_prov = $this->db->select('*')->from('wilayah_provinsi')->get();
		$data['provinsi'] = $get_prov->result();

		$data['namamenu'] 		= "EDIT ANGGOTA";
		$data['header'] 		=  'admin/layout/v_header';
		$data['sidebar']		= 'admin/layout/v_sidebar';
		$data['content']		= 'admin/v_edit_anggota';
		$data['footer']			= 'admin/layout/v_footer';
		$data['js']				= 'admin/layout/v_js';	
		$this->load->view('admin/layout/main',$data);
	}

	function update_anggota()
	{

		$kartupic = array();
		$gambarpic = array();

		$id = $this->input->post('id');
		$nokta = $this->input->post('nokta');
		$nik = $this->input->post('nik');
		$nama = $this->input->post('nama');
		$jenkel = $this->input->post('jenkel');
		$alamat = $this->input->post('alamat');
		$tempat = $this->input->post('tempat');
		$tgl = $this->input->post('tgl');
		$kerja = $this->input->post('kerja');
		$nohp = $this->input->post('nohp');
		$ukuran = $this->input->post('ukuran');
		$tglkartu = $this->input->post('tglkartu');
		$prov = $this->input->post('prov');
		$kab = $this->input->post('kab');
		$kec = $this->input->post('kec');
		$des = $this->input->post('des');
		$stsmember = $this->input->post('member');

		$config['upload_path'] = './assets/images/'; //path folder
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

		$this->upload->initialize($config);
		if (!empty($_FILES['filefoto']['name'])) {
			if ($this->upload->do_upload('filefoto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$gambar = $gbr['file_name'];
				
			
				$gambarpic = array('photo' => $gambar);
			} else {
				echo $this->session->set_flashdata('msg', 'warning');
				redirect('admin/anggota');
			}
		}


		if (!empty($_FILES['filekartu']['name'])) {
			if ($this->upload->do_upload('filekartu')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$kartu = $gbr['file_name'];

				$kartupic = array('kartu' => $kartu);
			
			} else {
				echo $this->session->set_flashdata('msg', 'warning');
				redirect('admin/anggota');
			}
		}

		if (!empty($_FILES['filektp']['name'])) {
			if ($this->upload->do_upload('filektp')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$ktp = $gbr['file_name'];

				$ktp = array('ktp' => $ktp);
			
				
			} else {
				echo $this->session->set_flashdata('msg', 'warning');
				redirect('admin/anggota');
			}
		}

		$data = array(
			'no_kta' => $nokta,
			'nik' => $nik,
			'nama_lengkap' => $nama,
			'jenis_kelamin' => $jenkel,
			'alamat' => $alamat,
			'tempat_lahir' => $tempat,
			'tgl_lahir' => $tgl,
			'pekerjaan' => $kerja,
			'no_hp' => $nohp,
			'ukuran_baju' => $ukuran,
			'tgl_kartu' => $tglkartu,
			'provinsi' => $prov,
			'kota' => $kab,
			'kecamatan' => $kec,
			'desa' => $des,
			'status_member' => $stsmember
		);
		$data = $data + $gambarpic + $kartupic;

		$this->m_anggota->update_anggota($id,$data);
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/anggota');
	}

	function hapus_anggota()
	{
		$kode = $this->input->post('kode');
		$data = $this->m_pengguna->get_pengguna_login($kode);
		$q = $data->row_array();
		$p = $q['photo'];
		$path = base_url() . 'assets/images/' . $p;
		delete_files($path);
		$this->m_anggota->hapus_anggota($kode);
		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/anggota');
	}

	function add_ajax_kab($id_prov,$nilai_prv=null)
	{
		
		$query = $this->db->get_where('wilayah_kabupaten', array('provinsi_id' => $id_prov));
		$data = "<option value=''>- Select Kabupaten -</option>";
		foreach ($query->result() as $value) {
			if($value->id == $nilai_prv){
				$selected = " selected";
			}
			else{
				$selected = "";
			}
			$data .= "<option value='" . $value->id . "' ".$selected.">" . $value->nama . "</option>";
		}
		echo $data;
	}

	function add_ajax_kec($id_kab,$nilai_kab=null)
	{
		$query = $this->db->get_where('wilayah_kecamatan', array('kabupaten_id' => $id_kab));
		$data = "<option value=''> - Pilih Kecamatan - </option>";
		foreach ($query->result() as $value) {
			if($value->id == $nilai_kab){
				$selected = " selected";
			}
			else{
				$selected = "";
			}
			$data .= "<option value='" . $value->id ."' ".$selected." >" . $value->nama . "</option>";
		}
		echo $data;
	}

	function add_ajax_des($id_kec,$nilai_desa=null)
	{
		
		$query = $this->db->get_where('wilayah_desa', array('kecamatan_id' => $id_kec));
		$data = "<option value=''> - Pilih Desa - </option>";
		foreach ($query->result() as $value) {
			if($value->id == $nilai_desa){
				$selected = " selected";
			}
			else{
				$selected = "";
			}
			$data .= "<option value='" . $value->id . "' ".$selected." >" . $value->nama . "</option>";
		}
		echo $data;
	}
	
}
