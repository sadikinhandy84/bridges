<?php 
class Home extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_tulisan');
		$this->load->model('m_pengunjung');
		$this->load->model('m_kategori');
		$this->load->model('m_tulisan');
        $this->m_pengunjung->count_visitor();
	}
	function index(){
		$data['post']=$this->m_tulisan->get_post_home();
		$data['newskat']=$this->m_tulisan->get_kategori_for_blog();
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['slidegambar'] = $this->db->get('tbl_slide')->result_array();
		$data['partner'] = $this->db->get('tbl_partner')->result_array();
		$this->load->view('v_home1',$data);
		
	}
}