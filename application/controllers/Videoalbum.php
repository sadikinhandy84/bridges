<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Videoalbum extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_video');
		$this->load->model('m_pengunjung');
		$this->load->model('m_tulisan');
        $this->m_pengunjung->count_visitor();
	}

	function index(){
		$x['newskat']=$this->m_tulisan->get_kategori_for_blog();
		$x['dataweb'] = $this->db->get('tbl_web')->row_array();
		$x['alb']=$this->m_video->get_all_videokat();
		$x['data']=$this->m_video->get_all_video();
		$this->load->view('v_videokat',$x);
	}

	function kategori(){
		$album=$this->uri->segment(3);
		$x['newskat']=$this->m_tulisan->get_kategori_for_blog();
		$x['dataweb'] = $this->db->get('tbl_web')->row_array();
		$x['alb']=$this->m_video->get_all_videokat();
		$x['data']=$this->m_video->get_video_by_kat_id($album);
		$this->load->view('v_video',$x);
	}

}









