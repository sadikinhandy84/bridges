<?php 
class Visimisi extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_pengunjung');
		$this->load->model('m_tulisan');
        $this->m_pengunjung->count_visitor();
	}

	function index(){
		$data['newskat']=$this->m_tulisan->get_kategori_for_blog();
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['slidegambar'] = $this->db->get('tbl_slide')->result_array();
		$this->load->view('v_visimisi',$data);
	}

	function kirim_pesan(){
		$nama=htmlspecialchars($this->input->post('nama',TRUE),ENT_QUOTES);
		$email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
		$pesan=htmlspecialchars(trim($this->input->post('pesan',TRUE)),ENT_QUOTES);
		$this->m_kontak->kirim_pesan($nama,$email,$pesan);
		echo $this->session->set_flashdata('msg',"<div class='alert alert-info'>Terima kasih telah menghubungi kami.</div>");
		redirect('kontak');
	}
}