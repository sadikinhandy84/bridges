<?php
class Daftar extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_pengunjung');
		$this->load->model('m_daftar');
		$this->load->model('m_kategori');
		$this->load->model('m_tulisan');
		$this->load->library('form_validation');
		$this->load->library('upload');
	}

	function index()
	{
		$data['newskat'] = $this->m_tulisan->get_kategori_for_blog();
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['slidegambar'] = $this->db->get('tbl_slide')->result_array();
		$data['partner'] = $this->db->get('tbl_partner')->result_array();

		$get_prov = $this->db->select('*')->from('wilayah_provinsi')->get();
		$data['provinsi'] = $get_prov->result();
		$this->load->view('v_daftar', $data);
	}



	function simpan_daftar()
	{

		$config['upload_path'] = './assets/images/'; //path folder
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

		$nik = $this->input->post('nik');
		$nama = $this->input->post('nama');
		$jenkel = $this->input->post('jenkel');
		$alamat = $this->input->post('alamat');
		$tempat = $this->input->post('tempat');
		$tgl = $this->input->post('tgl');
		$kerja = $this->input->post('kerja');
		$nohp = $this->input->post('nohp');
		$ukuran = $this->input->post('ukuran');
		$prov = $this->input->post('prov');
		$kab = $this->input->post('kab');
		$kec = $this->input->post('kec');
		$des = $this->input->post('des');
		$setuju = $this->input->post('setuju');
		// $ktp = $_FILES['filektp']['name'];
		// $gambar = $_FILES['filephoto']['name'];
		$status = $this->input->post('status');



		$this->_validasi();
		if ($this->form_validation->run() == false) {
			$data['newskat'] = $this->m_tulisan->get_kategori_for_blog();
			$data['dataweb'] = $this->db->get('tbl_web')->row_array();
			$data['slidegambar'] = $this->db->get('tbl_slide')->result_array();
			$data['partner'] = $this->db->get('tbl_partner')->result_array();
			$get_prov = $this->db->select('*')->from('wilayah_provinsi')->get();
			$data['provinsi'] = $get_prov->result();
			$this->load->view('v_daftar', $data);
		} else {

			$this->upload->initialize($config);
			if ($this->upload->do_upload('filephoto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$gambar = $gbr['file_name'];
			} else {

				$data['newskat'] = $this->m_tulisan->get_kategori_for_blog();
				$data['dataweb'] = $this->db->get('tbl_web')->row_array();
				$data['slidegambar'] = $this->db->get('tbl_slide')->result_array();
				$data['partner'] = $this->db->get('tbl_partner')->result_array();
				$get_prov = $this->db->select('*')->from('wilayah_provinsi')->get();
				$data['provinsi'] = $get_prov->result();
				echo $this->session->set_flashdata('msg', 'error');
				$this->load->view('v_daftar', $data);
			}

			if ($this->upload->do_upload('filektp')) {
				$ktp = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;

				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$ktp = $ktp['file_name'];
			}


			$data = array(
				'nik' => $nik,
				'nama_lengkap' => $nama,
				'jenis_kelamin' => $jenkel,
				'alamat' => $alamat,
				'tempat_lahir' => $tempat,
				'tgl_lahir' => $tgl,
				'pekerjaan' => $kerja,
				'no_hp' => $nohp,
				'ukuran_baju' => $ukuran,
				'photo' => $gambar,
				'ktp' => $ktp,
				'provinsi' => $prov,
				'kota' => $kab,
				'kecamatan' => $kec,
				'desa' => $des,
				'status_member' => $status
			);

			if ($this->m_daftar->simpan_daftar($data)) {
				echo $this->session->set_flashdata('msg', 'success');
				redirect('daftar');
			} else {
				echo $this->session->set_flashdata('msg', 'error');
				redirect('daftar');
			}
		}
	}


	private function _validasi()
	{

		$this->form_validation->set_rules('nik', 'NIk KTP/SIM', 'required|trim|is_unique[tbl_anggota.nik]');
		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
		$this->form_validation->set_rules('tempat', 'Tempat Lahir', 'required|trim');
		$this->form_validation->set_rules('tgl', 'Tanggal Lahir', 'required|trim');
		$this->form_validation->set_rules('nohp', 'No HP', 'required|trim');
		// $this->form_validation->set_rules($_FILES['filephoto']['name'], 'Photo', 'required|trim');
		// $this->form_validation->set_rules($_FILES['filektp']['name'], 'Ktp', 'required|trim');
		$this->form_validation->set_rules('prov', 'Provinsi', 'required|trim');
		$this->form_validation->set_rules('kab', 'Kota/kabupaten', 'required|trim');
		$this->form_validation->set_rules('kec', 'Kecamatan', 'required|trim');
		$this->form_validation->set_rules('des', 'Desa', 'required|trim');
		$this->form_validation->set_rules('setuju', 'Pernyataan', 'required|trim');
	}

	function add_ajax_kab($id_prov)
	{
		$query = $this->db->get_where('wilayah_kabupaten', array('provinsi_id' => $id_prov));
		$data = "<option value=''>- Select Kabupaten -</option>";
		foreach ($query->result() as $value) {
			$data .= "<option value='" . $value->id . "'>" . $value->nama . "</option>";
		}
		echo $data;
	}

	function add_ajax_kec($id_kab)
	{
		$query = $this->db->get_where('wilayah_kecamatan', array('kabupaten_id' => $id_kab));
		$data = "<option value=''> - Pilih Kecamatan - </option>";
		foreach ($query->result() as $value) {
			$data .= "<option value='" . $value->id . "'>" . $value->nama . "</option>";
		}
		echo $data;
	}

	function add_ajax_des($id_kec)
	{
		$query = $this->db->get_where('wilayah_desa', array('kecamatan_id' => $id_kec));
		$data = "<option value=''> - Pilih Desa - </option>";
		foreach ($query->result() as $value) {
			$data .= "<option value='" . $value->id . "'>" . $value->nama . "</option>";
		}
		echo $data;
	}
}
