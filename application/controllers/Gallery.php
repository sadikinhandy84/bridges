<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gallery extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_galeri');
		$this->load->model('m_album');
		$this->load->model('m_pengunjung');
		$this->load->model('m_tulisan');
        $this->m_pengunjung->count_visitor();
	}

	function kategori(){
		$album=$this->uri->segment(3);
		
		$x['newskat']=$this->m_tulisan->get_kategori_for_blog();
		$x['dataweb'] = $this->db->get('tbl_web')->row_array();
		$x['alb']=$this->m_album->get_all_album();
		$x['data']=$this->m_galeri->get_galeri_by_album_id($album);
		$this->load->view('v_gallery',$x);
	}
}


