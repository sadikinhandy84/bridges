<?php 
class Ourvalue extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_pengunjung');
		$this->load->model('m_kontak');
		$this->load->model('m_tulisan');
        $this->m_pengunjung->count_visitor();
	}

	function index(){
		$data['newskat']=$this->m_tulisan->get_kategori_for_blog();
		$data['dataweb'] = $this->db->get('tbl_web')->row_array();
		$data['slidegambar'] = $this->db->get('tbl_slide')->result_array();
		$this->load->view('v_ourvalue',$data);
	}

}