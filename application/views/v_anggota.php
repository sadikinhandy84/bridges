<?php
error_reporting(0);
$url = '';
$img = '';
$title = '';
?>
<?php
$this->load->view('v_header');
?>

<div class="mt-5 pt-2">
  <?php
  $this->load->view('v_toolbar');
  ?>
</div>
<?php foreach ($data->result_array() as $j) { ?>
  <div class="modal fade" id="modalKartu<?php echo $j['id']; ?>" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span aria-hidden="true">×</span></button>
          
        </div>
        <div class="modal-body">
          <div class="form-group">
    
            <div class="col-12">
              <img id="blah1" name="photo1" height="100%" width="100%" alt="" src="<?php echo base_url('assets/images/' . $j['kartu']); ?>"><br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } ?>

<button class="fa fa-angle-up" onclick="topFunction()" id="myBtn" title="Go to top"></button>

<section class="bg-light" id="portfolio">
  <div class="container">
    <div class="row mt-5">
      <div class="col-lg-12 text-center pb__20">
        <h2 class="text-uppercase">Daftar Anggota</h2>
        <div class="divider"></div>
      </div>
    </div>

    <div class="row">
      <?php foreach ($data->result_array() as $j) {
        $tgl = date('Y-m-d', strtotime($j['tgl_kartu']))
      ?>
        <div class="col-lg-4">
          <div class="team-member">
            <div class="card mb-4 shadow-sm">
              <img class="mx-auto rounded-circle" src="<?php echo base_url() . 'assets/images/' . $j['photo'] ?>" alt="...">
              <h5><?= $j['nama_lengkap'] ?></h5>
              <p class="text-muted"><?= $j['no_kta'] ?></p>

              <!-- <p><a data-toggle="modal" data-target="#modalKartu<?php echo $j['id']; ?>" class="btn btn-primary with-arrow">Detail<i class="icon-arrow-right"></i></a></p> -->
            </div>
          </div>
        </div>


        <!-- <div class="col-6 col-md-3">
          <div class="card mb-4 shadow-sm">
            <img src="<?php echo base_url() . 'assets/images/' . $j['photo'] ?>" alt="" class="card-img-top">
            <div class="card-body">
              <div class="portfolio-caption">
                <h4><?= $j['nama_lengkap'] ?></h4>
                <p class="text-muted"><?= $j['no_kta'] ?></p>
               <h4 class="service-heading"><?= $j['no_kta'] ?></h4>
                <h5 class="service-heading"><?= $j['nama_lengkap'] ?></h5>
                <span class="author"></i><?= tgl_indo($tgl) ?> </span><br>
                <p><a href="<?php echo base_url() . 'anggota/page/' . $j['id']; ?>" class="btn btn-primary with-arrow">Detail<i class="icon-arrow-right"></i></a></p>
              </div>
            </div>
          </div>
        </div>  -->

      <?php } ?>
    </div>

    <div class="row">
      <div class="col-lg-12 text-center pt__20">
        <center><?php echo $pagination; ?></center>
      </div>
    </div>
  </div>



</section>
<?php $this->load->view('v_footer'); ?>
</div>

<?php function tgl_indo($tanggal)
{
  $bulan = array(
    1 => 'Jan',
    'Feb',
    'Mar',
    'Apr',
    'Mei',
    'Jun',
    'Jul',
    'Agust',
    'Sept',
    'Okt',
    'Nov',
    'Des',
  );
  $pecahkan = explode('-', $tanggal);

  // variabel pecahkan 0 = tanggal
  // variabel pecahkan 1 = bulan
  // variabel pecahkan 2 = tahun

  return $pecahkan[2] . ' ' . $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
} ?>

<script>
  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function() {
    scrollFunction()
  };

  function scrollFunction() {
    if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
      document.getElementById("myBtn").style.display = "block";
    } else {
      document.getElementById("myBtn").style.display = "none";
    }
  }

  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
</script>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url() . 'themes/vendor/jquery/jquery.min.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/vendor/bootstrap/js/bootstrap.min.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/vendor/bootstrap/js/popper.min.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/js/slick.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/bootstrap/js/aos.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/vendor/jquery-easing/jquery.easing.min.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/js/jqBootstrapValidation.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/js/contact_me.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/js/agency.min.js' ?>"></script>
</body>

</html>