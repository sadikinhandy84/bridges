<?php
$this->load->view('v_header');
?>

<div class="mt-5 pt-2">
	<section>
		<div class="row">
			<div class="col-lg-12 text-center">

				<h2 class="section-heading text-uppercase">Formulir Pendaftaran</h2>
				<h3 class="section-subheading text-muted">Member Bridge Shooting Club</h3>
				<div class="divider"></div>
			</div>
		</div>


		<div class="row justify-content-center">
			<div class="container">
				<div class="col-md-8">
					<form action="<?= base_url('daftar/simpan_daftar') ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="nama">Nama Lengkap</label>
							<input type="text" class="form-control <?= form_error('nama') ? 'invalid' : '' ?>" id="nama" name="nama" placeholder="Nama Lengkap" value="<?= set_value('nama') ?>">
							<?= form_error('nama', '<small class="text-danger">', '</small>'); ?>
						</div>
						<div class="form-group">
							<label for="nik">Nomor Indentitas (KTP/SIM)</label>
							<input type="text" class="form-control <?= form_error('nik') ? 'invalid' : '' ?>" id="nik" name="nik" value="<?= set_value('nik') ?>" placeholder="NIK KTP/SIM">
							<?= form_error('nik', '<small class="text-danger">', '</small>'); ?>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="tempat">Tempat Lahir</label>
								<input type="tempat" class="form-control <?= form_error('tempat') ? 'invalid' : '' ?>" id="tempat" name="tempat" value="<?= set_value('tempat') ?>" placeholder="Tempat Lahir">
								<?= form_error('tempat', '<small class="text-danger">', '</small>'); ?>
							</div>
							<div class="form-group col-md-6">
								<label for="tglahir">Tanggal Lahir</label>
								<input type="date" class="form-control <?= form_error('tgl') ? 'invalid' : '' ?>" id="tgl" name="tgl" value="<?= set_value('tgl') ?>" placeholder="Tanggal Lahir">
								<?= form_error('tgl', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="nik">Nomor Handphone </label>
							<input type="text" class="form-control <?= form_error('nohp') ? 'invalid' : '' ?>" value="<?= set_value('nohp') ?>" id="nohp" name="nohp" placeholder="Nomor Handphone">
							<?= form_error('nohp', '<small class="text-danger">', '</small>'); ?>
						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputEmail4">Jenis Kelamin</label>
								<div class="col">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="jenkel" id="jenkel" value="L" checked>
										<label class="form-check-label" for="inlineRadio1">Laki-Laki</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="jenkel" id="jenkel" value="P">
										<label class="form-check-label" for="inlineRadio1">Perempuan</label>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="inputUserName">Pekerjaan</label>
							<select class="form-control" name="kerja">
								<option value="ASN/PNS">ASN/PNS</option>
								<option value="Pegawai Swasta">Pegawai Swasta</option>
								<option value="Wiraswasta">Wiraswasta</option>
								<option value="Lainnya">Lainnya</option>
							</select>
						</div>
						<div class="form-group">
							<label for="inputAddress2">Alamat</label>
							<textarea style="min-height: 100px; height: 0px;" name="alamat" id="alamat" placeholder="Alamat" name="alamat" class="form-control <?= form_error('alamat') ? 'invalid' : '' ?>"><?= set_value('alamat') ?></textarea>
							<?= form_error('alamat', '<small class="text-danger">', '</small>'); ?>
						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="prov">Provinsi</label>
								<select name="prov" id="provinsi" class="form-control <?= form_error('prov') ? 'invalid' : '' ?>" value="<?= set_value('prov') ?>">
									<option value=''>- Pilih Provinsi -</option>
									<?php
									foreach ($provinsi as $prov) {
										echo '<option value="' . $prov->id . '">' . $prov->nama . '</option>';
									}
									?>
								</select>
								<?= form_error('prov', '<small class="text-danger">', '</small>'); ?>
							</div>
							<div class="form-group col-md-6">
								<label for="kab">Kota/Kabupaten</label>
								<select name="kab" id="kabupaten" class="form-control <?= form_error('kab') ? 'invalid' : '' ?>" value="<?= set_value('kab') ?>">
									<option value=''>Pilih Kabupaten</option>
								</select>
								<?= form_error('kab', '<small class="text-danger">', '</small>'); ?>
							</div>

						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="kec">Kecamatan</label>
								<select name="kec" id="kecamatan" class="form-control <?= form_error('kec') ? 'invalid' : '' ?>" value="<?= set_value('kec') ?>">
									<option value=''>Pilih Kecamatan</option>
								</select>
								<?= form_error('kec', '<small class="text-danger">', '</small>'); ?>
							</div>
							<div class="form-group col-md-6">
								<label for="des">Desa</label>
								<select name="des" class="form-control <?= form_error('des') ? 'invalid' : '' ?>" value="<?= set_value('des') ?>" id="desa">
									<option value=''>Pilih Desa</option>
								</select>
								<?= form_error('des', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>

						<div class="form-group">
							<label for="inputUserName">Ukuran Baju</label>
							<select class="form-control" name="ukuran">
								<option value="S">S</option>
								<option value="M">M</option>
								<option value="L">L</option>
								<option value="XL">XL</option>
								<option value="XXL">XXL</option>
								<option value="XXXL">XXXL</option>
							</select>
						</div>

						<div class="form-group">
							<label for="inputUserName">Kategori Member</label>
							<select class="form-control" name="status">
								<option value="1">Member Pelajar</option>
								<option value="2">Member Umum</option>
								<option value="3">Member TNI/POLRI</option>
							</select>
						</div>

						<div class="form-group">
							<label for="inputUserName" class="col-sm-4 control-label">Upload Photo</label>
							<input type="file" name="filephoto" accept=".jpg,.png,image/*" />
							<?= form_error('filephoto', '<small class="text-danger">', '</small>'); ?>
						</div>

						<div class="form-group">
							<label for="inputUserName" class="col-sm-4 control-label">Upload KTP</label>
							<input type="file" name="filektp" accept=".jpg,.png,image/*" />
							<?= form_error('filektp', '<small class="text-danger">', '</small>'); ?>
						</div>

						<hr>
						<div style="height:500px; overflow-y: scroll;">
							<p>
							<h5><strong>Syarat</strong></h5><br>
							Menyerahkan Lampiran sebagai berikut : <br>
							1. Foto copy KTP <br>
							2. Pas poto (2 x 3) : 2 lembar , (4 x 6) : 2 lembar <br>
							3. Foto copy SKCK
							</p>
							<p>
							<h5><strong>Ketentuan</strong></h5><br>
							Saya bersedia untuk mentaati segala peraturan yang berlaku, dan saya bersedia untuk
							mengikuti / melaksanakan kegiatan-kegiatan keorganisasian. <br>
							1. Memegang teguh peraturan dan kebijakan yang dikeluarkan oleh PB Perbakin.<br>
							2. Mematuhi segala tata tertib yang dikeluarkan oleh Bridge Shooting Club, dan apabila ada
							pelanggaran berat sehingga berurusan dengan pihak berwajib dan menyangkut dengan
							club maka secara otomatis keluar dari keanggotaan.<br>
							3. Memenuhi hak dan Kewajiban serta tanggung jawab penuh sebagai anggota Bridge
							Shooting Club.<br>
							4. Berperan secara proaktif dalam kegiatan yang positif untuk kepentingan Bridge Shooting
							Club.<br>
							5. Tidak menyalahgunakan identitas Club untuk kepentingan Pribadi ataupun Golongan.<br>
							6. Tidak tercatat pada Club atau komunitas lain yang tercatat dibawah binaan PB Perbakin.<br>
							7. Tidak menggunakan atribut lain Kecuali atribut club dan atribut yang sudah disepakati.<br>
							8. Selalu menjaga keharmonisan dan kerukunan serta mempererat tali silaturahmi antar
							sesama anggota Bridge Shooting Club.<br>
							Bila saya melanggar hal-hal tersebut di atas, saya siap menerima konsekuensinya

							</p>

						</div>


						<div class="form-group">
							<div class="form-check">
								<input class="form-check-input text-bold" name="setuju" type="checkbox" id="gridCheck">
								<label class="form-check-label" for="gridCheck">
									<strong>Setuju dengan ketentuan dan persyaratan diatas.</strong>
								</label>
								<?= form_error('setuju', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
						<button type="submit" class="btn btn-primary">DAFTAR</button>
						<br>
						<br>
						<p>* Catatan <br>
							– Konfirmasi pembayaran dan informasi lebih lanjut dapat melalui chat/telephone Whatsapp <a href="https://api.whatsapp.com/send?phone=<?= substr(intval($dataweb['tlp_web1']), 0, 2) != "62" ? "62" . $dataweb['tlp_web1'] : $dataweb['tlp_web1'] ?>" target="_blank" class="btn btn-primary btn-md"> <i class="fab fa-whatsapp"></i> Konfirmasi</a><br>

							– Untuk proses administrasi, pengambilan kaos, kartu member dll, dapat dilakukan di kantor kesekretariatan BRIDGE SHOOTING CLUB.

						</p>
					</form>
				</div>
			</div>
	</section>
</div>
</div>

<?php $this->load->view('v_footer'); ?>
</div>


<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url() . 'themes/vendor/jquery/jquery.min.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/vendor/bootstrap/js/bootstrap.min.js' ?>"></script>

<script src="<?php echo base_url() . 'themes/js/slick.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/bootstrap/js/aos.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/vendor/jquery-easing/jquery.easing.min.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/js/jqBootstrapValidation.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/js/contact_me.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/js/agency.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>

<!-- Maps -->
<script>
	$(document).ready(function() {
		$("#provinsi").change(function() {
			var url = "<?php echo site_url('daftar/add_ajax_kab'); ?>/" + $(this).val();
			$('#kabupaten').load(url);
			return false;
		})

		$("#kabupaten").change(function() {
			var url = "<?php echo site_url('daftar/add_ajax_kec'); ?>/" + $(this).val();
			$('#kecamatan').load(url);
			return false;
		})

		$("#kecamatan").change(function() {
			var url = "<?php echo site_url('daftar/add_ajax_des'); ?>/" + $(this).val();
			$('#desa').load(url);
			return false;
		})
	});
</script>

<?php if ($this->session->flashdata('msg') == 'error') : ?>
	<script type="text/javascript">
		$.toast({
			heading: 'Error',
			text: "Pendaftaran Gagal",
			showHideTransition: 'slide',
			icon: 'error',
			position: 'mid-center',
			stack: false,
			bgColor: '#FF4859'
		});
	</script>

<?php elseif ($this->session->flashdata('msg') == 'success') : ?>
	<script type="text/javascript">
		$.toast({
			heading: 'Success',
			text: "Pendaftaran Berhasil.",
			showHideTransition: 'slide',
			icon: 'success',
			bgColor: '#7EC857',
			position: 'mid-center',
			stack: false,
		});
	</script>
<?php else : ?>
<?php endif; ?>

</body>

</html>