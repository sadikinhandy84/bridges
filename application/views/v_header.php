<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shorcut icon" href="<?php echo base_url() . 'storage/images/website/' . $dataweb['favicon_pic'] ?>">
  <title><?= $dataweb['nama_web'] ?></title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('themes/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

  <!-- scroll animation -->
  <!--<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">-->

  <link href="<?php echo base_url('assets/bootstrap/css/aos.css'); ?>" rel="stylesheet" type="text/css">

  <!-- Custom fonts for this template -->
  <link href="<?php echo base_url('themes/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
  <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'> -->

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('themes/css/agency.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('themes/css/responsive.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('themes/css/animate.css'); ?>" rel="stylesheet">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/slick/slick.css') ?> ">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/slick/slick-theme.css') ?> "> -->
  <link href="<?php echo base_url('themes/dist/css/lightbox.css'); ?>" rel="stylesheet">
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css" /> -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <style>
    /* .home-slider img {
        width: 100%;
        height: 40vh;
        object-fit: cover; 
        object-position: 50% 50%;
        overflow: hidden;
      } */

    /* form {
      box-sizing: border-box;
      display: flex;
      flex-direction: column;
      gap: 1rem;
      margin: 2em 0;
    } */

    label {
      margin-bottom: 0.5rem;
    }

    /* input,
    textarea {
      display: inline-block;
      padding: 0.5rem;
      width: 100%;
    } */

    input:focus,
    textarea:focus {
      outline-color: dodgerblue;
    }

    .invalid {
      border: 2px solid rgb(153, 16, 16);
    }

    .invalid::placeholder {
      color: rgb(153, 16, 16);
    }

    .invalid-feedback:empty {
      display: none;
    }

    .invalid-feedback {
      font-size: smaller;
      color: rgb(153, 16, 16);
    }

    .item-menu .navbar-toggler {

      font-size: 12px;
      right: 0;
      padding: 13px;
      color: white;
      border: 0;
      background-color: #04c0D1;
      border: 2px solid #e9ecef;
      font-family: "Montserrat", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
    }

    #menu-section {
      position: absolute;
      width: 100%;
      top: 120px;
      z-index: 999;
    }

    .menu-name {
      padding-top: 2px;
    }

    .list-menu {
      display: flex;
      justify-content: space-around;
    }

    .box-slide {
      position: relative;
      display: block;
      top: 0;
      width: 740px;
      height: 425px;
    }

    .slide-body {
      position: relative;
      display: block;
      border-radius: 20px;
      width: 740px;
      height: 400px;
      background: #24C6DC;
      /* fallback for old browsers */
      background: -webkit-linear-gradient(to right, #514A9D, #24C6DC);
      /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to right, #514A9D, #24C6DC);
      /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }

    .slide-body-1 {
      position: relative;
      display: block;
      border-radius: 20px;
      width: 740px;
      height: 400px;
      background: #f12711;
      /* fallback for old browsers */
      background: -webkit-linear-gradient(to right, #f5af19, #f12711);
      /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to right, #f5af19, #f12711);
      /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }

    .slide-text {
      position: absolute;
      display: block;
      width: 400px;
      top: 7rem;
      left: 2rem;
      height: auto;
      color: #fff;
      font-size: 30px;
      padding: 8px;
      font-weight: 600;
    }

    .slide-btn {
      position: absolute;
      display: block;
      top: 15rem;
      left: 40px;
      width: 90px;
      height: 40px;
      color: #fff;
    }

    .slide-btn a {
      display: inline-block;
      color: #fff;
      text-decoration: none;
      text-align: center;
      vertical-align: middle;
      padding: 8px;
      width: 100px;
      border-radius: 40px;
      background: transparent;
      border: 2px solid #fff;
    }

    .slide-btn a:hover {
      color: black;
      background: #fff;
    }

    .slide-body img {
      position: absolute;
      width: 300px;
      height: 300px;
      top: 2rem;
      right: 12px;
    }

    .slide-body-1 img {
      position: absolute;
      width: 300px;
      height: 300px;
      top: 2rem;
      right: 12px;
    }
  </style>
  <!-- Add the slick-theme.css if you want default styling -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css" />
</head>

<body id="page-top">

  <nav class="navbar fixed-top navbar-expand-lg navbar-dark" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="<?= base_url() ?>">
        <img class="img-responsive" src="<?= base_url() . 'storage/images/website/' . $dataweb['logo_pic'] ?>" height="50" class="d-inline-block align-top">
        <span style="word-wrap:break-word"><?= $dataweb['nama_web'] ?></span>
      </a>

      <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarResponsive">

        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
          <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
        </svg>
        <span class="small d-block">Menu</span>


      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?= base_url() ?>">Home</a>
          </li>

          <li class="nav-item dropdown">
            <a href="<?= base_url() . '#about' ?>" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Profile
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="<?= base_url('visimisi') ?>">Visi Misi</a>
              <a class="dropdown-item" href="<?= base_url('about') ?>">Sejarah Singkat</a>

            </div>
          </li>

          <li class="nav-item dropdown">
            <a href="<?= base_url() ?>" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Keanggotaan
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="<?= base_url('daftar') ?>">Pendaftaran</a>
              <a class="dropdown-item" href="<?= base_url('anggota') ?>">Data Anggota</a>
            </div>
          </li>



          <li class="nav-item dropdown">
            <a href="<?= base_url() ?>" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Informasi
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <?php foreach ($newskat->result_array() as $nk) { ?>
                <a class="dropdown-item" href="<?= base_url() . 'berita/kategori/' . $nk['kategori_id'] ?>"><?= $nk['kategori_nama']; ?></a>
              <?php } ?>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="<?= base_url() ?>" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              GALERI
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="<?= base_url() . 'album' ?>">Photo</a>
              <a class="dropdown-item" href="<?= base_url() . 'videoalbum' ?>">Video</a>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?= base_url() . '#contact' ?>">Kontak</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- 
  <nav class="navbar navbar-dark navbar-expand fixed-top d-md-none d-lg-none d-xl-none">
   
       

    <div class="row">
      <ul class="navbar-nav nav-justified w-100">
        <li class="nav-item">
          <a href="#" class="nav-link text-center">
            <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
              <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
            </svg>
            <span class="small d-block">Home</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link text-center">
            <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
              <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
            </svg>
            <span class="small d-block">Home</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link text-center">
            <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
              <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
            </svg>
            <span class="small d-block">Home</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link text-center">
            <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
              <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
            </svg>
            <span class="small d-block">Search</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link text-center">
            <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
            </svg>
            <span class="small d-block">Profile</span>
          </a>
        </li>
      </ul>
    </div>
  </nav> -->