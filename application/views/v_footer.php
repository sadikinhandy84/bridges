 <!-- Footer -->
 <div id="fb-root"></div>
 <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2"></script>

 <footer class="page-footer font-small unique-color-dark">

   <div class="container text-md-left mt-5">
     <!-- Grid row -->
     <div class="row mt-3">
       <!-- Grid column -->
       <div class="col-md-4 col-lg-4 col-xl-3 mx-auto mb-4">
         <!-- Content -->
         <a class="navbar-brand js-scroll-trigger" href="#page-top">
           <img class="img-responsive" height="60" src="<?= base_url() . 'storage/images/website/' . $dataweb['logo_pic'] ?>" alt="logo">
           <?= $dataweb['nama_web'] ?>
         </a>
         <br>
         <p>
           <?= $dataweb['slogan_web'] ?>
         </p>
       </div>
       <!-- Grid column -->


       <!-- Grid column -->
       <div class="col-md-5 col-lg-5 col-xl-4 mx-auto mb-md-0 mb-5">

         <!-- Links -->
         <h6 class="text-uppercase font-weight-bold">Head Office</h6>
         <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
         <p>
           <?= $dataweb['alamat_web'] ?><br>
         
        
           <?= $dataweb['kelurahan_web'] ?> - <?= $dataweb['kecamatan_web'] ?><br>
           <?= $dataweb['kota_web'] ?> - <?= $dataweb['provinsi_web'] ?><br>

         <p>
           <i class="fas fa-envelope mr-3"></i> <?= $dataweb['email_web'] ?>
         </p>
         <?php if ($dataweb['tlp_web1']) { ?>
          <p>
            <i class="fas fa-phone mr-3"></i> <a href="https://api.whatsapp.com/send?phone=<?= substr(intval($dataweb['tlp_web1']),0,2) != "62" ? "62".$dataweb['tlp_web1'] : $dataweb['tlp_web1'] ?>" target="_blank" class="btn btn-primary btn-md">Hubungi Kontak</a>
      
          </p>
          <?php } ?> 

       </div>
    
     </div>
     <!-- Grid row -->
   </div>
 </footer>
 <div class="footer-copyright text-center py-3">
   <span onclick="topFunction()" class="totop"> <i class="fas fa-caret-square-up"></i></span>
   <br>
   Copyright &copy;<script>
     document.write(new Date().getFullYear());
   </script> <a href="<?= $dataweb['facebook_link'] ?>"> <?= $dataweb['nama_web'] ?></a>
 </div>
 <!-- Copyright -->
 <!-- Footer -->

 


 <!-- <footer>
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <img class="img-responsive" height="60" src="<?= base_url() . 'storage/images/website/logo.png' ?>" alt="logo">
            <p><?= $dataweb['alamat_web'] ?></span><br>
            <?= $dataweb['kelurahan_web'] ?><br>
            <?= $dataweb['kecamatan_web'] ?></span><br>
            <?= $dataweb['kota_web'] ?> <?= $dataweb['kodepos_web'] ?><br>
            <strong>Phone:</strong><?= $dataweb['tlp_web1'] ?><br>
            <strong>Email:</strong> <?= $dataweb['email_web'] ?><br>

            </p>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Link</h4>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">Berita</a></li>
              <li><a href="#">Program Sosial</a></li>
              <li><a href="#">Pendaftaran</a></li>
            </ul>
          </div>


        <div class="col-lg-4 col-md-6 footer-links">
            <h4>Sosial Media</h4>
            <p>Tetap Terhubung dengan informasi Al-Wasilah Lilhasanah melalui sosial media</p>
          <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <?php if ($dataweb['instagram_link']) { ?>
                <a href="<?= $dataweb['instagram_link'] ?>">
                  <i class="fab fa-instagram"></i>
                </a>
                <?php } else {
                ?>
                <a href="#">
                  <i class="fab fa-instagram"></i>
                </a>
                <?php } ?>
              </li>
              <li class="list-inline-item">
                <?php if ($dataweb['facebook_link']) { ?>
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
                <?php
                } else { ?>
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
                <?php } ?>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-youtube"></i>
                </a>
              </li>
            </ul>
            
                <div class="row">
              <div class="col-md-4">
                <div class="info-box bg-aqua">
                  <span class="info-box-icon"><i class="fa fa-users"></i></span>
                  <?php
                  $query = $this->db->query("SELECT * FROM tbl_pengunjung WHERE DATE_FORMAT(pengunjung_tanggal,'%m%y')=DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 MONTH),'%m%y')");
                  $jml = $query->num_rows();

                  ?>
                  
                  <span class="info-box-number"><?php echo number_format($jml); ?></span>
                  
                  <div class="info-box-content">
                    <span class="info-box-text">Pengunjung Bulan Lalu</span>
                  </div>
                </div>
            </div>

            <div class="col-md-4">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-users"></i></span>
                <?php
                $query = $this->db->query("SELECT * FROM tbl_pengunjung WHERE DATE_FORMAT(pengunjung_tanggal,'%m%y')=DATE_FORMAT(CURDATE(),'%m%y')");
                $jml = $query->num_rows();
                ?>
                
                  <span class="info-box-number"><?php echo number_format($jml); ?></span>
                
                <div class="info-box-content">
                <span class="info-box-text">Pengunjung Bulan Ini</span>
                </div>
              </div>
            </div>

            <div class="col-md-4">
                <div class="info-box bg-aqua">
                  <span class="info-box-icon"><i class="fa fa-users"></i></span>
                  <?php
                  $query = $this->db->query("SELECT * FROM tbl_pengunjung");
                  $jml = $query->num_rows();
                  ?>
                <span class="info-box-number"><?php echo number_format($jml); ?></span>
                    
                    <div class="info-box-content">
                    <span class="info-box-text">Total Pengunjung</span>
                    </div>
                </div>
            </div>
        </div>

            
          </div>

          <div class="col-lg-4 col-md-6 footer-contact">
            <div class="fb-page"
            data-tabs="timeline,events,messages"
            data-href="<?= $dataweb['facebook_link'] ?>"
            data-height="300"
            data-width="300"
            data-hide-cover="false">
            </div>
          </div>



        </div>
      </div>
    </div>

      <div class="footerbottom">
        <div class="row">
          <div class="col-md-12">
          <span class="copyright">Copyright &copy;<script>document.write(new Date().getFullYear());</script><a href="<?= $dataweb['link_web'] ?>" target="_blank"> <?= $dataweb['nama_web'] ?></a></span>
      
          </div>
        </div>
      </div>

  </footer>


 -->