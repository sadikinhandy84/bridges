
<?php
error_reporting(0);
      $url='';
	    $img='';
	    $title='';
	    $author='';
	    $date='';
	    $kategori='';
	    $deskripsi='';
	    $isi='';
	    $views='';
	    $rating='';
    ?>
<?php
$this->load->view('v_header');
?>

<div class="mt-5 pt-2">
<?php
  $this->load->view('v_toolbar');
  ?>
</div>

    <button class="fa fa-angle-up" onclick="topFunction()" id="myBtn" title="Go to top"></button>

    <section class="bg-light" id="portfolio">
      <div class="container">
          <div class="row mt-5">
              <?php $namakategori;?>
                <div class="col-lg-12 text-center pb__20">
                    <h2 class="text-uppercase"><?=$namakategori?></h2>
                    <div class="divider"></div>
                </div>
          </div>

          <div class="row">
            
            <?php foreach ($data->result_array() as $j) {
    $tgl = date('Y-m-d', strtotime($j['tulisan_tanggal']))
    ?>

              <div class="col-6 col-md-3 col-sm-3 portfolio-item">
                <a class="portfolio-link" href="<?php echo base_url() . 'artikel/' . $j['tulisan_slug']; ?>">
                  <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                      <i class="fa fa-crosshairs fa-3x"></i>
                    </div>
                  </div>
                  <img class="img-fluid" src="<?php echo base_url() . 'storage/images/berita/' . $j['tulisan_gambar'] ?>" alt="">
                </a>
                <div class="portfolio-caption">
                    <a href="<?php echo base_url() . 'artikel/' . $j['tulisan_slug']; ?>"><span class="icon"></span></a>
                    <div class="portfolio-caption">
                        <h5 class="service-heading"><?=$j['tulisan_judul']?></h5>
                       <span class="author"></i><?=tgl_indo($tgl) . ' | ' . $j['tulisan_author']?> </span><br>
                        <p><a href="<?php echo base_url() . 'artikel/' . $j['tulisan_slug']; ?>" class="btn btn-primary with-arrow">Baca lebih lanjut<i class="icon-arrow-right"></i></a></p>
                    </div>
                </div>
              </div>
            <?php }?>
          </div>

          <div class="row">
            <div class="col-lg-12 text-center pt__20">
                    <center><?php echo $pagination; ?></center>
            </div>
            </div>
        </div>
      </section>
  <?php $this->load->view('v_footer');?>
</div>

<?php function tgl_indo($tanggal)
{
    $bulan = array(
        1 => 'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Agust',
        'Sept',
        'Okt',
        'Nov',
        'Des',
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
}?>

<script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
    document.getElementById("myBtn").style.display = "block";
  } else {
    document.getElementById("myBtn").style.display = "none";
  }
}


// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}



</script>

<!-- Bootstrap core JavaScript -->
 <script src="<?php echo base_url().'themes/vendor/jquery/jquery.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/bootstrap.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/popper.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/js/slick.js'?>"></script>
 <script src="<?php echo base_url().'assets/bootstrap/js/aos.js'?>"></script>
<script src="<?php echo base_url().'themes/vendor/jquery-easing/jquery.easing.min.js'?>"></script>
<script src="<?php echo base_url().'themes/js/jqBootstrapValidation.js'?>"></script>
<script src="<?php echo base_url().'themes/js/contact_me.js'?>"></script>
<script src="<?php echo base_url().'themes/js/agency.min.js'?>"></script>
</body>
</html>