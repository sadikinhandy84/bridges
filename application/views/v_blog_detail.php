<?php
error_reporting(0);
$b = $data->row_array();
$url = base_url() . 'artikel/' . $b['tulisan_slug'];
$img = base_url() . 'storage/images/berita/' . $b['tulisan_gambar'];
$title = $b['tulisan_judul'];
$author = $b['tulisan_author'];
$date = $b['tanggal'];
$kategori = $b['tulisan_kategori_nama'];
$deskripsi = strip_tags($b['tulisan_isi']);
$isi = $b['tulisan_isi'];
$views = $b['tulisan_views'];
$rating = $b['tulisan_rating'];
?>
<!DOCTYPE html>
<html class="no-js">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shorcut icon" type="text/css" href="<?php echo base_url() . 'storage/images/website/favicon.png' ?>">


	<title><?= $dataweb['nama_web'] ?></title>


	<meta property="og:locale" content="id_id" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="<?php echo $title; ?>" />
	<meta property="og:description" content="<?php echo $deskripsi; ?>" />
	<meta property="og:url" content="<?php echo $url; ?>" />
	<meta property="og:site_name" content="alwasilahlilhasanah.org" />

	<meta property="article:section" content="<?php echo $author; ?>" />
	<meta property="og:image" content="<?php echo $img; ?>" />
	<meta property="og:image:width" content="460" />
	<meta property="og:image:height" content="440" />

	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url('themes/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

	<!-- scroll animation -->
	<!--<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">-->

	<link href="<?php echo base_url('assets/bootstrap/css/aos.css'); ?>" rel="stylesheet" type="text/css">

	<!-- Custom fonts for this template -->
	<link href="<?php echo base_url('themes/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
	<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'> -->

	<!-- Custom styles for this template -->
	<!-- <link href="<?php echo base_url('themes/css/agency.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('themes/css/responsive.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('themes/css/animate.css'); ?>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/slick/slick.css') ?> ">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/slick/slick-theme.css') ?> ">
	<link href="<?php echo base_url('themes/dist/css/lightbox.css'); ?>" rel="stylesheet"> -->

	<style>
		.box-slide {
			position: relative;
			display: block;
			top: 0;
			width: 740px;
			height: 425px;
		}

		.slide-body {
			position: relative;
			display: block;
			border-radius: 20px;
			width: 740px;
			height: 400px;
			background: #24C6DC;
			/* fallback for old browsers */
			background: -webkit-linear-gradient(to right, #514A9D, #24C6DC);
			/* Chrome 10-25, Safari 5.1-6 */
			background: linear-gradient(to right, #514A9D, #24C6DC);
			/* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
		}

		.slide-body-1 {
			position: relative;
			display: block;
			border-radius: 20px;
			width: 740px;
			height: 400px;
			background: #f12711;
			/* fallback for old browsers */
			background: -webkit-linear-gradient(to right, #f5af19, #f12711);
			/* Chrome 10-25, Safari 5.1-6 */
			background: linear-gradient(to right, #f5af19, #f12711);
			/* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
		}

		.slide-text {
			position: absolute;
			display: block;
			width: 400px;
			top: 7rem;
			left: 2rem;
			height: auto;
			color: #fff;
			font-size: 30px;
			padding: 8px;
			font-weight: 600;
		}

		.slide-btn {
			position: absolute;
			display: block;
			top: 15rem;
			left: 40px;
			width: 90px;
			height: 40px;
			color: #fff;
		}

		.slide-btn a {
			display: inline-block;
			color: #fff;
			text-decoration: none;
			text-align: center;
			vertical-align: middle;
			padding: 8px;
			width: 100px;
			border-radius: 40px;
			background: transparent;
			border: 2px solid #fff;
		}

		.slide-btn a:hover {
			color: black;
			background: #fff;
		}

		.slide-body img {
			position: absolute;
			width: 300px;
			height: 300px;
			top: 2rem;
			right: 12px;
		}

		.slide-body-1 img {
			position: absolute;
			width: 300px;
			height: 300px;
			top: 2rem;
			right: 12px;
		}
	</style>
	<!-- Add the slick-theme.css if you want default styling -->
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css" />
</head>

<body id="page-top">

<?php
$this->load->view('v_header');
?>
	<div class="fh5co-pricing">
		<div class="container" style="padding-top:70px;">
			<section>

				<div class="row">

					<div class="col-md-8">
						<h1 style="margin-bottom:0px;"><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h1>
						<small><em>Posted by: <?php echo $author; ?> | <?php echo $date; ?> | Kategori: <?php echo $kategori; ?> | <?php echo $views; ?> kali dibaca | Rating: <?php echo $rating; ?></em></small>
						<figure>
							<img src="<?php echo $img; ?>" alt="" class="img-responsive">
						</figure>
						<?php echo $isi; ?>
						<!-- <?php if ($rate->num_rows() > 0) : ?>
						<?php else : ?>
							<div class="alert alert-primary">
								<strong>Apakah pendapat Anda tentang artikel ini?</strong><br /><br />
								<a class="btn btn-sm" href="<?php echo base_url() . 'berita/good/' . $b['tulisan_slug']; ?>" title="Good"> <i class="far fa-smile fa-2x"></i></a>
								<a class="btn btn-sm" href="<?php echo base_url() . 'berita/like/' . $b['tulisan_slug']; ?>" title="Like"><i class="far fa-thumbs-up fa-2x"></i></a>
								<a class="btn btn-sm" href="<?php echo base_url() . 'berita/love/' . $b['tulisan_slug']; ?>" title="Love"><i class="far fa-heart fa-2x"></i></a>
								<a class="btn btn-sm" href="<?php echo base_url() . 'berita/genius/' . $b['tulisan_slug']; ?>" title="Genius"><i class="far fa-lightbulb fa-2x"></i></a>

							</div>
						<?php endif; ?>
						<h4>Share:</h4>

						<div class="pb__20">
							<a class="popup2 btn btn-primary btn-sm" href="https://plus.google.com/share?url=<?php echo $url; ?>" title="Bagikan ke Google+"><i class="fab fa-google-plus-g"></i> Google+</a>
							<a class="popup2 btn btn-primary btn-sm" target="_parent" href="https://www.facebook.com/dialog/share?app_id=966242223397117&display=popup&href=<?php echo $url; ?>" title="Bagikan ke Facebook"><i class="fab fa-facebook-f"></i> Facebook</a>
							<a class="popup2 btn btn-primary btn-sm" href="http://twitter.com/share?source=sharethiscom&text=<?php echo $b['tulisan_judul']; ?>&url=<?php echo $url; ?>&via=badoey" title="Bagikan ke Twitter"><i class="fab fa-twitter"></i> Twitter</a>
						</div> -->
					</div>
					<div class="col-md-4">
						<form class="search_form" action="<?php echo base_url() . 'berita/search' ?>" method="post">
							<input type="text" name="xfilter" class="form-control" placeholder="Search" required>
							<button type="submit" id="btncari"></button>
						</form>
						<br />

						<h4>KATEGORI</h4>
						<div style="border-bottom: 1px #ccc solid;margin-top:-20px;margin-bottom:20px;"></div>
						<ul class="list-unstyled">
							<?php foreach ($kat->result() as $i) : ?>
								<li><a href="<?php echo base_url() . 'berita/kategori/' . $i->kategori_id; ?>"><?php echo $i->kategori_nama . ' (' . $i->jml . ')'; ?></a></li>
							<?php endforeach; ?>
						</ul>
						<br />
						<h4>POST POPULER</h4>
						<div style="border-bottom: 1px #ccc solid;margin-top:-20px;margin-bottom:20px;"></div>
						<?php foreach ($populer->result() as $row) : ?>
							<div class="media">
								<div class="media-left">
									<a href="<?php echo base_url() . 'artikel/' . $row->tulisan_slug; ?>">
										<img class="media-object" src="<?php echo base_url() . 'storage/images/berita/' . $row->tulisan_gambar; ?>" width="90">
									</a>
								</div>
								<div class="media-body">
									<h5 class="media-heading"><a href="<?php echo base_url() . 'artikel/' . $row->tulisan_slug; ?>"><?php echo $row->tulisan_judul; ?></a></h5>
									<span><small><i>by: <?php echo $row->tulisan_author; ?> | <?php echo $row->tanggal; ?></i></small></span>
								</div>
							</div>
						<?php endforeach; ?>

						<br />

						<h4>POST TERBARU</h4>
						<div style="border-bottom: 1px #ccc solid;margin-top:-20px;margin-bottom:20px;"></div>
						<?php foreach ($terbaru->result() as $row) : ?>
							<div class="media">
								<div class="media-left">
									<a href="<?php echo base_url() . 'artikel/' . $row->tulisan_slug; ?>">
										<img class="media-object" src="<?php echo base_url() . 'storage/images/berita/' . $row->tulisan_gambar; ?>" width="90">
									</a>
								</div>
								<div class="media-body">
									<h5 class="media-heading"><a href="<?php echo base_url() . 'artikel/' . $row->tulisan_slug; ?>"><?php echo $row->tulisan_judul; ?></a></h5>
									<span><small><i>by: <?php echo $row->tulisan_author; ?> | <?php echo $row->tanggal; ?></i></small></span>
								</div>
							</div>
						<?php endforeach; ?>

					</div>


				</div>


		</div>
	</div>

	</section>

	<?php function tgl_indo($tanggal)
	{
		$bulan = array(
			1 =>   'Jan',
			'Feb',
			'Mar',
			'Apr',
			'Mei',
			'Jun',
			'Jul',
			'Agust',
			'Sept',
			'Okt',
			'Nov',
			'Des'
		);
		$pecahkan = explode('/', $tanggal);

		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun

		return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
	} ?>


	<?php $this->load->view('v_footer'); ?>
	</div>


	<script>
		// When the user scrolls down 20px from the top of the document, show the button
		window.onscroll = function() {
			scrollFunction()
		};

		function scrollFunction() {
			if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
				document.getElementById("myBtn").style.display = "block";
			} else {
				document.getElementById("myBtn").style.display = "none";
			}
		}

		// When the user clicks on the button, scroll to the top of the document
		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>


	<!-- Bootstrap core JavaScript -->
	</script>

    <script src="<?php echo base_url().'themes/vendor/jquery/jquery.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/bootstrap.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/popper.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/js/slick.js'?>"></script>
 <script src="<?php echo base_url().'assets/bootstrap/js/aos.js'?>"></script>
<script src="<?php echo base_url().'themes/vendor/jquery-easing/jquery.easing.min.js'?>"></script>
<script src="<?php echo base_url().'themes/js/jqBootstrapValidation.js'?>"></script>
<script src="<?php echo base_url().'themes/js/contact_me.js'?>"></script>
<script src="<?php echo base_url().'themes/js/agency.min.js'?>"></script>

	<!-- MAIN JS -->
	<script src="<?php echo base_url() . 'theme/js/main.js' ?>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#btncari').hide();
		});
	</script>
	<script>
		jQuery(document).ready(function($) {
			$('.popup2').click(function(event) {
				var width = 575,
					height = 400,
					left = ($(window).width() - width) / 2,
					top = ($(window).height() - height) / 2,
					url = this.href,
					opts = 'status=1' +
					',width=' + width +
					',height=' + height +
					',top=' + top +
					',left=' + left;
				window.open(url, 'facebook', opts);
				return false;
			});
		});
	</script>

</body>

</html>