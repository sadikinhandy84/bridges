
<?php
	$this->load->view('v_header');
?>
    
		<section class="v2__features pb__80 pt__100 bg__dark overlay" id="features" style="background-position: 90% -11px;">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="v2_title pb__70 text-center">
							<h2 style="color:white">Why AL-`UNAIZY</h2>
						</div>
						<div class="container">
						
						
							<div class="col-md-3">
								<div class="row">
									<div class="col-md-12">
										<ul class="feaures__block">
											<li><i class="fa fa-code"></i>
											</li>
											<li>
												<h5>Superior generation</h5>
											</li>
											<li>
											    <p>
													Implementing Islamic Education optimally to give birth to superior generations in aqeedah, worship and Islamic morality.
												</p>
												<!--<p>Melaksanakan Pendidikan secara Islam optimal untuk melahirkan generasi unggul dalam aqidah,ibadah dan akhlak Islami. </p>-->
											</li>
										</ul>
									</div>
								</div>
						  
								<div class="row">
									<div class="col-md-12">
										<ul class="feaures__block">
											<li><i class="fa fa-home"></i>
											</li>
											<li>
												<h5>Education</h5>
											</li>
											<li>
												<p>1.Carry out quality education.</p>
												<p>2.Instill Ahlusunnah Aqeedah with the understanding of Salafusshalih.<!--.</p-->
												</p><p>3.Fostering true pray and noble character.</p>
												<p>4.Instilling love with the Qur'an and the Sunnah.</p>
												<p>5.Directing General Sciences and English.</p>
											</li>
											<!--<li>
												<p>1. Menyelengarakan pendidikan yang berkualitas.</p>
												<p>2. Menanamkan Aqidah Ahlusunnah dengan pemahaman Salafusshalih.</p>
												<p>3. Membina Ibadah Yang benar dan akhlak mulia.</p>
												<p>4. Menanamkan cinta kepada Al-qur`an dan As-Sunnah.</p>
												<p>5. Mengarahkan Ilmu-ilmu umum dan bahsa Inggris.</p>
											</li>-->
											
										</ul>
									</div>
								</div>
								<!-- feaures block end -->
							</div>
							
							<div class="col-md-6">
								<img class="v2__mockup" src="images/Ipad1.png" alt="" style="will-change: transform; transform: perspective(300px) rotateX(0deg) rotateY(0deg);">
							</div>
							
							<div class="col-md-3">
								<div class="row">
									<div class="col-md-12">
										<ul class="feaures__block">
										<li><i class="fa fa-television"></i>
										</li>
										<li>
											<h5>Fullday School Activities</h5>
										</li>
										<li>
											<p>Fullday School | Tahfiz AL-Qur`an 3 Juz | Arab and English Language | pray practice | Outing class | Character Building | Haflah Imtihan Qailulah | Manhaj Salafus Shalih </p>
										</li>
										</ul>									
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<ul class="feaures__block">
											<li><i class="fa fa-support"></i>
											</li>
											<li>
												<h5>Teacher</h5>
											</li>
											<!--<li>
												<p>Dididik dan dibimbing oleh guru-guru yang berpengalaman pendidikan dan berkompetensi dibidangnya dari berbabagai background pendidikan.</p>
											</li>-->
											<li>
												<p>Educated and guided by teachers who are experienced in education and competency in their fields from various educational backgrounds.</p>
											</li>
										</ul>
									</div>
								</div>
								<!-- feaures block end -->
							</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
	</div>

<?php 
	$this->load->view('v_footer');?>
</div>

<script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
    document.getElementById("myBtn").style.display = "block";
  } else {
    document.getElementById("myBtn").style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url('themes/default2/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('themes/default2/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('themes/default2/vendor/bootstrap/js/popper.min.js')?>"></script>
  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('themes/default2/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Contact form JavaScript -->
  <script src="<?php echo base_url('themes/default2/js/jqBootstrapValidation.js')?>"></script>
  <script src="<?php echo base_url('themes/default2/js/contact_me.js')?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('themes/default2/js/agency.min.js')?>"></script>
  
</body>
</html>
