<section class="content">
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="glyphicon glyphicon-th"></i>ADD ANGGOTA</h3>
                    <div class="pull-right">
                        <a href="https://bridgesnew.app/admin/Dashboard" class="btn btn-sm btn-danger">
                            <span class="fa fa-mail-forward"></span> Keluar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="<?php echo base_url() . 'admin/anggota/simpan_anggota' ?>" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="col-md-8">
                            <div class=" form-group row">
                                <label class="col-sm-4 control-label">No KTA</label>
                                <div class="col-sm-7">
                                    <input type="text" name="nokta" class="form-control" id="inputUserName" placeholder="Nomor KTA" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">NIK KTP</label>
                                <div class="col-sm-7">
                                    <input type="text" name="nik" class="form-control" placeholder="NIK KTP" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Lengkap</label>
                                <div class="col-sm-7">
                                    <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">Tempat,Tanggal Lahir</label>
                                <div class="col-sm-3">
                                    <input type="text" name="tempat" class="form-control" placeholder="Tempat Lahir" required>
                                </div>
                                <div class="col-sm-4">
                                    <input type="date" name="tgl" class="form-control" placeholder="Nama Lengkap" required>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">Jenis Kelamin</label>
                                <div class="col-sm-7">
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="inlineRadio1" value="L" name="jenkel" checked>
                                        <label for="inlineRadio1"> Laki-Laki </label>
                                    </div>
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="inlineRadio1" value="P" name="jenkel">
                                        <label for="inlineRadio2"> Perempuan </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label col-md-4" for="alamat">Alamat</label>
                                <div class="col-md-7">
                                    <textarea style="min-height: 100px; height: 0px;" class="form-control" placeholder="Alamat" name="alamat"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prov" class="col-sm-4 control-label">Provinsi</label>
                                <div class="col-md-7">
                                    <select name="prov" id="provinsi" class="form-control <?= form_error('prov') ? 'invalid' : '' ?>" value="<?= set_value('prov') ?>">
                                        <option value=''>- Pilih Provinsi -</option>
                                        <?php
                                        foreach ($provinsi as $item) {
                                            if ($item->id == $prov) {
                                                $selected = " selected";
                                            } else {
                                                $selected = "";
                                            }
                                            echo '<option value="' . $item->id . '"' . $selected . '>' . $item->nama . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="kab" class="col-sm-4 control-label">Kota/Kabupaten</label>
                                <div class="col-md-7">
                                    <select name="kab" id="kabupaten" class="form-control <?= form_error('kab') ? 'invalid' : '' ?>" value="<?= set_value('kab') ?>">
                                        <option value=''>Pilih Kabupaten</option>
                                    </select>
                                    <?= form_error('kab', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kec" class="col-sm-4 control-label">Kecamatan</label>
                                <div class="col-md-7">
                                    <select name="kec" id="kecamatan" class="form-control <?= form_error('kec') ? 'invalid' : '' ?>" value="<?= set_value('kec') ?>">
                                        <option value=''>Pilih Kecamatan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="des" class="col-sm-4 control-label">Desa</label>
                                <div class="col-md-6">
                                    <select name="des" class="form-control <?= form_error('des') ? 'invalid' : '' ?>" value="<?= set_value('des') ?>" id="desa">
                                        <option value=''>Pilih Desa</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">Pekerjaan</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="kerja">
                                        <option value="ASN/PNS">ASN/PNS</option>
                                        <option value="Pegawai Swasta">Pegawai Swasta</option>
                                        <option value="Wiraswasta">Wiraswasta</option>
                                        <option value="Lainnya">Lainnya</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">No Hp</label>
                                <div class="col-sm-7">
                                    <input type="text" name="nohp" class="form-control" id="inputUserName" placeholder="No Hp" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">Ukuran Baju</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="ukuran" required>
                                        <option value="S">S</option>
                                        <option value="M">M</option>
                                        <option value="L">L</option>
                                        <option value="XL">XL</option>
                                        <option value="XXL">XXL</option>
                                        <option value="XXXL">XXXL</option>
                                    </select>
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">Kategori Member</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="member">
                                        <option value="1">Member Pelajar</option>
                                        <option value="2">Member Umum</option>
                                        <option value="3">Member TNI/POLRI</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">Masa berlaku kartu sampai</label>
                                <div class="col-sm-7">
                                    <input type="date" name="tglkartu" class="form-control" placeholder="Masa Berlaku Kartu" required>
                                </div>
                            </div>

                            
                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">Photo</label>
                                <div class="col-sm-7">
                                    <input type="file" accept=".jpg,.png,image/*" name="filefoto" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">Kartu</label>
                                <div class="col-sm-7">
                                    <input type="file" accept=".jpg,.png,image/*" name="filekartu" />
                                </div>
                            </div>

                            
                            <div class="form-group row">
                                <label for="inputUserName" class="col-sm-4 control-label">KTP</label>
                                <div class="col-sm-7">
                                    <input type="file" accept=".jpg,.png,image/*" name="filektp" />
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                    </div>
                </div>
            </div>
        </div>


    </form>
</section>


<!-- /.content -->
</div>

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
<!-- page script -->
<script>
    $(document).ready(function() {

        $("#provinsi").change(function() {
            console.log("ok");
            var url = "<?php echo site_url('admin/anggota/add_ajax_kab'); ?>/" + $(this).val();
            $('#kabupaten').load(url);
            return false;
        })

        $("#kabupaten").change(function() {
            var url = "<?php echo site_url('admin/anggota/add_ajax_kec'); ?>/" + $(this).val();
            $('#kecamatan').load(url);
            return false;
        })

        $("#kecamatan").change(function() {
            var url = "<?php echo site_url('admin/anggota/add_ajax_des'); ?>/" + $(this).val();
            $('#desa').load(url);
            return false;
        })
    });
</script>
<script>
    $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });


    });
</script>
<?php if ($this->session->flashdata('msg') == 'error') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Error',
            text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
            showHideTransition: 'slide',
            icon: 'error',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#FF4859'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'warning') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Warning',
            text: "Gambar yang Anda masukan terlalu besar.",
            showHideTransition: 'slide',
            icon: 'warning',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#FFC017'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'success') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Success',
            text: "Anggota Berhasil disimpan ke database.",
            showHideTransition: 'slide',
            icon: 'success',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#7EC857'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'info') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Info',
            text: "Anggota berhasil di update",
            showHideTransition: 'slide',
            icon: 'info',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#00C9E6'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Success',
            text: "Anggota Berhasil dihapus.",
            showHideTransition: 'slide',
            icon: 'success',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#7EC857'
        });
    </script>




<?php else : ?>

    <?php endif; ?>>