<?php
	//--jika header di set ,maka tampilkan
	if (isset($header))
	{
		$this->load->view($header);
	}

	//jika side bar diset, maka tampilkan
	
	if (isset($sidebar))
	{
		$this->load->view($sidebar);
	}
	//jika content diset ,maka tampikan;
	
	if (isset($content))
	{
	?>
	 <div class="content-wrapper">
    <?php
		 $this->load->view($content);
	}

	if (isset($footer))
	{
		 $this->load->view($footer);
	}
?>
