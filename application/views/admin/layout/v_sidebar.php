<?php 
    $query=$this->db->query("SELECT * FROM tbl_inbox WHERE inbox_status='1'");
    $jum_pesan=$query->num_rows();
    $query1=$this->db->query("SELECT * FROM tbl_komentar WHERE komentar_status='0'");
    $jum_komentar=$query1->num_rows();

?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU UTAMA</li>
		
		
        <li>
          <a href="<?php echo base_url().'admin/dashboard'?>">
            <i class="fa fa-home"></i> <span>DASHBOARD</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
	    	<li class="treeview ">
          <a href="#">
            <i class="fa fa-cogs"></i>
              <span>PENGATURAN</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li>
            <a href="<?php echo base_url().'admin/pengaturan'?>"><i class="fa fa-cogs"></i> <span>Umum</span></a>
            </li>
            <li>
              <a href="<?php echo base_url().'admin/pengguna'?>"><i class="fa fa-users"></i> <span>Pengguna</span></a>
            </li>
             <li>
              <a href="<?php echo base_url().'admin/slider'?>"><i class="fa fa-users"></i> <span>Slider</span></a>
            </li>
          </ul>
        </li>

        <li class="treeview ">
          <a href="#">
            <i class="fa fa-folder" aria-hidden="true"></i>
              <span>DATA</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li>
            <a href="<?php echo base_url().'admin/anggota'?>"><i class="fa fa-users"></i> <span>Anggota</span></a>
            </li>
          </ul>
        </li>


        <li class="treeview ">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>BERITA & INFO</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/tulisan/add_tulisan'?>"><i class="fa fa-thumb-tack"></i> Tambah Berita & Info</a></li>
            <li><a href="<?php echo base_url().'admin/tulisan'?>"><i class="fa fa-list"></i> List Berita & Info</a></li>
            <li><a href="<?php echo base_url().'admin/kategori'?>"><i class="fa fa-wrench"></i> Kategori Berita & Info</a></li>
            
          </ul>
        </li>
	
       <li class="treeview">
          <a href="#">
            <i class="fa fa-code"></i>
            <span>PARTNER</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/partner'?>"><i class="fa fa-list"></i> Partner</a></li>
          </ul>
        </li>
       
        <li class="treeview">
          <a href="#">
            <i class="fa fa-camera"></i>
            <span>GALERI</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/album'?>"><i class="fa fa-clone"></i> Kategori Photo</a></li>
            <li><a href="<?php echo base_url().'admin/galeri'?>"><i class="fa fa-picture-o"></i> Photos</a></li>
            <li><a href="<?php echo base_url().'admin/videokat'?>"><i class="fa fa-youtube-play"></i> Kategori Video</a></li>
            <li><a href="<?php echo base_url().'admin/video'?>"><i class="fa fa-youtube-play"></i> Videos</a></li>
          </ul>
        </li>

        <li>
          <a href="<?php echo base_url().'admin/komentar'?>">
            <i class="fa fa-comment"></i> <span>KOMENTAR</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $jum_komentar;?></small>
            </span>
          </a>
        </li>
        
        <li>
          <a href="<?php echo base_url().'admin/inbox'?>">
            <i class="fa fa-envelope"></i> <span>INBOX</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $jum_pesan;?></small>
            </span>
          </a>
        </li>

         <li>
          <a href="<?php echo base_url().'administrator/logout'?>">
            <i class="fa fa-sign-out"></i> <span>SIGN OUT</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

 