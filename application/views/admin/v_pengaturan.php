    <section class="content">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"><i class="glyphicon glyphicon-th"></i> Pengaturan</h3>
                            <div class="pull-right">
                                <a href="<?php echo base_url('admin/Dashboard'); ?>"  class="btn btn-sm btn-danger" >
                                <span class="fa fa-mail-forward"></span> Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">

                            <?php if ($data->num_rows() > 0) {
    $row = $data->row();
    $id = $row->id_web;
    ?>
                                <form action="<?php echo base_url('admin/pengaturan/ubah_website') ?>" method="post" enctype="multipart/form-data" >
                                    <input readonly type="hidden" class="form-control" name="idweb" value="<?php echo $row->id_web; ?>">
                                <div class="col-md-12">
                                      <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label col-md-3" for="namaweb">Nama Website</label>
                                            <div class="col-md-9">
                                                <input type="text" name="namaweb" id="namaweb" class="form-control" placeholder="Nama Website" value="<?php echo $row->nama_web; ?>"">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                                <label class="control-label col-md-3" for="keteranganweb">Keterangan Website</label>
                                                <div class="col-md-9">
                                                    <textarea style="min-height: 100px; height: 0px;" class="form-control" placeholder="Keterangan Website" name="keteranganweb" ><?php echo $row->slogan_web; ?>
                                                    </textarea>
                                                </div>
                                        </div>

                                        

                                        <div class="form-group row">
                                                <label class="control-label col-md-3" for="alamatweb">Alamat Website</label>
                                                <div class="col-md-9">
                                                    <textarea style="min-height: 70px; height: 0px;" class="form-control" placeholder="Alamat Website" name="alamatweb" ><?php echo $row->alamat_web; ?>
                                                    </textarea>
                                                </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label col-md-3" for="kelurahanweb">Kelurahan</label>
                                            <div class="col-md-9">
                                                <input type="text" name="kelurahanweb" id="kelurahanweb" class="form-control" placeholder="Kelurahan" value="<?php echo $row->kelurahan_web; ?>">
                                            </div>
                                        </div>

                                       <div class="form-group row">
                                            <label class="control-label col-md-3" for="kecamatanweb">Kecamatan</label>
                                            <div class="col-md-9">
                                                <input type="text" name="kecamatanweb" id="kecamatanweb" class="form-control" placeholder="Kecamatan" value="<?php echo $row->kecamatan_web; ?>">
                                            </div>
                                        </div>

                                         <div class="form-group row">
                                            <label class="control-label col-md-3" for="kotaweb">Kota</label>
                                            <div class="col-md-9">
                                                <input type="text" name="kotaweb" id="kotaweb" class="form-control" placeholder="Kota" value="<?php echo $row->kota_web; ?>">
                                            </div>
                                        </div>

                                         <div class="form-group row">
                                            <label class="control-label col-md-3" for="provinsiweb">Provinsi</label>
                                            <div class="col-md-9">
                                                <input type="text" name="provinsiweb" id="provinsiweb" class="form-control" placeholder="Provinsi" value="<?php echo $row->provinsi_web; ?>">
                                            </div>
                                        </div>

                                         <div class="form-group row">
                                            <label class="control-label col-md-3" for="kodeposweb">Kodepos</label>
                                            <div class="col-md-9">
                                                <input type="text" name="kodeposweb" id="kodeposweb" class="form-control" placeholder="Kode Pos" value="<?php echo $row->kodepos_web; ?>">
                                            </div>
                                        </div>

                                       <div class="form-group row">
                                            <label class="control-label col-md-3" for="longitude">Longitude</label>
                                            <div class="col-md-9">
                                                <input type="text" name="longitude" id="longitude" class="form-control" placeholder="Longitude" value="<?php echo $row->longitude; ?>">
                                            </div>
                                        </div>
                                       <div class="form-group row">
                                            <label class="control-label col-md-3" for="latitude">Latitude</label>
                                            <div class="col-md-9">
                                                <input type="text" name="latitude" id="latitude" class="form-control" placeholder="Latitude" value="<?php echo $row->latitude; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label col-md-3" for="maps">Peta Link Embed</label>
                                                <div class="col-md-9">
                                                    <textarea style="min-height: 300px; height: 0px;" class="form-control" placeholder="Maps Link" name="maps" ><?php echo $row->mapslink; ?>
                                                    </textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group row">
                                           
                                                    <label class="control-label col-md-3" for="linkweb">Link Website</label>
                                                    <div class="col-md-9">
                                                        <input type="text" name="linkweb" id="linkweb" class="form-control" placeholder="Link Website" value="<?php echo $row->link_web; ?>">
                                                    </div>
                                            
                                        </div>
                                    
                                        <div class="form-group row">
                                            <label class="control-label col-md-3" for="emailweb">Email Website</label>
                                            <div class="col-md-9">
                                                <input type="text" name="emailweb" id="emailweb" class="form-control" placeholder="Email Website" value="<?php echo $row->email_web; ?>">
                                            </div>
                                         </div>

                                         <div class="form-group row">
                                            <label class="control-label col-md-3" for="tlpweb1">Telepon Website1</label>
                                            <div class="col-md-9">
                                                <input type="text" name="tlpweb1" id="tlpweb1" class="form-control" placeholder="Telepon Website1" value="<?php echo $row->tlp_web1; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label col-md-3" for="tlpweb2">Telepon Website2</label>
                                            <div class="col-md-9">
                                                <input type="text" name="tlpweb2" id="tlpweb2" class="form-control" placeholder="Telepon Website2" value="<?php echo $row->tlp_web2; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label col-md-3" for="faxweb1">Fax Website1</label>
                                            <div class="col-md-9">
                                                <input type="text" name="faxweb1" id="faxweb1" class="form-control" placeholder="Fax Website1" value="<?php echo $row->fax_web1; ?>">
                                            </div>
                                        </div>

                                         <div class="form-group row">
                                            <label class="control-label col-md-3" for="faxweb2">Fax Website2</label>
                                            <div class="col-md-9">
                                                <input type="text" name="faxweb2" id="faxweb2" class="form-control" placeholder="Fax Website2" value="<?php echo $row->fax_web2; ?>">
                                            </div>
                                        </div>

                                      <div class="form-group row">
                                            <label class="control-label col-md-3" for="facebooklink">Facebook Link</label>
                                            <div class="col-md-9">
                                                <input type="text" name="facebooklink" id="facebooklink" class="form-control" placeholder="Facebook Link" value="<?php echo $row->facebook_link; ?>">
                                            </div>
                                        </div>


                                         <div class="form-group row">
                                            <label class="control-label col-md-3" for="twitterlink">Twitter Link</label>
                                            <div class="col-md-9">
                                                <input type="text" name="twitterlink" id="twitterlink" class="form-control" placeholder="Twitter Link" value="<?php echo $row->twitter_link; ?>">
                                            </div>
                                        </div>

                                         <div class="form-group row">
                                            <label class="control-label col-md-3" for="instagramlink">Instagram Link</label>
                                            <div class="col-md-9">
                                                <input type="text" name="instagramlink" id="instagramlink" class="form-control" placeholder="Instagram Link" value="<?php echo $row->instagram_link; ?>">
                                            </div>
                                        </div>
                                       
                                    

                                    <div class="form-group row">
                                            <label class="control-label col-md-3" for="youtubelink">Youtube Link</label>
                                            <div class="col-md-9">
                                                <input type="text" name="youtubelink" id="youtubelink" class="form-control" placeholder="Youtube Link" value="<?php echo $row->youtube_link; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label col-md-3" for="linkvideo">Link Video Hal</label>
                                            <div class="col-md-9">
                                                <input type="text" name="linkvideo" id="linkvideo" class="form-control" placeholder="Link video" value="<?php echo $row->link_video; ?>">
                                            </div>
                                        </div>
                                   

                                
                                  

                                   

                                </div>

                           

                                


                            
                                

                                <div class="col-md-12">
                                    <div class="form-group row">

                                        <div class="col-md-6">
                                            <label class="control-label col-md-3" for="filefavicon">Favicon</label>
                                            <div class="col-md-9">
                                                <?php if (!empty($row->favicon_pic)) {?>
                                                    <img  id="idicon" name="idicon" height="150px" width="150px" alt="" src="<?php echo base_url('/storage/images/website/' . $row->favicon_pic . '?' . uniqid()); ?>"><br>
                                                <?php } else {?>
                                                    <img  id="idicon" name="idicon" height="150px" width="150px" alt="" src="<?php echo base_url('/assets/images/user_blank.png'); ?>"><br>
                                                <?php }?>
                                            <label style="width:150px;border-radius: 0px;margin-bottom:0px" class="btn btn-info btn-xs">Browse Foto
                                                <input  type="file" style="width:150px;display:none;" id="imageicon" name="filefavicon" accept=".jpg,.png,image/*" capture onchange="readURLicon()">
                                            </label>
                                            </div>
                                            <input type="hidden" name="favicon2" value="<?php echo $row->favicon_pic; ?>">
                                        </div>


                                        <div class="col-md-6">
                                            <label class="control-label col-md-3" for="filefavicon">Logo</label>
                                            <div class="col-md-9">
                                                <?php if (!empty($row->logo_pic)) {?>
                                                    <img  id="idlogo" name="idlogo" height="150px" width="150px" alt="" src="<?php echo base_url('/storage/images/website/' . $row->logo_pic . '?' . uniqid()); ?>"><br>
                                                <?php } else {?>
                                                    <img  id="idlogo" name="idlogo" height="150px" width="150px" alt="" src="<?php echo base_url('/assets/images/user_blank.png'); ?>"><br>
                                                <?php }?>
                                            <label style="width:150px;border-radius: 0px;margin-bottom:0px" class="btn btn-info btn-xs">Browse Foto
                                                <input  type="file" style="width:150px;display:none;" id="imagelogo" name="filelogo" accept=".jpg,.png,image/*" capture onchange="readURLlogo()">
                                            </label>
                                            </div>
                                            <input type="hidden" name="logo2" value="<?php echo $row->logo_pic; ?>">
                                        </div>

                                    </div>
                                </div>

                                
                              



                                <div class="col-md-12">
                                    <div class="pull-right">
                                    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-check "></i> Simpan</button>
                                        <!--<a class="btn btn-warning" href="#" id="reset" ><i class="fa fa-retweet" aria-hidden="true"></i> Reset</a>-->
                                        <a class="btn btn-danger" href="<?php echo base_url('admin/Dashboard'); ?>" id="Keluar"><i class="glyphicon glyphicon-log-out" aria-hidden="true"></i> Keluar</a>
                                    </div>
                                </div>

                                </form>
                            <?php } else {echo informasi('Data Not Found!');}?>
                        </div>
                    </div>
                </div>
            </div>
    </section>

</div>

<?php
$this->load->view($js);
?>

<!--MESAGE BOX-->
<?php if ($this->session->flashdata('msg') == 'error'): ?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Data Gagal Disimpan.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>

    <?php elseif ($this->session->flashdata('msg') == 'success'): ?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Data Berhasil disimpan.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif ($this->session->flashdata('msg') == 'info'): ?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Data berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus'): ?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Data Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else: ?>

<?php endif;?>
<!--END MESSAGE BOX -->

<!--LOAD GAMBAR -->
<script type="text/javascript">
	function readURLicon() {

		var oFReader = new FileReader();

		oFReader.readAsDataURL(document.getElementById("imageicon").files[0]);

		oFReader.onload = function(oFREvent) {
		  document.getElementById("idicon").src = oFREvent.target.result;
		};
	  };
	</script>

<script type="text/javascript">
	function readURLlogo() {

		var oFReader = new FileReader();

		oFReader.readAsDataURL(document.getElementById("imagelogo").files[0]);

		oFReader.onload = function(oFREvent) {
		  document.getElementById("idlogo").src = oFREvent.target.result;
		};
	  };
</script>

<script type="text/javascript">
    $( "#reset" ).click(function () {
    $('#id_barang').val('');
    $('#kode_barang').val('');
    $('#nama_barang').val('');
    $('#kategori_barang').val('');
    $('#satuan').val('');
    $('#harga_beli').val('');
    $('#harga_jual').val('');
    $('#ukuran').val('');
    $('#warna').val('');
    $('#merek').val('');
    $('#lokasi').val('');
    $('#stok').val('');
    $('#stok_minimal').val('');
    $('#expired').val('');
    $( "#selectSatuan" ).load( "data.php?satuanBarang=satuan" );
    $( "#selectKategori" ).load( "data.php?kategoriBarang=kategori" );
    $('#loadUploader').load('data.php?imgUpload=img&imgID=0');

} );

</script>

<!-- upload icon -->
<!--<script type="text/javascript">
$(document).ready(function(){
    $(#upload).submit(function(e){
        e.preveDefault();
            $.ajax({
                url:'<?php base_url();?>pengaturan/upload_icon';
                type:post,
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    alert("Upload Icon Berhasil");
                }
            });

    });
});
</script>-->


