<!--Counter Inbox-->
<section class="content">
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="glyphicon glyphicon-th"></i> <?php echo $namamenu; ?></h3>
                    <div class="pull-right">
                        <a href="<?php echo base_url('admin/Dashboard'); ?>" class="btn btn-sm btn-danger">
                            <span class="fa fa-mail-forward"></span> Keluar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span> Tambah Partner</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-striped" style="font-size:13px;">
                        <thead>
                            <tr>
                                <th>Aksi</th>
                                <th>Photo</th>
                                <th>Judul</th>
                                <th>Link</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data->result_array() as $i) :
                                $id = $i['id'];
                                $judul = $i['judul'];
                                $link = $i['link'];
                                $photo = $i['photo'];
                            ?>
                                <tr>
                                    <td>
                                        <a class="btn" title="Ubah" data-toggle="modal" data-target="#ModalEdit<?php echo $id; ?>"><span class="fa fa-pencil"></span></a>
                                        <a class="btn" title="Hapus" data-toggle="modal" data-target="#ModalHapus<?php echo $id; ?>"><span class="fa fa-trash"></span></a>
                                    </td>
                                    <td><img width="40" height="40" class="img-circle" src="<?php echo base_url() . 'assets/images/' . $photo; ?>"></td>
                                    <td><?php echo $judul; ?></td>
                                    <td><?php echo $link; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->



<!-- ./wrapper -->

<!--Modal Add Pengguna-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Partner</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url() . 'admin/partner/simpan_partner' ?>" method="post" enctype="multipart/form-data">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Judul</label>
                        <div class="col-sm-7">
                            <input type="text" name="judul" class="form-control"  placeholder="Judul" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Link Web</label>
                        <div class="col-sm-7">
                            <input type="text" name="link" class="form-control" placeholder="Link" >
                        </div>
                    </div>

                    
                    
                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Photo</label>
                        <div class="col-sm-7">
                            <input type="file" accept=".jpg,.png,image/*" name="filefoto" required />
                        </div>
                    </div>
                    

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<?php foreach ($data->result_array() as $i) :
            $id = $i['id'];
            $judul = $i['judul'];
            $link = $i['link'];
            $photo = $i['photo'];
          

          
        ?>
	
        <div class="modal fade" id="ModalEdit<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Partner</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url() . 'admin/partner/update_partner' ?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                                
                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Judul</label>
                        <div class="col-sm-7">
                            <input type="text" value="<?= $judul ?>" name="judul" class="form-control"  placeholder="Judul">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Link Web</label>
                        <div class="col-sm-7">
                            <input type="text" value="<?= $link ?>" name="link" class="form-control" placeholder="Link" required>
                        </div>
                    </div>

            
                    
                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Photo</label>
                        <img id="blah1" name="photo1" height="150px" width="150px" alt="" src="<?php echo base_url('assets/images/' . $photo); ?>"><br>
                        <div class="col-sm-7">
                            <input type="file" accept=".jpg,.png,image/*" name="filefoto" />
                        </div>
                    </div>

                   
            
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach; ?>

<?php foreach ($data->result_array() as $i) :
    $id = $i['id'];
    $judul = $i['photo'];
?>
    <!--Modal Hapus Pengguna-->
    <div class="modal fade" id="ModalHapus<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                    <h4 class="modal-title" id="myModalLabel">Hapus Partner</h4>
                </div>
                <form class="form-horizontal" action="<?php echo base_url() . 'admin/partner/hapus_partner' ?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <input type="hidden" name="kode" value="<?php echo $id; ?>" />
                        <p>Apakah Anda yakin mau menghapus Partner ?</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>



<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
<!-- page script -->
<script>
    $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<?php if ($this->session->flashdata('msg') == 'error') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Error',
            text: "Gagal Simpan",
            showHideTransition: 'slide',
            icon: 'error',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#FF4859'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'warning') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Warning',
            text: "Gambar yang Anda masukan terlalu besar.",
            showHideTransition: 'slide',
            icon: 'warning',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#FFC017'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'success') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Success',
            text: "Partner Berhasil disimpan ke database.",
            showHideTransition: 'slide',
            icon: 'success',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#7EC857'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'info') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Info',
            text: "Partner berhasil di update",
            showHideTransition: 'slide',
            icon: 'info',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#00C9E6'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Success',
            text: "Anggota Berhasil dihapus.",
            showHideTransition: 'slide',
            icon: 'success',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#7EC857'
        });
    </script>

<?php else : ?>

<?php endif; ?>