<!--Counter Inbox-->
<section class="content">
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="glyphicon glyphicon-th"></i> <?php echo $namamenu; ?></h3>
                    <div class="pull-right">
                        <a href="<?php echo base_url('admin/Dashboard'); ?>" class="btn btn-sm btn-danger">
                            <span class="fa fa-mail-forward"></span> Keluar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <a class="btn btn-success btn-flat"  href="<?php echo base_url().'admin/anggota/add_anggota'?>"><span class="fa fa-plus"></span> Tambah Anggota</a>
                    <!-- <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span> Tambah Anggota</a> -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-striped" style="font-size:13px;">
                        <thead>
                            <tr>
                                <th>Aksi</th>
                                <th>Photo</th>
                                <th>NIK KTP</th>
                                <th>No KTA</th>
                                <th>Nama Lengkap</th>
                                <th>Jenis Kelamin</th>
                                <th>Tempat & Tgl Lahir</th>
                                <th>Alamat</th>
                                <th>Pekerjaan</th>
                                <th>No Hp</th>
                                <th>Ukuran Baju</th>
                                <th>Tgl Kadaluarsa Kartu</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data->result_array() as $i) :
                                $id = $i['id'];
                                $nik = $i['nik'];
                                $no_kta = $i['no_kta'];
                                $nama_lengkap = $i['nama_lengkap'];
                                $jenis_kelamin = $i['jenis_kelamin'];
                                $tempat_lahir = $i['tempat_lahir'];
                                $tgl_lahir = $i['tgl_lahir'];
                                $alamat = $i['alamat'];
                                $pekerjaan = $i['pekerjaan'];
                                $no_hp = $i['no_hp'];
                                $photo = $i['photo'];
                                $kartu = $i['kartu'];
                                $ukuran_baju = $i['ukuran_baju'];
                                $tgl_kartu = $i['tgl_kartu'];
                            ?>
                                <tr>
                                    <td>
                                        <a class="btn" title="Ubah" data-widget="chat-pane-toggle" href="<?php echo base_url().'admin/anggota/edit_anggota/'.$id;?>"><span class="fa fa-pencil"></span></a>
                                        <a class="btn" title="Hapus" data-toggle="modal" data-target="#ModalHapus<?php echo $id; ?>"><span class="fa fa-trash"></span></a>
                                    </td>
                                    <td><img width="40" height="40" class="img-circle" src="<?php echo base_url() . 'assets/images/' . $photo; ?>"> <br>
                                        <img width="60" height="40" src="<?php echo base_url() . 'assets/images/' . $kartu; ?>">
                                    </td>
                                    <td><?php echo $nik; ?></td>
                                    <td><?php echo $no_kta; ?></td>
                                    <td><?php echo $nama_lengkap; ?></td>
                                    <?php if ($jenis_kelamin == 'L') : ?>
                                        <td>Laki-Laki</td>
                                    <?php else : ?>
                                        <td>Perempuan</td>
                                    <?php endif; ?>
                                    <td><?php echo $tempat_lahir . "," . $tgl_lahir; ?> </td>
                                    <td><?php echo $alamat; ?></td>
                                    <td><?php echo $pekerjaan; ?></td>
                                    <td><?php echo $no_hp; ?></td>
                                    <td><?php echo $ukuran_baju; ?></td>
                                    <td><?php echo $tgl_kartu; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->



<!-- ./wrapper -->

<!--Modal Add Pengguna-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Anggota</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url() . 'admin/anggota/simpan_anggota' ?>" method="post" enctype="multipart/form-data">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">No KTA</label>
                        <div class="col-sm-7">
                            <input type="text" name="nokta" class="form-control" id="inputUserName" placeholder="Nomor KTA" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">NIK KTP</label>
                        <div class="col-sm-7">
                            <input type="text" name="nik" class="form-control" placeholder="NIK KTP" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Nama Lengkap</label>
                        <div class="col-sm-7">
                            <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Tempat,Tanggal Lahir</label>
                        <div class="col-sm-3">
                            <input type="text" name="tempat" class="form-control" placeholder="Tempat Lahir" required>
                        </div>
                        <div class="col-sm-4">
                            <input type="date" name="tgl" class="form-control" placeholder="Nama Lengkap" required>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Jenis Kelamin</label>
                        <div class="col-sm-7">
                            <div class="radio radio-info radio-inline">
                                <input type="radio" id="inlineRadio1" value="L" name="jenkel" checked>
                                <label for="inlineRadio1"> Laki-Laki </label>
                            </div>
                            <div class="radio radio-info radio-inline">
                                <input type="radio" id="inlineRadio1" value="P" name="jenkel">
                                <label for="inlineRadio2"> Perempuan </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="control-label col-md-4" for="alamat">Alamat</label>
                        <div class="col-md-7">
                            <textarea style="min-height: 100px; height: 0px;" class="form-control" placeholder="Alamat" name="alamat"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Pekerjaan</label>
                        <div class="col-sm-7">
                            <input type="text" name="kerja" class="form-control" placeholder="Pekerjaan" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">No Hp</label>
                        <div class="col-sm-7">
                            <input type="text" name="nohp" class="form-control" id="inputUserName" placeholder="No Hp" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Ukuran Baju</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="ukuran" required>
                                <option value="S">S</option>
                                <option value="M">M</option>
                                <option value="L">L</option>
                                <option value="XL">XL</option>
                                <option value="XXL">XXL</option>
                                <option value="XXXL">XXXL</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Photo</label>
                        <div class="col-sm-7">
                            <input type="file" accept=".jpg,.png,image/*" name="filefoto" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Kartu</label>
                        <div class="col-sm-7">
                            <input type="file" accept=".jpg,.png,image/*" name="filekartu" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputUserName" class="col-sm-4 control-label">Masa berlaku kartu sampai</label>
                        <div class="col-sm-7">
                            <input type="date" name="tglkartu" class="form-control" placeholder="Masa Berlaku Kartu" required>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<?php foreach ($data->result_array() as $i) :
    $id = $i['id'];
    $nik = $i['nik'];
    $no_kta = $i['no_kta'];
    $nama_lengkap = $i['nama_lengkap'];
    $jenis_kelamin = $i['jenis_kelamin'];
    $tempat_lahir = $i['tempat_lahir'];
    $tgl_lahir = $i['tgl_lahir'];
    $alamat = $i['alamat'];
    $pekerjaan = $i['pekerjaan'];
    $no_hp = $i['no_hp'];
    $photo = $i['photo'];
    $ukuran_baju = $i['ukuran_baju'];
    $tgl_kartu = $i['tgl_kartu'];
    $prov = $i['provinsi'];
    $kab = $i['kota'];
    $kec = $i['kecamatan'];
    $des = $i['desa'];
    $stsmember = $i['status_member'];

    if ($jenis_kelamin == 'L') {
        $kell = 'checked';
        $kelp = '';
    } else {
        $kell = '';
        $kelp = 'checked';
    }
?>

    <div class="modal fade" id="ModalEdit<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Anggota</h4>
                </div>
                <form class="form-horizontal" action="<?php echo base_url() . 'admin/anggota/update_anggota' ?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
            <input type="hidden" id="nilaiprovinsi" name="nilaiprovinsi" value="<?= $prov ; ?>" class="form-control" />
            <input type="hidden" id="nilaikota" name="nilaikota" value="<?= $kab ; ?>" class="form-control" />
            <input type="hidden" id="nilaikecamatan" name="nilaikecamatan" value="<?= $kec ; ?>" class="form-control" />
            <input type="hidden" id="nilaidesa" name="nilaidesa" value="<?= $des ; ?>" class="form-control" />  
                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">No KTA</label>
                            <div class="col-sm-7">
                                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                <input type="text" name="nokta" value="<?= $no_kta ?>" class="form-control" id="inputUserName" placeholder="Nomor KTA" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">NIK KTP</label>
                            <div class="col-sm-7">
                                <input type="text" name="nik" value="<?= $nik ?>" class="form-control" placeholder="NIK KTP" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">Nama Lengkap</label>
                            <div class="col-sm-7">
                                <input type="text" name="nama" value="<?= $nama_lengkap ?>" class="form-control" placeholder="Nama Lengkap" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">Tempat,Tanggal Lahir</label>
                            <div class="col-sm-3">
                                <input type="text" name="tempat" value="<?= $tempat_lahir ?>" class="form-control" placeholder="Tempat Lahir" required>
                            </div>
                            <div class="col-sm-4">
                                <input type="date" name="tgl" value="<?= $tgl_lahir ?>" class="form-control" placeholder="Nama Lengkap" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">Jenis Kelamin</label>
                            <div class="col-sm-7">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="inlineRadio1" value="L" name="jenkel" <?= $kell ?>>
                                    <label for="inlineRadio1"> Laki-Laki </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="inlineRadio1" value="P" name="jenkel" <?= $kelp ?>>
                                    <label for="inlineRadio2"> Perempuan </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4" for="alamat">Alamat</label>
                            <div class="col-md-7">
                                <textarea style="min-height: 100px; height: 0px;" class="form-control" placeholder="Alamat" name="alamat"><?= $alamat ?> </textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="prov" class="col-sm-4 control-label">Provinsi</label>
                            <div class="col-md-7">
                                <select name="prov" id="provinsi" class="form-control <?= form_error('prov') ? 'invalid' : '' ?>" value="<?= set_value('prov') ?>">
                                    <option value=''>- Pilih Provinsi -</option>
                                    <?php
                                    foreach ($provinsi as $item) {
                                        if($item->id == $prov){
                                            $selected = " selected";
                                        }
                                        else{
                                            $selected = "";
                                        }
                                        echo '<option value="' . $item->id . '"'. $selected.'>' . $item->nama . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kab" class="col-sm-4 control-label">Kota/Kabupaten</label>
                            <div class="col-md-7">
                                <select name="kab" id="kabupaten" class="form-control <?= form_error('kab') ? 'invalid' : '' ?>" value="<?= set_value('kab') ?>">
                                    <option value=''>Pilih Kabupaten</option>
                                </select>
                                <?= form_error('kab', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="kec" class="col-sm-4 control-label">Kecamatan</label>
                            <div class="col-md-7">
                                <select name="kec" id="kecamatan" class="form-control <?= form_error('kec') ? 'invalid' : '' ?>" value="<?= set_value('kec') ?>">
                                    <option value=''>Pilih Kecamatan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="des" class="col-sm-4 control-label">Desa</label>
                            <div class="col-md-6">
                                <select name="des" class="form-control <?= form_error('des') ? 'invalid' : '' ?>" value="<?= set_value('des') ?>" id="desa">
                                    <option value=''>Pilih Desa</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">Pekerjaan</label>
                            <div class="col-md-6">
                                <select class="form-control" name="kerja">
                                    <option value="ASN/PNS" <?= ($pekerjaan == "ASN/PNS") ? 'selected' : '' ?>>ASN/PNS</option>
                                    <option value="Pegawai Swasta" <?= ($pekerjaan == "Pegawai Swasta") ? 'selected' : '' ?>>Pegawai Swasta</option>
                                    <option value="Wiraswasta" <?= ($pekerjaan == "Wiraswasta") ? 'selected' : '' ?>>Wiraswasta</option>
                                    <option value="Lainnya" <?= ($pekerjaan == "Lainnya") ? 'selected' : '' ?>>Lainnya</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">No Hp</label>
                            <div class="col-sm-7">
                                <input type="text" name="nohp" class="form-control" id="inputUserName" placeholder="No Hp" value="<?= $no_hp ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">Ukuran Baju</label>
                            <div class="col-sm-7">
                                <select class="form-control" name="ukuran" required>
                                    <option value="S" <?php if ($ukuran_baju == 'S') {
                                                            echo "selected=selected";
                                                        } ?>>S</option>
                                    <option value="M" <?php if ($ukuran_baju == 'M') {
                                                            echo "selected=selected";
                                                        } ?>>M</option>
                                    <option value="L" <?php if ($ukuran_baju == 'L') {
                                                            echo "selected=selected";
                                                        } ?>>L</option>
                                    <option value="XL" <?php if ($ukuran_baju == 'XL') {
                                                            echo "selected=selected";
                                                        } ?>>XL</option>
                                    <option value="XXL" <?php if ($ukuran_baju == 'XXL') {
                                                            echo "selected=selected";
                                                        } ?>>XXL</option>
                                    <option value="XXXL" <?php if ($ukuran_baju == 'XXXL') {
                                                                echo "selected=selected";
                                                            } ?>>XXXL</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label" >Kategori Member</label>
                            <div class="col-sm-7">
                                <select class="form-control" name="status">
                                    <option value="1" <?= ($stsmember == 1) ? 'selected' : '' ?>>Member Pelajar</option>
                                    <option value="2" <?= ($stsmember == 2) ? 'selected' : '' ?>>Member Umum</option>
                                    <option value="3" <?= ($stsmember == 3) ? 'selected' : '' ?>>Member TNI/POLRI</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">Masa berlaku kartu sampai</label>
                            <div class="col-sm-7">
                                <input type="date" name="tglkartu" class="form-control" value="<?= $tgl_kartu ?>" placeholder="Masa Berlaku Kartu" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">Photo</label>

                            <div class="col-sm-7">
                                <img id="blah1" name="photo1" height="150px" width="150px" alt="" src="<?php echo base_url('assets/images/' . $photo); ?>"><br>
                                <input type="file" accept=".jpg,.png,image/*" name="filefoto" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">Kartu</label>
                            <div class="col-sm-7">
                                <img id="blah1" name="photo1" height="150px" width="250px" alt="" src="<?php echo base_url('assets/images/' . $kartu); ?>"><br>
                                <input type="file" accept=".jpg,.png,image/*" name="filekartu" />
                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<?php foreach ($data->result_array() as $i) :
    $id = $i['id'];
    $nama_lengkap = $i['nama_lengkap'];
?>
    <!--Modal Hapus Pengguna-->
    <div class="modal fade" id="ModalHapus<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                    <h4 class="modal-title" id="myModalLabel">Hapus Anggota</h4>
                </div>
                <form class="form-horizontal" action="<?php echo base_url() . 'admin/anggota/hapus_anggota' ?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <input type="hidden" name="kode" value="<?php echo $id; ?>" />
                        <p>Apakah Anda yakin mau menghapus Anggota <b><?php echo $nama_lengkap; ?></b> ?</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>



<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
<!-- page script -->
<script>
        $(document).ready(function() {


            var nilaikota = $('#nilaikota').val();
            var nilaikec = $('#nilaikecamatan').val();
            var nilaidesa = $('#nilaidesa').val();

            // var url = "<?php echo site_url('daftar/add_ajax_kab'); ?>/" + $(this).val() + "/" + nilaikota;
            // $('#kabupaten').load(url);
            
            // var url = "<?php echo site_url('daftar/add_ajax_kec'); ?>/" + $(this).val() + "/" + nilaikec;
            // $('#kecamatan').load(url);
                
            // var url = "<?php echo site_url('daftar/add_ajax_des'); ?>/" + $(this).val() + "/" + nilaidesa ;
            // $('#desa').load(url);
              
     
            $("#provinsi").change(function() {
                console.log("ok");
                var url = "<?php echo site_url('daftar/add_ajax_kab'); ?>/" + $(this).val();
                $('#kabupaten').load(url);
                return false;
            })

            $("#kabupaten").change(function() {
                var url = "<?php echo site_url('daftar/add_ajax_kec'); ?>/" + $(this).val();
                $('#kecamatan').load(url);
                return false;
            })

            $("#kecamatan").change(function() {
                var url = "<?php echo site_url('daftar/add_ajax_des'); ?>/" + $(this).val();
                $('#desa').load(url);
                return false;
            })
        });
</script>
<script>
    $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });


    });
</script>
<?php if ($this->session->flashdata('msg') == 'error') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Error',
            text: "Gagal Disimpan",
            showHideTransition: 'slide',
            icon: 'error',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#FF4859'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'warning') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Warning',
            text: "Data Kurang Valid.",
            showHideTransition: 'slide',
            icon: 'warning',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#FFC017'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'success') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Success',
            text: "Anggota Berhasil disimpan ke database.",
            showHideTransition: 'slide',
            icon: 'success',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#7EC857'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'info') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Info',
            text: "Anggota berhasil di update",
            showHideTransition: 'slide',
            icon: 'info',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#00C9E6'
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
    <script type="text/javascript">
        $.toast({
            heading: 'Success',
            text: "Anggota Berhasil dihapus.",
            showHideTransition: 'slide',
            icon: 'success',
            hideAfter: false,
            position: 'bottom-right',
            bgColor: '#7EC857'
        });
    </script>

    


<?php else : ?>

<?php endif; ?>