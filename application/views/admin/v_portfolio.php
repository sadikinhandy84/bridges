<section class="content">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><i class="glyphicon glyphicon-th"></i><?php echo $namamenu; ?></h3>
                        <div class="pull-right">
                            <a href="<?php echo base_url('admin/Dashboard'); ?>"  class="btn btn-sm btn-danger" >
                            <span class="fa fa-mail-forward"></span> Keluar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
           
          <div class="box">
            <div class="box-header">
              <a class="btn btn-success btn-flat" href="<?php echo base_url().'admin/portfolio/add_portfolio'?>"><span class="fa fa-plus"></span> Add Portfolio</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-striped" style="font-size:13px;">
                <thead>
                <tr>
                    <th width="80px">Aksi</th>
          					<th width="100px">Gambar</th>
          					<th>Judul</th>
          					<th>Tanggal</th>
          					<th>Author</th> 
                    
                </tr>
                </thead>
                <tbody>
          				<?php
          					$no=0;
          					foreach ($data->result_array() as $i) :
          					   $no++;
          					   $port_id=$i['port_id'];
          					   $port_judul=$i['port_judul'];
          					   $port_isi=$i['port_deskripsi'];
          					   $port_tanggal=$i['tanggal'];
          					   $port_author=$i['port_author'];
          					   $port_gambar=$i['port_image'];
                       
                    ?>
                <tr>
                  <td>
                        <a class="btn" title="Ubah" href="<?php echo base_url().'admin/portfolio/get_edit/'.$port_id;?>"><span class="fa fa-pencil"></span></a>
                        <a class="btn" title="Hapus" data-toggle="modal" data-target="#ModalHapus<?php echo $port_id;?>"><span class="fa fa-trash"></span></a>
                  </td>
                  <td><img src="<?php echo base_url().'assets/images/'.$port_gambar;?>" style="width:90px;"></td>
                  <td><?php echo $port_judul;?></td>
                  
        				  <td><?php echo $port_tanggal;?></td>
        				  <td><?php echo $port_author;?></td>
                  
                </tr>
				<?php endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php
  $this->load->view($js);
?>

	
	<?php foreach ($data->result_array() as $i) :
              $port_id=$i['port_id'];
              $port_judul=$i['port_judul'];
              $port_gambar=$i['port_image'];
            ?>
	<!--Modal Hapus Pengguna-->
        <div class="modal fade" id="ModalHapus<?php echo $port_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Hapus Portfolio</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/portfolio/hapus_portfolio'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">       
							       <input type="hidden" name="kode" value="<?php echo $port_id;?>"/> 
                     <input type="hidden" value="<?php echo $port_gambar;?>" name="gambar">
                            <p>Apakah Anda yakin mau menghapus Portfolio <b><?php echo $port_judul;?></b> ?</p>
                               
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>
	
	



<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
    
    <?php if($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Portfolio Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Portfolio berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Portfolio Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else:?>

    <?php endif;?>
