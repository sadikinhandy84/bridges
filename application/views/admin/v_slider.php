<!--Counter Inbox-->

  <section class="content">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><i class="glyphicon glyphicon-th"></i> <?php echo $namamenu; ?></h3>
                        <div class="pull-right">
                            <a href="<?php echo base_url('admin/Dashboard'); ?>"  class="btn btn-sm btn-danger" >
                            <span class="fa fa-mail-forward"></span> Keluar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Main content -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
  
            <div class="box-header">
              <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span> Add Slider</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-striped" style="font-size:13px;" class="display">
                <thead>
                <tr>
                    <th>Aksi</th>
                    <th>Gambar</th>
                    <th>Judul</th>
                    <th>Keterangan</th>
                    <th>Link Menu</th>
                    <th>Publish</th>
                </tr>
                </thead>
                <tbody>
          				<?php
          					$no=0;
          					foreach ($data->result_array() as $i) :
          					   $no++;
          					   $id=$i['id'];
          					   $title=$i['title'];
          					   $text=$i['text'];
          					   $img=$i['img'];
          					   $linkmenu=$i['linkmenu'];
          					   $publish=$i['publish'];
                            
                       
                    ?>
                <tr>
                    <td>
                        <a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $id;?>"><span class="fa fa-pencil"></span></a>
                        <?php if ($id != 1 ) {  ?>
                            <a class="btn" data-toggle="modal" data-target="#ModalHapus<?php echo $id;?>"><span class="fa fa-trash"></span></a>
                        <?php } ?>
                        
                    </td>
                    <td><img src="<?php echo base_url().'assets/images/'.$img;?>" style="width:80px;"></td>
                    <td><?php echo $title;?></td>
                    <td><?php echo $text;?></td>
                    <td><?php echo $linkmenu;?></td>
                    <td><?php echo $publish;?></td>
                </tr>
				<?php endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php
   $this->load->view($js);
 ?>
  

<!-- ./wrapper -->

    <!--Modal Add Pengguna-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Slider</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/slider/simpan_slider'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                                
                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Judul</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="judul" class="form-control" id="inputUserName" placeholder="Judul" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="keterangan" class="col-sm-4 control-label">Keterangan</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="keterangan" class="form-control" id="keterangan" placeholder="Keterangan" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="linkmenu" class="col-sm-4 control-label">Link Menu</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="linkmenu" class="form-control" id="linkmenu" placeholder="linkmenu" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="filefoto" class="col-sm-4 control-label">Photo</label>
                                        <div class="col-sm-7">
                                            <input type="file" name="filefoto" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="filefoto" class="col-sm-4"></label>
                                        <label for="filefoto" class="col-sm-7">Ukuran Gambar 4044 x 1296 px agar terlihat porposinal</label>
                                    </div>
                               
                               

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

  <!--Modal Edit Album-->
  <?php foreach ($data->result_array() as $i) :
              $id = $i['id'];
$title = $i['title'];
$text = $i['text'];
$img = $i['img'];
$linkmenu = $i['linkmenu'];
$publish = $i['publish'];

            ?>
  
        <div class="modal fade" id="ModalEdit<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Slider</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/slider/update_slider'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">       
                                <input type="hidden" name="kode" value="<?php echo $id;?>"/> 
                                <input type="hidden" value="<?php echo $img;?>" name="gambar">
                                  <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Judul</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="judul" class="form-control" value="<?php echo $title;?>" id="inputUserName" placeholder="Judul" required>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Keterangan</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="keterangan" class="form-control" value="<?php echo $text;?>" id="Keterangan" placeholder="Keterangan">
                                        </div>
                                    </div>

                                        <div class="form-group">
                                        <label for="linkmenu" class="col-sm-4 control-label">Link Menu</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="linkmenu" class="form-control" value="<?php echo $text;?>" id="linkmenu" placeholder="Link Menu" >
                                        </div>
                                    </div>

                                    
                                    
                                    <div class="form-group">
                                        <label for="filefoto" class="col-sm-4 control-label">Photo</label>
                                        <div class="col-sm-7">
                                            <input type="file" name="filefoto"/>
                                        </div>
                                    </div>
                                      
                                     <div class="form-group">
                                        <label for="filefoto" class="col-sm-4"></label>
                                        <label for="filefoto" class="col-sm-7">Ukuran Gambar 4044 x 1296 px agar terlihat porposinal</label>
                                    </div>
                               
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
  <?php endforeach;?>
	<!--Modal Edit Album-->

	<?php foreach ($data->result_array() as $i) :
                $id = $i['id'];
                $title = $i['title'];
                $text = $i['text'];
                $img = $i['img'];
                $linkmenu = $i['linkmenu'];
                $publish = $i['publish'];

            ?>
	<!--Modal Hapus Pengguna-->
        <div class="modal fade" id="ModalHapus<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Hapus Slider</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/slider/hapus_slider'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">       
							       <input type="hidden" name="kode" value="<?php echo $id;?>"/> 
                                    <input type="hidden" value="<?php echo $img;?>" name="gambar">
                            <p>Apakah Anda yakin mau menghapus Posting <b><?php echo $title;?></b> ?</p>
                               
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>
	
	


<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php if($this->session->flashdata('msg')=='error'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>
    
    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Photo Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Photo berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Photo Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else:?>

    <?php endif;?>
