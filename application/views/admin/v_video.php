<!--Counter Inbox-->

<section class="content">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><i class="glyphicon glyphicon-th"></i> <?php echo $namamenu; ?></h3>
                        <div class="pull-right">
                            <a href="<?php echo base_url('admin/Dashboard'); ?>"  class="btn btn-sm btn-danger" >
                            <span class="fa fa-mail-forward"></span> Keluar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Main content -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
  
            <div class="box-header">
              <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span> Tambah Video</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-striped" style="font-size:13px;">
                <thead>
                <tr>
                            <th>Aksi</th>
          					<th>Video</th>
          					<th>Judul</th>
          					<th>Tanggal</th>
                            <th>Kategori</th>
          					<th>Author</th>
                            
                </tr>
                </thead>
                <tbody>
          			<?php
                        $no=0;
                        foreach ($data->result_array() as $i) :
                            $no++;
                            $video_id=$i['video_id'];
                            $video_judul=$i['video_judul'];
                            $video_kat = $i['videokat_nama'];
                            $video_tanggal=$i['tanggal'];
                            $video_author=$i['video_author'];
                            $video_link=$i['video_link'];
                    ?>
                <tr>
                    <td>
                        <a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $video_id;?>"><span class="fa fa-pencil"></span></a>
                        <a class="btn" data-toggle="modal" data-target="#ModalHapus<?php echo $video_id;?>"><span class="fa fa-trash"></span></a>
                  </td>
                  <td><iframe width="200" height="100" src="<?php echo $video_link;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></td>
                  <td><?php echo $video_judul;?></td>
                  <td><?php echo $video_tanggal;?></td>
                  <td><?php echo $video_kat;?></td>	 
                  <td><?php echo $video_author;?></td>

                  

                  
                </tr>
				      <?php endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php
   $this->load->view($js);
 ?>
  

<!-- ./wrapper -->

    <!--Modal Add video-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Tambah Video</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/video/simpan_video'?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">   
                            <div class="form-group">
                                <label for="inputJudul" class="col-sm-4 control-label">Judul</label>
                                <div class="col-sm-7">
                                    <input type="text" name="xjudul" class="form-control" id="inputJudul" placeholder="Judul" required>
                                </div>
                            </div>
                            <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Kategori</label>
                                        <div class="col-sm-7">
                                            
                                          <select class="form-control" name="xalbum" style="width: 100%;" required>
                                                    <option value="">-Pilih-</option>
                                              <?php
                                              $no=0;
                                              foreach ($alb->result_array() as $a) :
                                                 $no++;
                                                           $alb_id=$a['videokat_id'];
                                                           $alb_nama=$a['videokat_nama'];
                                                        ?>
                                                    <option value="<?php echo $alb_id;?>"><?php echo $alb_nama;?></option>
                                              <?php endforeach;?>
                                          </select>
                                        </div>
                                    </div>
                                    

                            <div class="form-group">
                                <label for="inputVideo" class="col-sm-4 control-label">Link Video</label>
                                <div class="col-sm-7">
                                    <input type="text" name="xvideo" class="form-control" id="inputVideo" placeholder="Link Video" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

  <!--Modal Edit Video-->
  <?php foreach ($data->result_array() as $i) :
              $video_id=$i['video_id'];
              $video_judul=$i['video_judul'];
              $video_tanggal=$i['tanggal'];
              $videokat_id = $i['video_kat_id'];
              $video_author=$i['video_author'];
              $video_link=$i['video_link'];
            ?>
  
        <div class="modal fade" id="ModalEdit<?php echo $video_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Video</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/video/update_video'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">       
                        <input type="hidden" name="kode" value="<?php echo $video_id;?>"/> 
                        <div class="form-group">
                            <label for="inputJudul" class="col-sm-4 control-label">Judul</label>
                            <div class="col-sm-7">
                                <input type="text" name="xjudul" class="form-control" value="<?php echo $video_judul;?>" id="inputJudul" placeholder="Judul" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">Album</label>
                            <div class="col-sm-7">
                                <select class="form-control" name="xalbum" style="width: 100%;" required>
                                                            <option value="">-Pilih-</option>
                                                        <?php
                                                        foreach ($alb->result_array() as $a) {
                                                                    $alb_id=$a['videokat_id'];
                                                                    $alb_nama=$a['videokat_nama'];
                                                                    if($videokat_id==$alb_id)
                                                                        echo "<option value='$alb_id' selected>$alb_nama</option>";
                                                                    else
                                                                        echo "<option value='$alb_id'>$alb_nama</option>";
                                                                }?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputLink" class="col-sm-4 control-label">Link Video</label>
                            <div class="col-sm-7">
                            <!--<textarea style="min-height: 70px; height:150px;" class="form-control" name="inputLink"><?php echo $video_link;?></textarea>-->
                            <input type="text" name="xvideo" class="form-control" id="inputVideo" value="<?php echo $video_link;?>" placeholder="Link Video" required>
                            
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
  <?php endforeach;?> 
	<!--Modal Edit Video-->

    <!--Modal Hapus Video-->
	 <?php foreach ($data->result_array() as $i) :
              $video_id=$i['video_id'];
              $video_kat_id = $i['video_kat_id'];
              $video_judul=$i['video_judul'];
              $video_tanggal=$i['tanggal'];
              $video_author=$i['video_author'];
              $video_link=$i['video_link'];
            ?>
	
        <div class="modal fade" id="ModalHapus<?php echo $video_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Hapus Video</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/video/hapus_galeri' ?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">       
                                <input type="hidden" name="kode" value="<?php echo $video_id;?>"/> 
                                <input type="hidden" name="kat" value="<?php echo $video_kat_id;?>"/> 
                                <p>Apakah Anda yakin mau menghapus Video <b><?php echo $video_judul;?></b> ?</p>
                                
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>



<script>
  $(function () {
    $('#example1').DataTable({
        "responsive": true
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php if($this->session->flashdata('msg')=='error'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>
    
    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Photo Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Photo berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Photo Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else:?>
<?php endif;?>
