<?php
$this->load->view('v_header');
?>

<div class="mt-5 pt-2">
<?php
  $this->load->view('v_toolbar');
  ?>
</div>

<button class="fa fa-angle-up" onclick="topFunction()" id="myBtn" title="Go to top"></button>

<div class="container">
	<section>
		<div class="row mt-5">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading text-uppercase">Visi</h2>
				<div class="divider"></div>
				<p class="section-subheading text-muted">Membentuk sebuah organisasi yang solid dan mampu bersaing secara positif
					untuk berperan serta bersama untuk meraih impian dan menggaapai essensi,
					serta Mewujudkan BRIDGE SHOOTING CLUB sebagai organiasi dengan tata
					kelola yang profesional, transparan dan taat hukum, guna menghasilkan atlet
					petembak yang berbakat, berprestasi baik tingkat daerah maupun nasional.</p>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading text-uppercase">Misi</h2>
				<div class="divider"></div>
				<p class="section-subheading text-muted">A. Menjadi wadah dalam mengembangkan dan meningkatkan kualitas olahraga menembak dan berburu.</p>
				<p class="section-subheading text-muted">B. Bekerja sama dengan Pengkab PERBAKIN dan semua Klub, melaksanakan
					pembinaan dan latihan secara berkesinambungan untuk menghasilkan atlet
					yang disiplin dan berprestasi.</p>
				<p class="section-subheading text-muted">C. Mendorong terciptanya tertib dan taat peraturan perundang-undangan dalam
					olahraga menembak dan berburu.</p>
				<p class="section-subheading text-muted">D. Menanamkan jiwa altlet yang menjunjung tinggi sportivitas dan pantang
					menyerah, Disiplin, Berprestasi dan Bermartabat.</p>
				<p class="section-subheading text-muted">E. Menyelenggarakan pertandingan yang berkala dan berjenjang, baik langsung
					ataupun tidak langsung, baik sendiri maupun melalui kerjasama dengan
					pihak lain.
				</p>
				<p class="section-subheading text-muted">F. Turut menjaga kelestarian alam demi keberlangsungan kehidupan.
				</p>
				<p class="section-subheading text-muted">G. Mendukung program pemerintah dalam meningkatkan prestasi olahraga.
				</p>
			</div>
		</div>
	</section>
</div>

<?php
$this->load->view('v_footer'); ?>
</div>


<script>
	// When the user scrolls down 20px from the top of the document, show the button
	window.onscroll = function() {
		scrollFunction()
	};

	function scrollFunction() {
		if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
			document.getElementById("myBtn").style.display = "block";
		} else {
			document.getElementById("myBtn").style.display = "none";
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}
</script>
<script src="<?php echo base_url().'themes/vendor/jquery/jquery.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/bootstrap.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/popper.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/js/slick.js'?>"></script>
 <script src="<?php echo base_url().'assets/bootstrap/js/aos.js'?>"></script>
<script src="<?php echo base_url().'themes/vendor/jquery-easing/jquery.easing.min.js'?>"></script>
<script src="<?php echo base_url().'themes/js/jqBootstrapValidation.js'?>"></script>
<script src="<?php echo base_url().'themes/js/contact_me.js'?>"></script>
<script src="<?php echo base_url().'themes/js/agency.min.js'?>"></script>

</body>

</html>