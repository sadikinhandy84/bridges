<?php foreach ($data->result_array() as $i) :

    $kartu = $i['kartu'];
?>

    <div class="modal fade" id="modalkartu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>

                </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">Kartu</label>
                            <div class="col-sm-7">
                                <img id="blah1" name="photo1" height="150px" width="250px" alt="" src="<?php echo base_url('assets/images/' . $kartu); ?>"><br>
                                <input type="file" accept=".jpg,.png,image/*" name="filekartu" />
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>