
<?php
	$this->load->view('v_header');
?>
	
	<button class="fa fa-angle-up" onclick="topFunction()" id="myBtn" title="Go to top"></button>
	<div class="container">
	<section class="pricing-section pt__200 pb__100 pt__sm__70 pb__sm__50" id="pricing">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="v2_title title__light pb__70 text__center">
							<h2 class="section-heading text-uppercase">Our Value</h2>
							<p class="section-subheading text-muted">We provide the best quality in learning activities to students with 5 learning methods namely innovation, creative, fun, active, effective</>
						</div>

						<div class="col-md-4 col-sm-6">
							<div class="pricing focused">
								<div class="package__name p__30">
									<h2>Facilities</h2>
								</div>
								<ul class="package__feature pt__40 pb__20 list__none">
									<li><i class="fa fa-check"></i>own school building</li>
									<li><i class="fa fa-check"></i>comfortable classrooms 3 floors</li>
									<li><i class="fa fa-check"></i>Library</li>
									<li><i class="fa fa-check"></i>UKS Room</li>
									<li><i class="fa fa-check"></i>Sports field</li>
									<li><i class="fa fa-check"></i>lunch catering</li>
									<li><i class="fa fa-check"></i>office space 2 floors</li>
									<li><i class="fa fa-check"></i>Multimedia room</li>
									<li><i class="fa fa-check"></i>Language and Science Lab</li>
								</ul>
							</div>
						</div>
						<!-- pricing single end -->
						<div class="col-md-4 col-sm-6">
							<div class="pricing focused">
								<div class="package__name p__30">
									<h2>Extracurricular</h2>
								</div>
								<ul class="package__feature pt__40 pb__20 list__none">
									<li><i class="fa fa-check"></i>archery</li>
									<li><i class="fa fa-check"></i>scout</li>
									<li><i class="fa fa-check"></i>futsal</li>
									<li><i class="fa fa-check"></i>Calligraphy</li>
									<li><i class="fa fa-check"></i>Computer</li>
									<li><i class="fa fa-check"></i>Badminton</li>
									<li><i class="fa fa-check"></i>Karate</li>
									<li><i class="fa fa-check"></i>Dokter Cilik</li>
									<li><i class=""></i></li>
								</ul>
							</div>
						</div>
						<!-- pricing single end -->
						<div class="col-md-4 col-sm-6">
							<div class="pricing focused">
								<div class="package__name p__30">
									<h2>Learning Method</h2>
								</div>
								<ul class="package__feature pt__40 pb__20 list__none">
									<li><i class="fa fa-check"></i>Innovative</li>
									<li><i class="fa fa-check"></i>Fun</li>
									<li><i class="fa fa-check"></i>Creative</li>
									<li><i class="fa fa-check"></i>Active</li>
									<li><i class="fa fa-check"></i>Effective</li>
									<li><i class=""></i></li>
									<li><i class=""></i></li>
									<li><i class=""></i></li>
									<li><i class=""></i></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

<?php 
	$this->load->view('v_footer');?>
</div>

<script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
    document.getElementById("myBtn").style.display = "block";
  } else {
    document.getElementById("myBtn").style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url('themes/default2/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('themes/default2/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('themes/default2/vendor/bootstrap/js/popper.min.js')?>"></script>
  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('themes/default2/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Contact form JavaScript -->
  <script src="<?php echo base_url('themes/default2/js/jqBootstrapValidation.js')?>"></script>
  <script src="<?php echo base_url('themes/default2/js/contact_me.js')?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('themes/default2/js/agency.min.js')?>"></script>
  
</body>
</html>
