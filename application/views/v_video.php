
<?php
	$this->load->view('v_header');
?>

<button class="fa fa-angle-up" onclick="topFunction()" id="myBtn" title="Go to top"></button>
<section class="bg-light" id="team">
    <div class="container">
      <div class="row mt-5">
        <div class="col-lg-12 text-center">
          <h2 class="text-uppercase">Galery</h2>
					<div class="divider"></div>
        </div>
      </div>
      <div class="row">
      <div class="col-md-12">
        <?php foreach ($data->result_array() as $row) : ?>
        
					<div class="gallery_product col-lg-4 col-md-4 col-sm-4">
          <iframe src="<?php echo $row['video_link'];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              <!-- <span class="thumb-info-title" style="bottom:0;width:100%;">
											<p class="thumb-info-inner" style="font-weight:bold;">
									    <?= $row['galeri_judul'] ?>
                    </p>
								</span> -->
          </div>
        <?php endforeach;?>
        </div>
        <div class="col-lg-12 text-center pt__40">
            <a class="btn btn-primary btn-lg js-scroll-trigger" href="<?= base_url('album')?>">Back To Album</a>
        </div>
      </div>
    </div>
  </section>
  

	<?php $this->load->view('v_footer');?>
	</div>

  
  <script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
    document.getElementById("myBtn").style.display = "block";
  } else {
    document.getElementById("myBtn").style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>


<script src="<?php echo base_url().'themes/vendor/jquery/jquery.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/bootstrap.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/popper.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/js/slick.js'?>"></script>
 <script src="<?php echo base_url().'assets/bootstrap/js/aos.js'?>"></script>
<script src="<?php echo base_url().'themes/vendor/jquery-easing/jquery.easing.min.js'?>"></script>
<script src="<?php echo base_url().'themes/js/jqBootstrapValidation.js'?>"></script>
<script src="<?php echo base_url().'themes/js/contact_me.js'?>"></script>
<script src="<?php echo base_url().'themes/js/agency.min.js'?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('themes/default2/js/agency.min.js')?>"></script>
	</body>
</html>