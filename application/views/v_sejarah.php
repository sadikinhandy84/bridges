<?php
$this->load->view('v_header');
?>
<div class="mt-5 pt-2">
<?php
  $this->load->view('v_toolbar');
  ?>
</div>

<button class="fa fa-angle-up" onclick="topFunction()" id="myBtn" title="Go to top"></button>


<div class="container">
	<section>
		<div class="row mt-5">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading text-uppercase">Sejarah Singkat</h2>
				<div class="divider"></div>
				<p class="section-subheading text-muted">Organisasi ini adalah Perkumpulan Penggemar Olahraga Berburu dan Menembak yang dalam Anggaran Dasar ini disebut BRIDGE SHOOTING CLUB;
					Selanjutnya BRIDGE SHOOTING CLUB disingkat dengan (BriSC).
					Didirikan di Sukabumi pada tanggal 26 Oktober 2022 kami bersepakat untuk membuat wadah yang kami beri nama BRIDGE SHOOTING CLUB (BriSC).
					Wadah tersebut dipandang perlu dikarenakan pandangan dan tujuan untuk merealisasi atas keinginan segenap anggota, sehingga dapat menjadi dasar melangkah mencapai tujuan, baik secara perorangan maupun kelompok, berdasarkan prinsip Unifikasi, Rekreasi dan Prestasi.
					Unifikasi maksudnya adalah bahwa segenap anggota yang bergabung dalam organisasi wajib mengutamakan persatuan dan kesatuan tanpa memandang suku bangsa atau golongan, warna kulit dan agama.
					Rekreasi, maksudnya adalah bahwa setiap anggota yang bergabung dalam organisasi wajib memahami bahwa kegiatan organisasi semata-mata adalah bertujuan untuk Olahraga Menembak dan Berburu, tidak berpolitik dan tidak memihak.
					Prestasi, maksudnya adalah bahwa segenap anggota yang bergabung dalam organisasi wajib memahami bahwa kegiatan organisasi adalah juga untuk tujuan prestasi diri dan prestasi organisasi.</p>
			</div>
		</div>

	</section>
</div>

<?php
$this->load->view('v_footer'); ?>
</div>


<script>
	// When the user scrolls down 20px from the top of the document, show the button
	window.onscroll = function() {
		scrollFunction()
	};

	function scrollFunction() {
		if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
			document.getElementById("myBtn").style.display = "block";
		} else {
			document.getElementById("myBtn").style.display = "none";
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}
</script>
<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url().'themes/vendor/jquery/jquery.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/bootstrap.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/popper.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/js/slick.js'?>"></script>
 <script src="<?php echo base_url().'assets/bootstrap/js/aos.js'?>"></script>
<script src="<?php echo base_url().'themes/vendor/jquery-easing/jquery.easing.min.js'?>"></script>
<script src="<?php echo base_url().'themes/js/jqBootstrapValidation.js'?>"></script>
<script src="<?php echo base_url().'themes/js/contact_me.js'?>"></script>
<script src="<?php echo base_url().'themes/js/agency.min.js'?>"></script>

</body>

</html>