<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Site Under Maintenance</title>
    <link href="http://localhost/codeigniter/assets/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body class="bg">
    <h1 class="head text-center">Website services have not been extended</h1>
    <div class="container">
        <div class="content1"> 
            <p class="text-center">Sorry for the inconvenience. Please complete the service payment.</p>
            <!-- <p class="text-center">Sorry for the inconvenience. To improve our services, we have momentarily shutdown our site.</p> -->
        </div>
    </div>
</body>
</html>