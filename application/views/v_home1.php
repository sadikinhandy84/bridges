 <?php
  $this->load->view('v_header');
  ?>


 <!-----------------------    SLIDER CAROUSEL   ------------------------->
 <section id="home">
   <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
     <div class="carousel-inner">
       <?php foreach ($slidegambar as $gambar) {
          echo '<div class="carousel-item ';
          if ($gambar['active'] == 1) {
            echo 'active';
          }
          echo ' home-slider">
            <img class="d-block w-100" src="' . base_url('assets/images/' . $gambar['img']) . '" />
              <div class="carousel-caption">
                <h2 class="carousel-caption-header">' . $gambar['title'] . '</h2>
                <p class="carousel-caption-text hidden-sm hidden-xs">
                  ' . $gambar['text'] . '
                </p>
              </div>
            </div>
            ';
        }
        ?>
     </div>
     <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
       <span class="carousel-control-prev-icon" aria-hidden="true"></span>
       <span class="sr-only">Previous</span>
     </a>
     <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
       <span class="carousel-control-next-icon" aria-hidden="true"></span>
       <span class="sr-only">Next</span>
     </a>
   </div>
 </section>


 <!----------------------------------------END SLIDER---------------------------------->

 <?php
  $this->load->view('v_toolbar');
  ?>
 <!---------------------------------------- ABOUT US ----------------------------------->
 <section id="about">
   <div class="container">
     <div class="row">
       <div class="col-lg-12 text-center">
         <h2 class="section-heading blcenter text-uppercase"></span>Tentang <span>BRIDGE SHOOTING CLUB</span></h2>
         <h3 class="section-subheading text-muted">Selamat datang diwebsite kami</h3>
       </div>
     </div>

     <div class="row" data-aos="flip-left">
       <div class="col-md-6">
         <img class="img-fluid" src="assets/images/about1.jpg">
       </div>
       <div class="col-md-6">
         <p class="lead">Organisasi hobi dan Perkumpulan yang terbuka bagi semua orang yang ingin menyalurkan bakat, hobi maupun aktivitas dan kreativitas lainnya di bidang olahraga Berburu dan Menembak.</p>

       </div>
     </div>
   </div>
   <section>
     <div class="container">
       <div class="row" data-aos="flip-right">
         <div class="col-lg-6">
           <p class="lead">BriSC adalah Club Menembak dibawah naungan Perbakin Kabupaten Sukabumi Jawa Barat
             Dengan No SK- NO : 05/SKEP-CLUB/PB/KAB-SMI/XI/2022</p>
         </div>
         <div class="col-lg-6">
           <div class="embed-responsive embed-responsive-16by9">
             <iframe class="embed-responsive-item" src="<?= $dataweb['link_video'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
           </div>
         </div>
       </div>
     </div>
     </div>
   </section>


   <!-- <section id="projectmanagement">
   <div class="container">
     <div class="row">
       <div class="col-lg-12 text-center">
         <h2 class="section-heading blcenter text-uppercase">Vision &amp; Mission</h2>
         <h3 class="section-subheading text-muted">Welcome To Neer Construction.</h3>
       </div>
     </div>
     <div class="row">
       <div class="col-md-12">
         <div class="main-timeline">

           <div class="timeline">
             <a href="" class="timeline-content">
               <div class="timeline-icon">
                 <i class="fa fa-globe"></i>
               </div>
               <div class="inner-content">
                 <h3 class="title">VISION</h3>
                 <p class="description">To Create &amp; Develop The Indonesia. To Parallel &amp; competitive With Other</p>
               </div>
             </a>
           </div>

           <div class="timeline">
             <a href="" class="timeline-content">
               <div class="timeline-icon">
                 <i class="fa fa-users"></i>
               </div>
               <div class="inner-content">
                 <h3 class="title">MISSION</h3>
                 <p class="description">Involve and Full Participacy of Indonesia Development.To Pray and Work.</p>
               </div>
             </a>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section> -->

   <!-- <section class="testimonials text-center">
   <div class="container">
     <div class="row">
       <div class="col-md-8 mx-auto wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
         <h3 class="text-center font-weight-bold">JOIN MOJO<span class="bg-main">AVE</span> GROUPS</h3>
         <p class=" text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
       </div>
     </div>
     <div class="row">
       <div class="col-sm-6 col-md-4 col-lg-3 mt-4 wow bounceInUp" data-wow-duration="1.4s" style="visibility: visible; animation-duration: 1.4s; animation-name: bounceInUp;">
         <div class="card">
           <img class="card-img-top" src="https://images.pexels.com/photos/258732/pexels-photo-258732.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=650&amp;w=940">
           <div class="card-block">

             <h4 class="card-title text-center">CATHERINA GAIL</h4>

             <div class="card-text text-center">
               <div class="social-icons">
                 <a href="#" class="btn btn-circle my-social-btn fb"><i class="fa fa-facebook"></i></a>
                 <a href="#" class="btn btn-circle my-social-btn twitter"><i class="fa fa-twitter"></i></a>
                 <a href="#" class="btn btn-circle my-social-btn google"><i class="fa fa-google"></i></a>
               </div>
             </div>
           </div>
           <div class="card-footer text-center">
             <small>Lorem Ipsum is simply dummy text of the printing and typesetting</small>

           </div>
         </div>
       </div>
       <div class="col-sm-6 col-md-4 col-lg-3 mt-4 wow bounceInUp" data-wow-duration="1.4s" style="visibility: visible; animation-duration: 1.4s; animation-name: bounceInUp;">
         <div class="card">
           <img class="card-img-top" src="https://images.pexels.com/photos/210922/pexels-photo-210922.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=650&amp;w=940">
           <div class="card-block">

             <h4 class="card-title text-center">HARVEY RUBE</h4>

             <div class="card-text text-center">
               <div class="social-icons">
                 <a href="#" class="btn btn-circle my-social-btn fb"><i class="fa fa-facebook"></i></a>
                 <a href="#" class="btn btn-circle my-social-btn twitter"><i class="fa fa-twitter"></i></a>
                 <a href="#" class="btn btn-circle my-social-btn google"><i class="fa fa-google"></i></a>
               </div>
             </div>
           </div>
           <div class="card-footer text-center">
             <small>Lorem Ipsum is simply dummy text of the printing and typesetting</small>

           </div>
         </div>
       </div>
       <div class="col-sm-6 col-md-4 col-lg-3 mt-4 wow bounceInUp" data-wow-duration="1.4s" style="visibility: visible; animation-duration: 1.4s; animation-name: bounceInUp;">
         <div class="card">
           <img class="card-img-top" src="https://images.pexels.com/photos/756242/pexels-photo-756242.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=650&amp;w=940">
           <div class="card-block">

             <h4 class="card-title text-center">JANET PRIS</h4>

             <div class="card-text text-center">
               <div class="social-icons">
                 <a href="#" class="btn btn-circle my-social-btn fb"><i class="fa fa-facebook"></i></a>
                 <a href="#" class="btn btn-circle my-social-btn twitter"><i class="fa fa-twitter"></i></a>
                 <a href="#" class="btn btn-circle my-social-btn google"><i class="fa fa-google"></i></a>
               </div>
             </div>
           </div>
           <div class="card-footer text-center">
             <small>Lorem Ipsum is simply dummy text of the printing and typesetting</small>

           </div>
         </div>
       </div>
       <div class="col-sm-6 col-md-4 col-lg-3 mt-4 wow bounceInUp" data-wow-duration="1.4s" style="visibility: visible; animation-duration: 1.4s; animation-name: bounceInUp;">
         <div class="card">
           <img class="card-img-top" src="https://images.pexels.com/photos/167445/pexels-photo-167445.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=650&amp;w=940">
           <div class="card-block">

             <h4 class="card-title text-center">KEVIN WARD</h4>

             <div class="card-text text-center">
               <div class="social-icons">
                 <a href="#" class="btn btn-circle my-social-btn fb"><i class="fa fa-facebook"></i></a>
                 <a href="#" class="btn btn-circle my-social-btn twitter"><i class="fa fa-twitter"></i></a>
                 <a href="#" class="btn btn-circle my-social-btn google"><i class="fa fa-google"></i></a>
               </div>
             </div>
           </div>
           <div class="card-footer text-center">
             <small>Lorem Ipsum is simply dummy text of the printing and typesetting</small>

           </div>
         </div>
       </div>
     </div>
   </div>
 </section> -->

   <!-- <section class="bg-dark" id="services">
   <div class="container">
     <div class="row">
       <div class="col-lg-12 text-center">
         <h2 class="section-heading blcenter text-uppercase">Our Services</h2>
         <h3 class="section-subheading text-muted">We Provide Awesome Services.</h3>
       </div>
     </div>
     <div class="row">
       <div class="col-md-6 col-sm-6">
         <div class="package">
           <h4 class="package__name p__30">Lines Of Bussines</h4>
           <ul class="package__services pt__40 pb__20 list__none">
             <li><i class="fa fa-angle-right"></i>Engineering</li>
             <li><i class="fa fa-angle-right"></i>Procurement</li>
             <li><i class="fa fa-angle-right"></i>Construction</li>
             <li><i class="fa fa-angle-right"></i>Installation</li>
             <li><i class="fa fa-angle-right"></i>Commissioning</li>
             <li><i class="fa fa-angle-right"></i>Maintenance</li>
             <li><i class="fa fa-angle-right"></i>Manpower Outsourcing</li>
           </ul>
         </div>
       </div>
       <div class="col-md-6 col-sm-6">
         <div class="package">
           <h4 class="package__name p__30">Fields Of Bussines</h4>
           <ul class="package__services pt__40 pb__20 list__none">
             <li><i class="fa fa-angle-right"></i>Oil &amp; Gas Plant</li>
             <li><i class="fa fa-angle-right"></i>Power Plant</li>
             <li><i class="fa fa-angle-right"></i>Industrial Plant</li>
             <li><i class="fa fa-angle-right"></i>Petrochemical Plant</li>
             <li><i class="fa fa-angle-right"></i>Steel Structure &amp; Building Construction</li>
             <li><i class="fa fa-angle-right"></i>Pipe Line</li>
             <li><i class="fa fa-angle-right"></i>Mining Plant</li>
           </ul>
         </div>
       </div>
     </div>
   </div>
 </section> -->


   <!-- <section class="bg-light" id="portfolio">
   <div class="container">
     <div class="row">
       <div class="col-lg-12 text-center">
         <h2 class="section-heading blcenter blcenter text-uppercase">Portfolio</h2>
         <h3 class="section-subheading text-muted">We Provide Awesome Services.</h3>
       </div>
     </div>
     <div class="row">
       <div class="col-md-4 col-sm-6 portfolio-item">
         <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
           <div class="portfolio-hover">
             <div class="portfolio-hover-content">
               <i class="fas fa-plus fa-3x"></i>
             </div>
           </div>
           <img class="img-fluid" src="assets/images/piping1.jpg" alt="">
         </a>
         <div class="portfolio-caption">
           <h4>Storage Tank Project</h4>
         </div>
       </div>
       <div class="col-md-4 col-sm-6 portfolio-item">
         <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
           <div class="portfolio-hover">
             <div class="portfolio-hover-content">
               <i class="fas fa-plus fa-3x"></i>
             </div>
           </div>
           <img class="img-fluid" src="assets/images/piping2.jpg" alt="">
         </a>
         <div class="portfolio-caption">
           <h4>Steel Structure Fabrication Project</h4>
         </div>
       </div>
       <div class="col-md-4 col-sm-6 portfolio-item">
         <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
           <div class="portfolio-hover">
             <div class="portfolio-hover-content">
               <i class="fas fa-plus fa-3x"></i>
             </div>
           </div>
           <img class="img-fluid" src="assets/images/piping3.jpg" alt="">
         </a>
         <div class="portfolio-caption">
           <h4>Pipe And Mechanical fabrication Project</h4>

         </div>
       </div>
       <div class="col-md-4 col-sm-6 portfolio-item">
         <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
           <div class="portfolio-hover">
             <div class="portfolio-hover-content">
               <i class="fas fa-plus fa-3x"></i>
             </div>
           </div>
           <img class="img-fluid" src="assets/images/piping4.jpg" alt="">
         </a>
         <div class="portfolio-caption">
           <h4>Petrochemical Projects</h4>
         </div>
       </div>
       <div class="col-md-4 col-sm-6 portfolio-item">
         <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
           <div class="portfolio-hover">
             <div class="portfolio-hover-content">
               <i class="fas fa-plus fa-3x"></i>
             </div>
           </div>
           <img class="img-fluid" src="assets/images/piping6.jpg" alt="">
         </a>
         <div class="portfolio-caption">
           <h4>Gas Metering Project</h4>
         </div>
       </div>
       <div class="col-md-4 col-sm-6 portfolio-item">
         <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
           <div class="portfolio-hover">
             <div class="portfolio-hover-content">
               <i class="fas fa-plus fa-3x"></i>
             </div>
           </div>
           <img class="img-fluid" src="assets/images/piping7.jpg" alt="">
         </a>
         <div class="portfolio-caption">
           <h4>Gas Compressor Project</h4>
         </div>
       </div>
     </div>
   </div>

   <div class="col-lg-12 text-center">
     <div id="success">
       <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase">View More</button>
     </div>
   </div>
 </section> -->


   <!-- <section class="bg-dark" id="team">
   <div class="container">
     <div class="row">
       <div class="col-lg-12 text-center">
         <h2 class="section-heading blcenter text-uppercase" style="color:white">Board Of <span>Commissioner</span></h2>
         <h3 class="section-subheading text-muted">We Provide Awesome Services.</h3>
       </div>
     </div>

     <div class="container">
       <div class="row">
         <div class="col-md-4 col-sm-6">
           <div class="our-team">
             <div class="pic">
               <img src="assets/images/commissioner.png" alt="">
             </div>
             <div class="team-content">
               <h4 class="title">MASDAR</h4>
               <span class="post">Commissioner</span>
               <ul class="social">
                 <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                 <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                 <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
               </ul>
             </div>
           </div>
         </div>
         <div class="col-md-4 col-sm-6">
           <div class="our-team">
             <div class="pic">
               <img src="assets/images/presdir.png" alt="">
             </div>
             <div class="team-content">
               <h4 class="title">MUHAMMAD DARMAWAN</h4>
               <span class="post">President Director</span>
               <ul class="social">
                 <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                 <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                 <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
               </ul>
             </div>
           </div>
         </div>
         <div class="col-md-4 col-sm-6">
           <div class="our-team">
             <div class="pic">
               <img src="assets/images/director.png" alt="">
             </div>
             <div class="team-content">
               <h4 class="title">LEGOWO</h4>
               <span class="post">Director</span>
               <ul class="social">
                 <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                 <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                 <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
               </ul>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section> -->



   <section class="bg-light" id="portfolio">
     <div class="container">
       <div class="row">
         <div class="col-lg-12 text-center">
           <h2 class="text-uppercase"></strong>Berita</strong></h2>
           <div class="divider"></div>
         </div>
       </div>

       <div class="row">
         <div class="container">
           <div class="album bg-light">

             <div class="row">
               <?php foreach ($post->result_array() as $j) {
                  $tgl = date('Y-m-d', strtotime($j['tulisan_tanggal']))
                ?>

                 <div class="col-md-4 col-sm-4">
                   <div class="card mb-4 shadow-sm portfolio-item">
                     <a class="portfolio-link" href="<?php echo base_url() . 'artikel/' . $j['tulisan_slug']; ?>">
                       <div class="portfolio-hover">
                         <div class="portfolio-hover-content">
                           <i class="fa fa-crosshairs fa-3x"></i>
                         </div>
                       </div>
                       <img class="card-img-top" style="width:100% !important;" src="<?php echo base_url() . 'storage/images/berita/' . $j['tulisan_gambar'] ?>" alt="" alt="Card image cap">

                     </a>
                     <div class="card-body">
                       <p class="card-text"><?= $j['tulisan_judul'] ?></p>
                       <small class="text-muted"><?= tgl_indo($tgl) . ' | ' . $j['tulisan_author'] ?> </small>

                       <div class="text-left mt-1">
                         <a class="btn btn-primary btn-sm js-scroll-trigger  pb-2 pt-2 pl-3 pr-3" href="<?php echo base_url() . 'artikel/' . $j['tulisan_slug']; ?>">Baca lebih lanjut</a>
                       </div>
                     </div>
                   </div>
                 </div>
               <?php } ?>
             </div>
           </div>
         </div>


       </div>
       <div class="row">
         <div class="col-lg-12 text-center">
           <hr>
           <a class="btn btn-primary btn-md js-scroll-trigger" href="<?= base_url() . 'berita/kategori/1' ?>">Tampilkan Semua</a>
           <hr>
         </div>
       </div>
   </section>

   <section class="bg-dark" id="team">
     <div class="container">
       <div class="row">
         <div class="col-lg-12 text-center">
           <h2 class="section-heading blcenter text-uppercase" style="color:white">Struktur <span>Organisasi</span></h2>
           <h3 class="section-subheading text-muted"></h3>
         </div>
       </div>

       <div class="container">
         <div class="row">
           <div class="col-md-12 col-sm-6">
             <div class="our-management">
               <div class="pic">
                 <img src="assets/images/tmanagement.jpg" alt="">
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </section>





   <!-----------------------  END  SECTION ABOUT   ------------------------->



   <!-----------------------   NEW UPDATES   ------------------------->








   <!-----------------------   END NEW UPDATES   ------------------------->



   <!-------------------------------- ACHIEVMENT  
  <section class="bg-light" id="portfolio">
    <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Arsip</strong></h2>
            <div class="divider"></div>
          </div>
        </div>

        <div class="row">
          <?php foreach ($post->result_array() as $j) {
            $tgl = date('Y-m-d', strtotime($j['tulisan_tanggal']))
          ?>

            <div class="col-md-4 col-sm-4 portfolio-item">
              <a class="portfolio-link" data-toggle="modal" href="<?php echo base_url() . 'artikel/' . $j['tulisan_slug']; ?>">
                <div class="portfolio-hover">
                  <div class="portfolio-hover-content">
                    <i class="fa fa-location-arrow fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="<?php echo base_url() . 'storage/images/berita/' . $j['tulisan_gambar'] ?>" alt="">
              </a>
              <div class="portfolio-caption">
                <h5 class="service-heading"><?= $j['tulisan_judul'] ?></h5>
                <span class="author"><i class="far fa-calendar-alt"><?= tgl_indo($tgl) ?> </i></span>
                <span class="author"><i class="fas fa-id-badge"><?= ' By<?= tgl_indo($tgl) ?> ' . $j['tulisan_author'] ?> </i></span>
                <a class="btn btn-primary btn-sm js-scroll-trigger" href="<?php echo base_url() . 'artikel/' . $j['tulisan_slug']; ?>">Read More</a>

              </div>
            </div>
          <?php } ?>
        </div>
       <div class="row">
        <div class="col-lg-12 text-center pt__40">
            <a class="btn btn-primary btn-lg js-scroll-trigger" href="<?= base_url() . 'berita/kategori/3' ?>">View More</a>
        </div>
      </div>
    </div>
 </section>



 <!-------------------------------- SERVING ----------------------------->
   <!--<section class="image__content">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="side__img mt__30 mt__sm__0" data-aos="fade-left">
							<img src="assets/images/iMac.png" alt="img">
						</div>
					</div>
					<div class="col-md-6 col-md-offset-2 col-sm-9 col-sm-offset-3">
						<div class="v2_title pb__150 pt__130 pt__md__90 pt__md__90 pt__sm__80 pb__sm__80" data-aos="fade-right">
                <h2>Kompetensi Lulusan</h2>
                <ul class="list__item pl__0 pt__40 list__none">
                  <li><i class="fa fa-angle-right"></i><span>Hafizh Qur’an 30 juz mutqin.</span></li>
                  <li><i class="fa fa-angle-right"></i><span>Memahami akidah ahlussunnah wal jama’ah.</span></li>
                  <li><i class="fa fa-angle-right"></i><span>Menguasai dasar-dasar ilmu syar’i.</span></li>
                  <li><i class="fa fa-angle-right"></i><span>Memiliki kemampuan berkomunikasi dengan 2 bahasa internasional [Bahasa Arab dan Inggris].</span></li>
                </ul>
						</div>
					</div>
				</div>
			</div>
		</section>-->
   <!-------------------------------- END SERVING ----------------------------->


   <!-- <section class="bg-light">
   <div class="container">
     <div class="row">
       <div class="col-lg-12 text-center">
         <h2 class="section-heading blcenter blcenter text-uppercase">Our <span>Legalitas</span></h2>
         <h3 class="section-subheading text-muted">We .</h3>
       </div>
     </div>
     <div class="row text-center">
       <div class="col-md-6 col-sm-6">
        <div id="app">
              <iframe
                src="https://drive.google.com/file/d/1kJJeabYl-NH4nsNV8kLs-qTUYESbUfMx/preview"
                width="50%"
                height="50%"
              >
              </iframe>
       </div>
       <div class="col-md-6 col-sm-6">
         <a href="#">
           <img class="img-fluid d-block mx-auto" width="50%" src="assets/images/logoiso.jpg" alt="">
         </a>
       </div>
     </div>
   </div>
 </section> -->
   <!-------------------------------- OUR PARTNER ----------------------------->

   <section class="py-5">
     <div class="container">
       <div class="row">
         <div class="col-lg-12 text-center">
           <h2 class="section-heading text-uppercase"><strong>PARTNER</strong></h2>
           <div class="divider"></div>
         </div>
       </div>
       <section class="customer-logos text-center slider">
         <?php foreach ($partner as $j) { ?>
           <div class="slide"><img src="<?= base_url('assets/images/') . $j['photo'] ?>" alt=""></div>
         <?php  } ?>
       </section>
     </div>
   </section>



   <!-- <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="close-modal" data-dismiss="modal">
         <div class="lr">
           <div class="rl"></div>
         </div>
       </div>
       <div class="container">
         <div class="row">
           <div class="col-lg-8 mx-auto">
             <div class="modal-body">
             
               <h2 class="text-uppercase">Storage Tank Project</h2>
              
               <img class="img-fluid d-block mx-auto" src="assets/images/piping1.jpg" alt="">
        
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Threads</li>
                    <li>Category: Illustration</li>
                  </ul>
               <button class="btn btn-primary" data-dismiss="modal" type="button">
                 <i class="fas fa-times"></i>
                 Close Project</button>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>

 
 <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="close-modal" data-dismiss="modal">
         <div class="lr">
           <div class="rl"></div>
         </div>
       </div>
       <div class="container">
         <div class="row">
           <div class="col-lg-8 mx-auto">
             <div class="modal-body">
               
               <h2 class="text-uppercase">Steel Strcuture Fabrication Project</h2>
               <img class="img-fluid d-block mx-auto" src="assets/images/piping2.jpg" alt="">
               <button class="btn btn-primary" data-dismiss="modal" type="button">
                 <i class="fas fa-times"></i>
                 Close Project</button>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>


 <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="close-modal" data-dismiss="modal">
         <div class="lr">
           <div class="rl"></div>
         </div>
       </div>
       <div class="container">
         <div class="row">
           <div class="col-lg-8 mx-auto">
             <div class="modal-body">
           
               <h2 class="text-uppercase">Pipe And Mechanical Fabrication Project</h2>
               <img class="img-fluid d-block mx-auto" src="assets/images/piping3.jpg" alt="">
               <button class="btn btn-primary" data-dismiss="modal" type="button">
                 <i class="fas fa-times"></i>
                 Close Project</button>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>


 <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="close-modal" data-dismiss="modal">
         <div class="lr">
           <div class="rl"></div>
         </div>
       </div>
       <div class="container">
         <div class="row">
           <div class="col-lg-8 mx-auto">
             <div class="modal-body">
            
               <h2 class="text-uppercase">Petrochemical Project</h2>
               <img class="img-fluid d-block mx-auto" src="assets/images/piping4.jpg" alt="">
               <button class="btn btn-primary" data-dismiss="modal" type="button">
                 <i class="fas fa-times"></i>
                 Close Project</button>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>


 <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="close-modal" data-dismiss="modal">
         <div class="lr">
           <div class="rl"></div>
         </div>
       </div>
       <div class="container">
         <div class="row">
           <div class="col-lg-8 mx-auto">
             <div class="modal-body">
              
               <h2 class="text-uppercase">Gas Metering Project</h2>
               <img class="img-fluid d-block mx-auto" src="assets/images/piping6.jpg" alt="">
               <button class="btn btn-primary" data-dismiss="modal" type="button">
                 <i class="fas fa-times"></i>
                 Close Project</button>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>


 <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="close-modal" data-dismiss="modal">
         <div class="lr">
           <div class="rl"></div>
         </div>
       </div>
       <div class="container">
         <div class="row">
           <div class="col-lg-8 mx-auto">
             <div class="modal-body">
              
               <h2 class="text-uppercase">Gas Compressor Project</h2>
               <img class="img-fluid d-block mx-auto" src="assets/images/piping7.jpg" alt="">
               <button class="btn btn-primary" data-dismiss="modal" type="button">
                 <i class="fas fa-times"></i>
                 Close Project</button>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div> -->

   <section id="contact">
     <div class="container">
       <div class="row">

         <div class="col-lg-12 text-center">
           <h2 class="section-heading blcenter text-uppercase" style="color:white">Kontak <span>Kami</span></h2>
           <h3 class="section-subheading text-muted"></h3>
         </div>
       </div>
       <div class="row">
         <div class="col-lg-12">
           <form action="<?= base_url('kontak/kirim_pesan') ?>" class="contact-form" method="post">
             <div class="row">
               <div class="col-md-6 col-lg-6">
                 <div class="container-fluid ">
                   <div class="row">
                     <iframe width="100%" height="320" id="" src="<?= $dataweb['mapslink'] ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                   </div>
                 </div>
               </div>
               <div class="col-md-12 col-lg-6">
                 <div class="form-group">
                   <input class="form-control" name="name" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name.">
                   <p class="help-block text-danger"></p>
                 </div>
                 <div class="form-group">
                   <input class="form-control" name="email" id="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address.">
                   <p class="help-block text-danger"></p>
                 </div>
                 <div class="form-group">
                   <input class="form-control" name="phone" id="phone" type="tel" placeholder="Your Phone *" required="required" data-validation-required-message="Please enter your phone number.">
                   <p class="help-block text-danger"></p>
                 </div>
                 <div class="form-group">
                   <textarea class="form-control" name="message" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                   <p class="help-block text-danger"></p>
                 </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-lg-12 text-center">
                 <div id="success"></div>
                 <button class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
               </div>
             </div>
           </form>
         </div>
       </div>
     </div>
   </section>

   <?php
    $this->load->view('v_footer');
    ?>

   <button class="fa fa-angle-up" onclick="topFunction()" id="myBtn" title="Go to top"></button>


   <!-- Bootstrap core JavaScript -->
   <script src="<?php echo base_url('themes/vendor/jquery/jquery.min.js') ?>"></script>
   <script src="<?php echo base_url('themes/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
   <script src="<?php echo base_url('themes/vendor/bootstrap/js/popper.min.js') ?>"></script>
   <!-- <script src="<?php echo base_url('themes/js/slick.js') ?>"></script> -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>


   <!-- FUNGSI LIMIT KARAKTER BERITA -->
   <?php function limit_text($text, $limit)
    {
      if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
    } ?>

   <!-- FUNGSI TANGGAL  -->
   <?php function tgl_indo($tanggal)
    {
      $bulan = array(
        1 => 'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Agust',
        'Sept',
        'Okt',
        'Nov',
        'Des',
      );
      $pecahkan = explode('-', $tanggal);

      // variabel pecahkan 0 = tanggal
      // variabel pecahkan 1 = bulan
      // variabel pecahkan 2 = tahun

      return $pecahkan[2] . ' ' . $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
    } ?>


   <!-- scrol animation -->
   <script src="<?php echo base_url('assets/bootstrap/js/aos.js') ?>"></script>

   <script>
     AOS.init();
   </script>

   <script>
     //==== * SLIDE SLOW OUR PARTNER ======== */
     $(document).ready(function() {
       $('.customer-logos').slick({
         slidesToShow: 4,
         slidesToScroll: 1,
         autoplay: true,
         autoplaySpeed: 1000,
         arrows: false,
         dots: false,
         pauseOnHover: false
       });

     });

     $(".f1").hover(function(e) {
       $(this).addClass('animated pulse');
     }, function(e) {
       $(this).removeClass('animated pulse');
     });

     $(".f2").hover(function(e) {
       $(this).addClass('animated pulse');
     }, function(e) {
       $(this).removeClass('animated pulse');
     });

     $(".f3").hover(function(e) {
       $(this).addClass('animated pulse');
     }, function(e) {
       $(this).removeClass('animated pulse');
     });

     $(".f4").hover(function(e) {
       $(this).addClass('animated pulse');
     }, function(e) {
       $(this).removeClass('animated pulse');
     });

     $(".f5").hover(function(e) {
       $(this).addClass('animated pulse');
     }, function(e) {
       $(this).removeClass('animated pulse');
     });
   </script>

   <script>
     // When the user scrolls down 20px from the top of the document, show the button
     window.onscroll = function() {
       scrollFunction()
     };

     function scrollFunction() {
       if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
         document.getElementById("myBtn").style.display = "block";
       } else {
         document.getElementById("myBtn").style.display = "none";
       }
     }

     // When the user clicks on the button, scroll to the top of the document
     function topFunction() {
       document.body.scrollTop = 0;
       document.documentElement.scrollTop = 0;
     }
   </script>


   <!-- Bootstrap core JavaScript -->
   <script src="<?php echo base_url('themes/default2/vendor/jquery/jquery.min.js') ?>"></script>
   <script src="<?php echo base_url('themes/default2/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
   <script src="<?php echo base_url('themes/default2/vendor/bootstrap/js/popper.min.js') ?>"></script>
   <!-- Plugin JavaScript -->
   <script src="<?php echo base_url('themes/default2/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

   <!-- Contact form JavaScript -->
   <script src="<?php echo base_url('themes/default2/js/jqBootstrapValidation.js') ?>"></script>
   <script src="<?php echo base_url('themes/default2/js/contact_me.js') ?>"></script>
   <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>

   <!-- Custom scripts for this template -->
   <script src="<?php echo base_url('themes/default2/js/agency.min.js') ?>"></script>

   <?php if ($this->session->flashdata('msg') == 'error') : ?>
     <script type="text/javascript">
       $.toast({
         heading: 'Error',
         text: "Gagal Menguhubungi Kami",
         showHideTransition: 'slide',
         icon: 'error',
         hideAfter: false,
         position: 'bottom-right',
         bgColor: '#FF4859'
       });
     </script>

   <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
     <script type="text/javascript">
       $.toast({
         heading: 'Success',
         text: "Terima Kasih Telah Menghubungi Kami.",
         showHideTransition: 'slide',
         icon: 'success',
         hideAfter: false,
         position: 'bottom-right',
         bgColor: '#7EC857'
       });
     </script>
   <?php else : ?>
   <?php endif; ?>
   <script>
     function readURLKK($element, $tampil) {
       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById($element).files[0]);
       oFReader.onload = function(oFREvent) {
         document.getElementById($tampil).src = oFREvent.target.result;
       };
     };
   </script>

   </body>

   </html>