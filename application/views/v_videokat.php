
<?php
error_reporting(0);
?>
<?php
	$this->load->view('v_header');
?>

<div class="mt-5 pt-2">
<?php
  $this->load->view('v_toolbar');
  ?>
</div>
<button class="fa fa-angle-up" onclick="topFunction()" id="myBtn" title="Go to top"></button>
<section class="bg-light" id="team">
    <div class="container">
      <div class="row mt-5">
        <div class="col-lg-12 text-center">
          <h2 class="text-uppercase">Video</h2>
					<div class="divider"></div>
        </div>
      </div>
      <div class="row">
      <div class="col-md-12">
        <?php foreach ($alb->result_array() as $row) : ?>
        
					<div class="gallery_product col-lg-4 col-md-4 col-sm-4">
              <a href="<?php echo base_url().'videoalbum/kategori/'.$row['videokat_id'];?>">
              <img class="thumbnail img-responsive"
              src="<?= base_url('/storage/images/galery/').$row['videokat_cover'] ?>"></a>
              <span class="thumb-info-title" style="bottom:0;width:100%;">
											<p class="thumb-info-inner" style="font-weight:bold;">
                      <?= $row['videokat_nama'] ?> <small style="font-weight:normal;">(<?= $row['videokat_count'] ?> Videos)</small>
                    </p>
								</span>
          </div>
        <?php endforeach;?>
        </div>
      </div>
    </div>
  </section>
  

	<?php $this->load->view('v_footer');?>
	</div>

  
  <script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
    document.getElementById("myBtn").style.display = "block";
  } else {
    document.getElementById("myBtn").style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>


<script src="<?php echo base_url().'themes/vendor/jquery/jquery.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/bootstrap.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/vendor/bootstrap/js/popper.min.js'?>"></script>
 <script src="<?php echo base_url().'themes/js/slick.js'?>"></script>
 <script src="<?php echo base_url().'assets/bootstrap/js/aos.js'?>"></script>
<script src="<?php echo base_url().'themes/vendor/jquery-easing/jquery.easing.min.js'?>"></script>
<script src="<?php echo base_url().'themes/js/jqBootstrapValidation.js'?>"></script>
<script src="<?php echo base_url().'themes/js/contact_me.js'?>"></script>
<script src="<?php echo base_url().'themes/js/agency.min.js'?>"></script>
	</body>
</html>