<?php
class M_anggota extends CI_Model{

	function get_all_anggota(){
		$hsl=$this->db->query("SELECT tbl_anggota.*,IF(jenis_kelamin='L','Laki-Laki','Perempuan') AS jenkel FROM tbl_anggota");
		return $hsl;	
	}

    function get_anggota_byid($id){
		$hsl=$this->db->query("SELECT tbl_anggota.*,IF(jenis_kelamin='L','Laki-Laki','Perempuan') AS jenkel FROM tbl_anggota where id = $id");
		return $hsl;	
	}

    function get_all_anggota_perpage($offset,$limit){
		$hsl=$this->db->query("SELECT tbl_anggota.*,IF(jenis_kelamin='L','Laki-Laki','Perempuan') AS jenkel FROM tbl_anggota limit $offset,$limit");
		return $hsl;	
	}

    

	function simpan_anggota($data){
        $hsl = $this->db->insert('tbl_anggota', $data);
		return $hsl;
	}

	public function update_anggota($kode, $data)
    {
        $this->db->where('id', $kode);
        $hsl = $this->db->update('tbl_anggota', $data);
		return $hsl;
	}

	public function hapus_anggota($id)
    {
        $this->db->where('id', $id);
        $hsl = $this->db->delete('tbl_anggota');
        return $hsl;
    }

	public function get_anggota($id)
    {
        $this->db->where('id', $id);
        $hsl = $this->db->get('tbl_anggota');
        return $hsl;
    }

}