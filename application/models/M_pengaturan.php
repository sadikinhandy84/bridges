<?php

class M_pengaturan extends CI_model{

    function tampil_data_web(){
       $hasil = $this->db->get('tbl_web');
       return $hasil;
    }

    function ubah_data_web($id,$dataweb)
    {
       
        $this->db->where('id_web',$id);
        $hasil = $this->db->update('tbl_web',$dataweb);
        return $hasil;
    }

    function upload_icon($id,$dataweb)
    {
        $this->db->where('id_web',$id);
        $hasil = $this->db->update('tbl_web',$dataweb);
        return $hasil;
    }

    function upload_logo($id,$dataweb)
    {
        $this->db->where('id_web',$id);
        $hasil = $this->db->update('tbl_web',$dataweb);
        return $hasil;
    }

}

