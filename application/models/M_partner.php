<?php
class M_partner extends CI_Model{

	function get_all_partner(){
		$hsl=$this->db->query("SELECT tbl_partner.* FROM tbl_partner");
		return $hsl;	
	}

   
	function simpan_partner($data){
        $hsl = $this->db->insert('tbl_partner', $data);
		return $hsl;
	}

	public function update_partner($kode, $data)
    {
        $this->db->where('id', $kode);
        $hsl = $this->db->update('tbl_partner', $data);
		return $hsl;
	}

	public function hapus_partner($id)
    {
        $this->db->where('id', $id);
        $hsl = $this->db->delete('tbl_partner');
        return $hsl;
    }

	public function get_partner($id)
    {
        $this->db->where('id', $id);
        $hsl = $this->db->get('tbl_partner');
        return $hsl;
    }

}