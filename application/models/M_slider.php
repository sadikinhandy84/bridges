<?php
class M_slider extends CI_Model
{

    public function get_all_slider()
    {
        $hsl = $this->db->get('tbl_slide');
        return $hsl;
    }
    public function simpan_slider($data)
    {
        $this->db->insert('tbl_slide', $data);
        return $hsl;
    }

    public function detil_slider($kode)
    {
        $this->db->where('id', $kode);
        $hsl = $this->db->get('tbl_slide');
        return $hsl;
    }

    public function ubah_slider($kode, $data)
    {
        $this->db->where('id', $kode);
        $hsl = $this->db->update('tbl_slide', $data);
        return $hsl;
    }

    public function update_slider_tanpa_img($kode, $data)
    {
        $this->db->where('id', $kode);
        $hsl = $this->db->update('tbl_slide', $data);
        return $hsl;
    }

    public function hapus_slider($kode)
    {
        $this->db->trans_start();
        $this->db->where('id', $kode);
        $hsl = $this->db->delete('tbl_slide');

        $this->db->trans_complete();
        if ($this->db->trans_status() == true) {
            return true;
        } else {
            return false;
        }

    }

}
