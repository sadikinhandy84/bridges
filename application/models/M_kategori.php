<?php
class M_kategori extends CI_Model{

	function get_all_kategori(){
		$hsl=$this->db->get("tbl_kategori");
		return $hsl;
	}


	

	function simpan_kategori($kategori){
		
		$hsl=$this->db->insert('tbl_kategori',$kategori);
		return $hsl;
	}
	function update_kategori($kode,$kategori){
		$this->db->where('kategori_id',$kode);
		$hsl=$this->db->update('tbl_kategori',$kategori);
		return $hsl;
	}
	function hapus_kategori($kode){
		$this->db->where('kategori_id',$kode);
		$hsl=$this->db->delete('tbl_kategori');
		return $hsl;
	}
	
	function get_kategori_byid($kategori_id){
		$this->db->where('kategori_id',$kategori_id);
		$hsl=$this->db->get('tbl_kategori');
		return $hsl;
	}

	function get_kategori_list($kategori_id){
		$this->db->where('post_id',$kategori_id);
		$hsl=$this->db->get('tbl_posts_categories');
		return $hsl;
		
	}

}