<?php
class M_video extends CI_Model{

	function get_all_video(){
		$hsl=$this->db->query("SELECT tbl_video.*,DATE_FORMAT(video_tanggal,'%d/%m/%Y') AS tanggal,videokat_nama FROM tbl_video join tbl_videokat on video_kat_id=videokat_id ORDER BY video_id DESC");
		return $hsl;
	}
	function simpan_video($judul,$album,$user_id,$user_nama,$gambar){
		$this->db->trans_start();
            $this->db->query("insert into tbl_video(video_judul,video_kat_id,video_pengguna_id,video_author,video_link) values ('$judul','$album','$user_id','$user_nama','$gambar')");
            $this->db->query("update tbl_videokat set videokat_count=videokat_count-1 where videokat_id='$album'");
        $this->db->trans_complete();
        if($this->db->trans_status()==true)
        return true;
        else
        return false;
	}
	
	function update_video($galeri_id,$judul,$album,$user_id,$user_nama,$gambar){
		$hsl=$this->db->query("update tbl_video set video_judul='$judul',video_kat_id='$album',video_pengguna_id='$user_id',video_author='$user_nama',video_link='$gambar' where video_id='$galeri_id'");
		return $hsl;
	}

	function hapus_video($kode,$album){
		$this->db->trans_start();
            $this->db->query("delete from tbl_video where video_id='$kode'");
            $this->db->query("update tbl_videokat set videokat_count=videokat_count-1 where videokat_id='$album'");
        $this->db->trans_complete();
        if($this->db->trans_status()==true)
        return true;
        else
        return false;
	}

	//Front-End
	function get_video_home(){
		$hsl=$this->db->query("SELECT tbl_video.*,DATE_FORMAT(galeri_tanggal,'%d/%m/%Y') AS tanggal,videokat_nama FROM tbl_video join tbl_videokat on video_kat_id=videokat_id ORDER BY video_id DESC limit 4");
		return $hsl;
	}

	function get_video_by_kat_id($idalbum){
		$hsl=$this->db->query("SELECT tbl_video.*,DATE_FORMAT(video_tanggal,'%d/%m/%Y') AS tanggal,videokat_nama FROM tbl_video join tbl_videokat on video_kat_id=videokat_id where video_kat_id='$idalbum' ORDER BY video_id DESC");
		return $hsl;
	}

	function get_all_videokat(){
		$hsl=$this->db->query("SELECT tbl_videokat.*,DATE_FORMAT(videokat_tanggal,'%d/%m/%Y') AS tanggal FROM tbl_videokat ORDER BY videokat_id DESC");
		return $hsl;
	}
	function simpan_videokat($album,$user_id,$user_nama,$gambar){
		$hsl=$this->db->query("insert into tbl_videokat(videokat_nama,videokat_pengguna_id,videokat_author,videokat_cover) values ('$album','$user_id','$user_nama','$gambar')");
		return $hsl;
	}
	
	function update_videokat($album_id,$data){
		$this->db->where('videokat_id',$album_id);
		$hsl=$this->db->update('tbl_videokat',$data);
		return $hsl;
	}
	
	function hapus_videokat($kode){
		$this->db->where('videokat_id',$kode);
		$hsl=$this->db->delete('tbl_videokat');
		return $hsl;
	}
	


}